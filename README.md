SSPBrno Web v3
===
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/Zhincore/sspbrnoweb/master)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_sspbrnoweb-monorepo&metric=security_rating)](https://sonarcloud.io/dashboard?id=purkynka-renewal_sspbrnoweb-monorepo)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_sspbrnoweb-monorepo&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=purkynka-renewal_sspbrnoweb-monorepo)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_sspbrnoweb-monorepo&metric=alert_status)](https://sonarcloud.io/dashboard?id=purkynka-renewal_sspbrnoweb-monorepo)

Monorepo hosting the following apps:

- `pcms` (`apps/backend/pcms`)
- `pcms-admin` (`apps/backend/admin`)
- `sspbrno-web` (`apps/frontend`)


## <a name="contents"></a>Contents
- [Contents](#contents)
- [Structure](#folder-structure)
- [Usage](#usage)
    - [Requirements](#requirements)
    - [Installation](#installation)
        - [Preparation](#preparation)
        - [Building](#building)
    - [Running](#running)
        - [Example systemd service and NGINX configuration](#example)
    - [Updating](#updating)
- [Development](#development)


## <a name="folder-structure"></a>Folder structure
```
.
|- example - example configuration files
'- apps - the actual source code
  |- backend
  | |- pcms - the backbone of the whole project, manages database and files
  | '- admin - the control panel for PCMS, provides GUI for pcms
  |- frontend - the website
  '- _shared - (not a standalone app) code shared between frontends
```

## <a name="usage"></a>Usage
### <a name="requirements"></a>Requirements
#### Common
- NodeJS v12 or newer
- PNPM (`npm i -g pnpm`)

#### Backend
- MySQL database
- FFmpeg (to probe media files and generate video thumbnails)

### <a name="installation"></a>Instalation
#### <a name="preparation"></a>Preparation
Before building, you should:

- Setup a database so that it can be configured by the build process:
    1. Connect to your MySQL (e.g. `sudo mysql`).
    1. Create a database (`CREATE DATABASE pcms;`).
    1. Create an account and give it rights for the database (`GRANT ALL ON pcms.* to 'pcms'@'localhost' IDENTIFIED BY 'securepassword';`).  

  With this setup the "`DATABASE_URL`" should be `mysql://pcms:securepassword@localhost/pcms?socket=/run/mysqld/mysqld.sock`.  
  The build process will create or update all needed tables and you don't have to worry about them.

- Create an `.env` file in the project's root and configure it to your needs, use the `.env.sample` as a template.

#### <a name="building"></a>Building
1. Run `pnpm i -P --frozen-lockfile` to install dependencies.
1. Run `pnpm build:backend` to build the backend.
1. [Start up PCMS before building the frontend](#running) (frontend needs to fetch initial data while building)
1. Finally run `pnpm build:frontend` to build the frontend.

### <a name="running"></a>Running
To start the backend, use `pnpm start:backend` in the root of the project.

To start the frontend, use `pnpm start:frontend`.
By default it runs on `localhost:3000`, to change that either:

- set environment variable `PORT`/`HOST` (e.g. `PORT="6530" pnpm start:frontend`)
- set arguments for Next.js like this: `pnpm start:frontend -- -- -p 6530`

#### <a name="example"></a>Example SystemD service and NGINX configuration
See the `example` folder in the root of this repository.

#### Notes
- Setting `NODE_ENV="production"` is highly recommended for best performance.
- Host address for both apps should be `localhost` and a reverse proxy should be placed between the apps and the internet.
- It's recommend to use SystemD socket or the `PCMS_SOCKET` variable in `.env` (`PCMS_HOST` and `PCMS_PORT` will be ignored) for a speed improvement of at least 5ms.
    - If you want the socket to be accessible by different users, add `PCMS_SOCKET_PUBLIC=true` to your `.env` file.
- Adding `?socket=<path>` (as shown in `.env.sample`) at the end of `DATABASE_URL` grants additional miliseconds of speed improvement.

#### <a name="ondemand-pcms"></a>Running PCMS on-demand
You can also setup PCMS to only run on-demand with SystemD. (Frontend is not capable of running on-demand.)

- Create a new file in SystemD configuration folder, next to the `pcms.service`:
    - Name it the same as the service but with `.socket` extension (`pcms.socket` in our case).
    - Put the following content there:
      ```
      [Socket]
      ListenStream=/run/pcms.sock

      [Install]
      WantedBy=sockets.target
      ```  
    - **Note:** You can replace `/run/pcms.sock` with a different path or a port.
- Modify `pcms.service`:
    - Remove the whole `[Install]` section.
    - Remove the `Restart=always` line.
- Add `PCMS_AUTO_EXIT=10m` to `.env` to exit after 10 minutes of inactivity (you can change the value).
- Now stop `pcms.service` if it's running and enable and start `pcms.socket` (`sudo systemctl enable --now pcms.socket`).

##### Notes
- If you want to use SystemD socket but not on-demand mode, don't set `PCMS_AUTO_EXIT` or set it to `0`.
- Neither `PCMS_SOCKET` nor `PCMS_PORT` will be used in SystemD mode.
- For slightly faster boot of PCMS modify `pcms.service` as follows:
    - Change `WorkingDirectory` to the full path to PCMS (e.g. `/srv/sspbrnoweb/apps/backend/pcms`)
    - Change `ExecStart` to `/usr/bin/env node .`

### <a name="updating"></a>Updating
1. Run `git pull` (if git cloned previously)
1. Continue with steps in [Installation](#installation)

## <a name="development"></a>Development
All apps have a dev script (`pnpm dev`) that will start the app in development mode which automatically responds to file changes.  
ESLint with Prettier and SonarJS plugins is used to keep the code formatted, clean and consistent.  

## Trouble shooting
### Prisma's "⚠️  There might be data loss when applying the changes"
You're probably upgrading and the database schema changed. You're about to loose some data, sorry.

Run `pnpm -C apps/backend/pcms build:db` and answer to Prisma's prompts (mostly you'll want to answer "`y`").
