import Path from "path";
import { exec } from "child_process";
import { promisify } from "util";
import fs from "fs";
import ora from "ora";
import inquirer from "inquirer";
import chalk from "chalk";

const execAsync = promisify(exec);

async function main() {
  console.log(chalk.yellowBright.bold("Welcome!"));
  console.log(chalk.whiteBright("Let's install my maturita, shall we?"));
  console.log("");
  console.log(chalk.dim(chalk.bold("NOTE: ") + "This script requires that the example folder is intact."));
  console.log("---\n");

  console.log(chalk.bold("Database setup:"));
  const dbUrl = await inquirer
    .prompt([
      {
        name: "setup",
        type: "confirm",
        message: "Do you want to create a MySQL user and database now?",
        suffix: chalk.dim(" Requires running as root"),
        default: false,
      },
      { name: "user", type: "input", message: "MySQL username:", default: "pcms", when: shouldSetupDb },
      { name: "pass", type: "password", message: "MySQL password:", default: "password", when: shouldSetupDb },
      { name: "name", type: "input", message: "MySQL database name:", default: "pcms", when: shouldSetupDb },
    ])
    .then(async (db) => {
      if (!db.setup) return;
      const spinner = ora(chalk.yellow("Setting up the DB...")).start();
      try {
        await setupDb(db);
        spinner.stop();
        console.log(chalk.green("DB setup done!"));
      } catch (err) {
        spinner.stop();
        console.error(err);
      }
      return `mysql://${db.user}:${db.pass}@localhost/${db.name}?socket=/var/run/mysqld/mysqld.sock`;
    });

  console.log(chalk.bold("Project configuration"));
  const config = await inquirer.prompt([
    // {
    //   name: "useSocket",
    //   type: "confirm",
    //   message: "Do you want to run PCMS over unix socket?",
    // },
    // {
    //   name: "pcms_socket",
    //   type: "input",
    //   message: "Path for unix socket of PCMS",
    //   default: "/tmp/pcms.socket",
    //   when:
    // },
    {
      name: "env",
      type: "editor",
      message: "Confirm the generated .env",
      default: async (/*answers*/) => {
        let path = ".env.sample";
        if (fs.existsSync(".env")) path = ".env";

        let env = await fs.promises.readFile(path, "utf-8");

        if (dbUrl) env = env.replace(/DATABASE_URL=.+\n/, `DATABASE_URL=${dbUrl}`);
        return env;
      },
    },
  ]);
  fs.promises.writeFile(".env", config.env, "utf-8");

  const { setupSysd } = await inquirer.prompt([
    {
      name: "setupSysd",
      type: "confirm",
      message: "Do you want to setup SystemD services now?",
      suffix: chalk.dim(" Requires running as root"),
    },
  ]);

  if (setupSysd) {
    console.log(chalk.bold("SystemD configuration"));
    const sysdConf = await inquirer.prompt([
      {
        name: "path",
        type: "input",
        message: "Path for the service files",
        default: "/usr/lib/systemd/system/",
      },
      {
        name: "user",
        type: "input",
        message: "User to run services as",
        default: "www-data",
      },
      {
        name: "group",
        type: "input",
        message: "Group to run services as",
        default: "www-data",
      },
    ]);
    await setupSysD(sysdConf);
  }
}

function shouldSetupDb(answers) {
  return answers.setup;
}

//

function setupDb(db) {
  const sql = [
    `CREATE DATABASE IF NOT EXISTS ${db.name};`,
    `GRANT ALL ON ${db.name}.* TO ${db.user}@localhost IDENTIFIED BY '${db.pass}';`,
  ].join(" ");
  return execAsync(["mysql", "-Be", sql].reduce((a, v) => (a += `"${v}" `), ""));
}

async function setupSysD(config) {
  const [pcmsServ, webServ] = await Promise.all([
    fs.promises.readFile("./example/pcms.service", "utf-8"),
    fs.promises.readFile("./example/sspbrno-web.service", "utf-8"),
  ]);

  const changeGUID = (conf) =>
    conf.replace(/User=.+\n/, `User=${config.user}`).replace(/Group=.+\n/, `Group=${config.user}`);

  return Promise.all([
    fs.promises.writeFile(Path.join(config.path, "pcms.service"), changeGUID(pcmsServ), "utf-8"),
    fs.promises.writeFile(Path.join(config.path, "sspbrno-web.service"), changeGUID(webServ), "utf-8"),
  ]);
}

//

main().catch(console.error);
