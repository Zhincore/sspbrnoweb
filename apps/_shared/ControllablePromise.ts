type Resolve<T> = (value: T | PromiseLike<T>) => void;
type Reject = (reason: any) => void;

export default class ControllablePromise<T> extends Promise<T> {
  private _resolve?: Resolve<T>;
  private _reject?: Reject;

  constructor(executor?: (resolve: Resolve<T>, reject: Reject) => void) {
    let resolve: Resolve<T> = () => null;
    let reject: Reject = () => null;

    super((_resolve, _reject) => {
      resolve = _resolve;
      reject = _reject;
      if (executor) executor(_resolve, _reject);
    });

    this._resolve = resolve;
    this._reject = reject;
  }

  getController() {
    return (resolve: Resolve<T>, reject: Reject) => this.then(resolve, reject);
  }

  getListeners() {
    return [this.resolve.bind(this), this.reject.bind(this)];
  }

  resolve(...args: Parameters<Resolve<T>>): ReturnType<Resolve<T>> {
    this._resolve!(...args);
  }

  reject(...args: Parameters<Reject>): ReturnType<Reject> {
    this._reject!(...args);
  }
}
