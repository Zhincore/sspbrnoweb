import { extendTheme, ThemeConfig, ColorHues, ThemeOverride, Theme, ColorMode, StorageManager } from "@chakra-ui/react";
import { lighten } from "@chakra-ui/theme-tools";

const config: ThemeConfig = {
  initialColorMode: "system",
  useSystemColorMode: false,
};

export const colors: Record<string, Partial<ColorHues> | string> = {
  pgreen: generateColorScheme("#199F58"),
  pred: generateColorScheme("#E2001A"),
  pblue: generateColorScheme("#003E90"),
  pyellow: "#EACA24",

  bgLight: "#FFFFFF",
  bgDark: "#171923",
};

const themeOverride: ThemeOverride = {
  config,
  colors,
  semanticTokens: {
    colors: {
      bg: {
        default: "bgLight",
        _dark: "bgDark",
      },
      text: {
        default: "bgDark",
        _dark: "bgLight",
      },
    },
  },
  shadows: {
    outline: `0 0 0 3px ${colors.pgreen["200"]}`,
    widget: "0px 0px 10px 6px rgba(0, 0, 0, 0.25)",
  },
  fonts: {
    heading: '"Open Sans", "Helvetica Neue", Helvetica, Arial, system-ui, sans-serif',
    body: '"Open Sans", "Helvetica Neue", Helvetica, Arial, system-ui, sans-serif',
  },
  components: {
    Link: {
      baseStyle: {
        color: "pgreen.400",
        _hover: {
          color: "pgreen.200",
        },
      },
    },
    Switch: {
      defaultProps: {
        colorScheme: "pgreen",
      },
    },
    Button: {
      variants: {
        link: {
          _active: {
            color: "pgreen.500",
            _hover: {
              textDecoration: "none",
            },
          },
        },
      },
    },
    Input: {
      defaultProps: { focusBorderColor: "pgreen.200" },
    },
    FormLabel: {
      baseStyle: {
        fontWeight: "bold",
      },
    },
  },
  styles: {
    global: {
      body: {
        backgroundColor: "bg",
        color: "text",
        scrollbarColor: `${colors.pgreen[400]} ${colors.bgDark}`,
        scrollbarWidth: "thin",
      },
      code: {
        wordBreak: "break-word",
      },
    },
  },
};

export const theme: Theme = extendTheme(themeOverride) as Theme;

export const getLocalStorageManager = (storageKey = "chakra-ui-color-mode"): StorageManager => ({
  get(init) {
    const value = localStorage.getItem(storageKey) as ColorMode | undefined;
    return value ?? init;
  },
  set(value) {
    localStorage.setItem(storageKey, value);
  },
  type: "localStorage",
});

export const noStorageManager: StorageManager = {
  get(init) {
    return init;
  },
  set() {
    /**/
  },
  type: "localStorage",
};

export function generateColorScheme(color: string): ColorHues {
  const hues: (keyof ColorHues)[] = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
  const hueMidI = hues.indexOf(500);
  return hues.reduce((acc, w, i) => {
    const value = ((hueMidI - i) / hues.length) * (100 * 0.8);
    acc[w] = lighten(color, value)({});
    return acc;
  }, {} as ColorHues);
}

const subThemeOverride: any = {
  colors: {
    pgreen: colors.pred,
    pblue: colors.pred,
    pyellow: colors.pred[400],
  },
};
const subTheme = extendTheme(subThemeOverride, theme);

export function themeWithSubmode(_theme: any, submode: number) {
  if (submode > 3) return subTheme;
  return _theme;
}
