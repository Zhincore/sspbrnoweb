import { useState, useEffect, useCallback } from "react";

export default function usePromise<T>(
  getPromise: () => Promise<T>,
  autoFetch = true,
): [T | undefined, unknown, () => void] {
  const [data, setData] = useState<T>();
  const [error, setError] = useState<unknown>();

  const update = useCallback(async () => {
    setError(null);
    try {
      const _data = await getPromise();
      setData(_data);
    } catch (err) {
      console.error(err);
      setError(err);
    }
  }, []);

  useEffect(() => {
    if (autoFetch) update();
  }, []);

  return [data, error, update];
}
