import { FileRecord, MediaRecord } from "./APITypes";

export type TypesOfObj<Obj> = Obj[keyof Obj];
export type KeysOfType<Obj, Type> = {
  [Key in keyof Obj]-?: Obj[Key] extends Type ? Key : never;
}[keyof Obj extends string ? keyof Obj : never];

export function fileToMedia({ ...file }: FileRecord): MediaRecord {
  const media = file.media;
  if (!media) throw new Error("Trying to convert normal file to media");
  return { ...media, file: file };
}

export function mediaToFile({ ...media }: MediaRecord): FileRecord {
  const file = media.file;
  if (!file) throw new Error("Trying to convert media property to full file");
  return { ...file, media: media };
}
