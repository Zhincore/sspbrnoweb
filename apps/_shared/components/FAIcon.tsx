import { chakra, isStyleProp } from "@chakra-ui/react";
import { FontAwesomeIcon, FontAwesomeIconProps } from "@fortawesome/react-fontawesome";

type FAIconProps = Omit<FontAwesomeIconProps, "icon"> & { icon: string | FontAwesomeIconProps["icon"] };

export default chakra(
  function FAIcon({ icon, ...props }: FAIconProps) {
    return <FontAwesomeIcon {...props} icon={icon as any} />;
  },
  {
    shouldForwardProp: (prop) => {
      return !isStyleProp(prop) || ["transform", "mask"].includes(prop);
    },
  },
);
