import PrismaTypes from "./prisma";

// Filesystem
export type FolderRecord = PrismaTypes.Folder & { folders: FolderRecord[]; files: FileRecord[] };
export type FileRecord = PrismaTypes.File & { media: PrismaTypes.Media | null };
export type MediaRecord = PrismaTypes.Media & { file: PrismaTypes.File };
export type FileNodeRecord = FileRecord | FolderRecord;

// Content
export type Social = PrismaTypes.Social;
export type Slide = PrismaTypes.Slide;
export type General = PrismaTypes.General & {
  favicon: MediaRecord | null;
  logo: MediaRecord | null;
  cover: MediaRecord | null;
  projects: MediaRecord[];
  socials: Social[];
  slides: Slide[];
};
export type PostCategory = PrismaTypes.PostCategory;
export type Post = PrismaTypes.Post & {
  cover: MediaRecord | null;
  attachments: FileRecord[];
  media: MediaRecord[];
  category: PostCategory | null;
};
export type Course = PrismaTypes.Course;

export type ContentTypes = {
  general: General;
  post: Post;
  course: Course;
};

// Image Config
export interface ImageConfig {
  sizes: number[];
  exts: string[];
  defaultExt: string;
  keepExts: string[];
}

export interface ListingOptions {
  fields?: string[];
  include?: string[];
  where?: Record<string, any>; // TODO: Better typing
  orderBy?: any; // TODO: Better typing
  pageSize?: number | null;
  page?: number;
}
