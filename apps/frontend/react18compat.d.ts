import * as React from "react";

declare module "@chakra-ui/react" {
  export const LightMode: React.FunctionComponent<{
    children?: React.ReactNode;
  }>;
  export const DarkMode: React.FunctionComponent<{
    children?: React.ReactNode;
  }>;
}
