import { useContext } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { MediaRecord } from "$shared/APITypes";
import { PageContext, getMediaUrl } from "~/lib/server";

export type SEOMetaProps = {
  title?: string;
  description?: string;
  thumbnail?: MediaRecord;
};

export default function SEOMeta({ title, description, thumbnail }: SEOMetaProps) {
  const data = useContext(PageContext);
  const { asPath, basePath } = useRouter();

  const host = process.env.NEXT_PUBLIC_CANONICAL_URL ?? location.href;
  const baseUrl = new URL(basePath, host).href;
  const url = new URL(asPath, baseUrl).href;

  const _title = [title, data.general.shortname].filter((v) => v).join(" | ");
  const favicon = data.general.favicon ? getMediaUrl(data.general.favicon, false, data.submode) : undefined;

  const _descriptionSrc = description ?? data.general.description ?? "";
  const _description = _descriptionSrc.length > 255 ? _descriptionSrc.slice(0, 252) + "..." : _descriptionSrc;

  const _imageSrc = thumbnail ?? data.general.cover;
  const _image = _imageSrc ? getMediaUrl(_imageSrc) : undefined;

  const twitter = data.general.socials.find((v) => v.type === "twitter");

  return (
    <Head>
      <title>{_title}</title>
      <link rel="canonical" href={url} />
      {favicon && <link rel="icon" href={favicon} />}
      <meta name="description" content={_description} />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:url" content={url} />
      <meta name="twitter:title" content={_title} />
      <meta name="twitter:description" content={_description} />
      <meta name="twitter:image" content={_image} />
      {twitter?.name && <meta name="twitter:site" content={"@" + twitter.name.replace(/^@/, "")} />}

      <meta property="og:type" content="website" />
      <meta property="og:title" content={_title} />
      <meta property="og:description" content={_description} />
      <meta property="og:site_name" content={data.general.name} />
      <meta property="og:url" content={url} />
      <meta property="og:image" content={_image} />

      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "https://schema.org",
            "@type": "HighSchool",
            logo: data.general.logo ? getMediaUrl(data.general.logo) : undefined,
            image: data.general.cover ? getMediaUrl(data.general.cover) : undefined,
            name: data.general.name,
            address: data.general.address,
            url: baseUrl,
            telephone: data.general.phone,
            openingHoursSpecification: [
              {
                "@type": "OpeningHoursSpecification",
                dayOfWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
                opens: "8:00",
                closes: "14:00",
              },
            ],
          }),
        }}
      />
    </Head>
  );
}
