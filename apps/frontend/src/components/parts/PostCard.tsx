import { Box, BoxProps, VStack, HStack, Heading, Text, Tag, Button } from "@chakra-ui/react";
import { useTranslation } from "next-i18next";
import { Post } from "$shared/APITypes";
import { usePageContext, getBackendURL } from "~/lib/server";
import Link from "$elements/Link";
import Media from "$elements/Media";

type PostCardProps = BoxProps & {
  post: Post;
};

export default function PostCard({ post, ...props }: PostCardProps) {
  const data = usePageContext();
  const { t } = useTranslation();

  return (
    <VStack w={64} shadow="widget" rounded="lg" {...props} overflow="hidden" position="relative" spacing={0}>
      {post.category && (
        <Tag
          as={Link}
          href={`/posts/categories/${post.category.slug}`}
          color="white"
          textTransform="uppercase"
          fontWeight="600"
          position="absolute"
          top={0}
          left={0}
          px={4}
          rounded={0}
          roundedBottomRight="lg"
          bg={post.category.color}
        >
          {post.category.name}
        </Tag>
      )}

      <Media
        src={post.cover ?? data.general.cover ?? (getBackendURL("/images") as any)}
        htmlWidth={256}
        width={64}
        height={32}
        m={0}
        objectFit="cover"
      />

      <VStack p={4} flex={1} w="full" alignItems="flex-start">
        <Link href={`/posts/${post.slug}`}>
          <Heading as="h3" size="sm" color={post.category?.color} textAlign="left" textTransform="uppercase">
            {post.title}
          </Heading>
        </Link>

        <Text noOfLines={4} lineHeight="normal" textAlign="justify" fontSize="sm" flex={1}>
          {post.content}
        </Text>

        <HStack justifyContent="space-between" w="full">
          <Box color={post.category?.color}>
            {new Date(post.updatedAt).toLocaleDateString(undefined, { dateStyle: "medium" })}
          </Box>

          <Button as={Link} href={`/posts/${post.slug}`} rounded="full" size="sm" px={6}>
            {t("readMore")}
          </Button>
        </HStack>
      </VStack>
    </VStack>
  );
}
