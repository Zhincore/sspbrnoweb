const pBar = {
  content: '""',
  position: "absolute",
  left: 0,
  width: "full",
  height: 2,
  bgGradient: "linear(to-r, pgreen.500, pgreen.500 33%, pblue.500 33%, pblue.500 66%, pred.500 66%, pred.500)",
};
export default pBar;
