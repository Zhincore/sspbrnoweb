import { useContext } from "react";
import { FormControl, FormLabel, Switch, VisuallyHidden, useColorMode, useId } from "@chakra-ui/react";
import { useTranslation } from "next-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLightbulb as fasLightBulb } from "@fortawesome/free-solid-svg-icons";
import { faLightbulb as farLightBulb } from "@fortawesome/free-regular-svg-icons";
import { PageContext } from "~/lib/server";

export default function ColorModeToggle() {
  const id = useId(undefined, "colorModeToggle");
  const { t } = useTranslation();
  const { colorMode, toggleColorMode } = useColorMode();
  const { submode, setSubmode } = useContext(PageContext);

  return (
    <FormControl w="auto">
      <FormLabel htmlFor={id}>
        <VisuallyHidden>{t("darkMode")}</VisuallyHidden>

        <FontAwesomeIcon icon={fasLightBulb} size="lg" />
        <Switch
          id={id}
          colorScheme="pgreen"
          mx={3}
          size="lg"
          isChecked={colorMode === "dark"}
          onChange={toggleColorMode}
          onDoubleClick={() => setSubmode(submode + 1)}
        />
        <FontAwesomeIcon icon={farLightBulb} size="lg" />
      </FormLabel>
    </FormControl>
  );
}
