import { Stack, VStack, Wrap, Text, DarkMode } from "@chakra-ui/react";
import { PageContext } from "~/lib/server";
import Media from "$elements/Media";
import pBar from "../pBar";
import ContactInfo from "./ContactInfo";
import ColorModeToggle from "./ColorModeToggle";

export default function Footer({ data: { general } }: { data: PageContext<any> }) {
  return (
    <Stack
      as="footer"
      p={[4, 12]}
      pt={[6, 14]}
      bgColor="bgDark"
      color="white"
      position="relative"
      direction={["column", "row"]}
      _before={{ ...pBar, top: 0 }}
      justifyContent="space-between"
      spacing={6}
    >
      <DarkMode>
        <VStack alignItems="flex-start" spacing={12}>
          <Wrap alignItems="flex-start">
            {general.logo && (
              <Media src={general.logo} alt={general.shortname} htmlHeight={128} display="inline-block" mr={5} />
            )}
            <Text color="whiteAlpha.700">
              &copy; {new Date().getFullYear()} <br />
              {general.name}
            </Text>
          </Wrap>

          <ContactInfo general={general} />
        </VStack>
      </DarkMode>

      <ColorModeToggle />
    </Stack>
  );
}
