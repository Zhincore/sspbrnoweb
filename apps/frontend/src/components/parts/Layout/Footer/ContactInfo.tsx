import { Text, List, ListItem, ListIcon, Link, VisuallyHidden } from "@chakra-ui/react";
import { useTranslation } from "next-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt, faPhone, faAt, faArchive } from "@fortawesome/free-solid-svg-icons";
import { General } from "$shared/APITypes";
import useInBrowser from "~/hooks/useInBrowser";

export default function ContactInfo({ general }: { general: General }) {
  const inBrowser = useInBrowser();
  const { t } = useTranslation();

  return (
    <List spacing={4}>
      <ListItem>
        <ListIcon as={FontAwesomeIcon} icon={faMapMarkerAlt} fixedWidth color="pred.500" size="lg" />
        <VisuallyHidden>{t("address")}</VisuallyHidden>
        {general.address}
      </ListItem>
      <ListItem>
        <ListIcon as={FontAwesomeIcon} icon={faPhone} fixedWidth color="pgreen.500" size="lg" />
        <VisuallyHidden>{t("phone")}</VisuallyHidden>
        {general.phone}
      </ListItem>
      <ListItem>
        <Link href={inBrowser ? `mailto:${general.email}` : "#"}>
          <ListIcon as={FontAwesomeIcon} icon={faAt} fixedWidth color="pblue.600" size="lg" />
          <VisuallyHidden>{t("email")}</VisuallyHidden>
          {inBrowser ? general.email : "@"}
        </Link>
      </ListItem>
      <ListItem>
        <ListIcon as={FontAwesomeIcon} icon={faArchive} fixedWidth color="pyellow" size="lg" />
        <Text as="span" color="whiteAlpha.700">
          {t("dataBox")}
        </Text>
        {" " + general.dataBox}
      </ListItem>
    </List>
  );
}
