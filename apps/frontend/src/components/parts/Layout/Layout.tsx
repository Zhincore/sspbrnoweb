import { useContext } from "react";
import { Flex } from "@chakra-ui/react";
import { PageContext } from "~/lib/server";
import Header from "./Header";
import Footer from "./Footer";

type LayoutProps = {
  children?: React.ReactNode;
};

export default function Layout({ children }: LayoutProps) {
  const data = useContext(PageContext);

  return (
    <>
      <Flex direction="column" minH="100vh">
        <Header data={data} />
        <Flex flexGrow={1} direction="column">
          {children}
        </Flex>
      </Flex>

      <Footer data={data} />
    </>
  );
}
