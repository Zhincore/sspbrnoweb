import { useContext, useMemo } from "react";
import Link from "next/link";
import { MenuList, Button, MenuGroup, LightMode, List, ListItem } from "@chakra-ui/react";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "next-i18next";
import FAIcon from "$shared/components/FAIcon";
import { getCourseColorSchemeName } from "~/lib/util";
import { PageContext } from "~/lib/server";

export default function CoursesMenu() {
  const { courses } = useContext(PageContext);
  const { t } = useTranslation();

  const splitCourses = useMemo(() => {
    const mat = [];
    const appren = [];

    for (const course of courses) {
      if (course.maturita) mat.push(course);
      else appren.push(course);
    }

    return [mat, appren].filter((a) => a.length);
  }, [courses]);

  return (
    <MenuList px={4} pb={4} pt={0} rounded="xl">
      <LightMode>
        <List>
          {splitCourses.map((part, i) => (
            <ListItem key={i}>
              <MenuGroup
                title={t(i ? "apprenticeCourses" : "maturitaCourses")}
                color="gray.400"
                mx={0}
                mt={4}
                mb={2}
                fontSize="small"
              >
                <List>
                  {part.map((course) => (
                    <ListItem key={course.slug}>
                      <Link href={`/courses/${course.slug}`} passHref>
                        <Button
                          as="a"
                          textTransform="uppercase"
                          fontWeight="semibold"
                          px={4}
                          py={2}
                          my={1}
                          minH={8}
                          justifyContent="start"
                          rounded="lg"
                          position="relative"
                          zIndex={1}
                          w="full"
                          overflow="hidden"
                          leftIcon={
                            <FAIcon icon={course.icon} transform="shrink-8" fontSize="1.5rem" mask={faCircle} />
                          }
                          colorScheme={getCourseColorSchemeName(course)}
                        >
                          {course.name}
                        </Button>
                      </Link>
                    </ListItem>
                  ))}
                </List>
              </MenuGroup>
            </ListItem>
          ))}
        </List>
      </LightMode>
    </MenuList>
  );
}
