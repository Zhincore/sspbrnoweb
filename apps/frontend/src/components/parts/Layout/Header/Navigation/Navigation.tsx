import { useRouter } from "next/router";
import Link from "next/link";
import {
  Stack,
  StackItem,
  Box,
  Button,
  ButtonProps,
  Flex,
  VisuallyHidden,
  useDisclosure,
  Menu,
  MenuButton,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle, faBars, faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "next-i18next";
import MENU from "./NavigationMenu";

export default function Navigation() {
  const { t } = useTranslation();
  const router = useRouter();
  const { isOpen, onToggle, getButtonProps, getDisclosureProps } = useDisclosure();

  return (
    <Flex as="nav" role="navigation" alignItems="center">
      <Button display={["block", "none"]} size="lg" onClick={onToggle} {...getButtonProps()}>
        <VisuallyHidden>Menu</VisuallyHidden>
        <FontAwesomeIcon icon={faBars} />
      </Button>

      <Stack
        as="ul"
        bg="bg"
        direction={["column", "row"]}
        position={["absolute", "relative"]}
        top={["100%", "unset"]}
        left={0}
        right={0}
        zIndex={10}
        w={["100%", "unset"]}
        h={["unset", "100%"]}
        py={[4, 0]}
        px={[2, 0]}
        transformOrigin="top"
        transform="auto"
        scaleY={[isOpen ? 1 : 0, 1]}
        transition="transform .3s"
        alignItems="center"
        {...{ ...getDisclosureProps(), hidden: undefined }}
      >
        {MENU.map((item, i) => {
          const btnProps: ButtonProps = {
            py: 4,
            px: 6,
            w: ["100%", "unset"],
            variant: "ghost",
            size: "lg",
            textTransform: "uppercase",
            fontSize: "1.1rem",
            leftIcon: item.icon ? (
              <FontAwesomeIcon icon={item.icon} transform="shrink-8" fontSize="1.5rem" mask={faCircle} />
            ) : undefined,
          };

          return (
            <StackItem as="li" key={i} w={["100%", "unset"]}>
              {item.link ? (
                <Link href={item.link} passHref>
                  <Button
                    as="a"
                    flex={1}
                    isActive={(item.link === "/" && router.asPath === "/") || router.asPath.startsWith(item.link)}
                    {...btnProps}
                  >
                    <Box as="span" flex={1}>
                      {t(item.label)}
                    </Box>
                  </Button>
                </Link>
              ) : (
                <Menu isLazy>
                  <MenuButton
                    as={Button}
                    {...btnProps}
                    rightIcon={<FontAwesomeIcon icon={faCaretDown} />}
                    textAlign="left"
                  >
                    {t(item.label)}
                  </MenuButton>
                  {item.menuList}
                </Menu>
              )}
            </StackItem>
          );
        })}
      </Stack>
    </Flex>
  );
}
