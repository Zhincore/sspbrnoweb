import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { faHome, faGraduationCap, faNewspaper, faSchool } from "@fortawesome/free-solid-svg-icons";
import CoursesMenu from "./CoursesMenu";

type MenuItem = ({ link: string; menuList?: never } | { link?: never; menuList: React.ReactNode }) & {
  label: string;
  icon?: IconDefinition;
};

const NavigationMenu: MenuItem[] = [
  {
    label: "home",
    link: "/",
    icon: faHome,
  },
  {
    label: "courses",
    icon: faGraduationCap,
    menuList: <CoursesMenu />,
  },

  {
    label: "news",
    link: "/posts",
    icon: faNewspaper,
  },
  {
    label: "aboutSchool",
    link: "/about",
    icon: faSchool,
  },
];

export default NavigationMenu;
