import Link from "next/link";
import { Heading, Flex } from "@chakra-ui/react";
import { PageContext } from "~/lib/server";
import Media from "$elements/Media";
import pBar from "../pBar";
import Navigation from "./Navigation";

export default function Header({ data: { general } }: { data: PageContext<any> }) {
  return (
    <Flex
      as="header"
      bg="bg"
      left={0}
      top={0}
      zIndex={99}
      w="100%"
      px={6}
      pb={2}
      textTransform="uppercase"
      position="sticky"
      justifyContent="space-between"
      _after={{ ...pBar, bottom: 0 }}
    >
      <Link href="/">
        <a>
          <Heading as="h1" my={4} display="flex" alignItems="center" fontSize="2xl" fontWeight="600">
            {general.logo && (
              <Media src={general.logo} alt={general.shortname} htmlHeight={56} display="inline-block" mr={5} />
            )}
            {general.shortname}
          </Heading>
        </a>
      </Link>

      <Navigation />
    </Flex>
  );
}
