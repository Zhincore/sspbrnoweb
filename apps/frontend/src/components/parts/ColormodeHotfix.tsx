import { useEffect } from "react";
import { useColorMode } from "@chakra-ui/react";

// NOTE: pain
export default function ColormodeHotfix() {
  const { colorMode, setColorMode } = useColorMode();

  useEffect(() => {
    const wanted = localStorage.getItem("chakra-ui-color-mode");
    if (wanted && colorMode !== wanted) {
      setColorMode(wanted);
      setTimeout(() => setColorMode(wanted));
    }
  });

  return <></>;
}
