import { Container } from "@chakra-ui/react";
import SEOMeta, { SEOMetaProps } from "$parts/SEOMeta";

type PageProps = SEOMetaProps & {
  children?: React.ReactNode;
};

export default function Page({ children, ...props }: PageProps) {
  return (
    <Container as="main" mx="auto" my={[8, 16]} px={[2, 4]} maxWidth="container.xl">
      <SEOMeta {...props} />

      {children}
    </Container>
  );
}
