import { Link as ChakraLink, LinkProps as ChakraLinkProps } from "@chakra-ui/react";
import NextLink, { LinkProps as NextLinkProps } from "next/link";

type LinkProps = ChakraLinkProps & {
  href: NextLinkProps["href"];
  asPath?: NextLinkProps["as"];
  replace?: NextLinkProps["replace"];
  scroll?: NextLinkProps["scroll"];
  shallow?: NextLinkProps["shallow"];
  prefetch?: NextLinkProps["prefetch"];
  locale?: NextLinkProps["locale"];
};

export default function Link({ href, asPath, replace, scroll, shallow, prefetch, locale, ...props }: LinkProps) {
  return (
    <NextLink passHref {...{ href, asPath, replace, scroll, shallow, prefetch, locale }}>
      <ChakraLink {...props} />
    </NextLink>
  );
}
