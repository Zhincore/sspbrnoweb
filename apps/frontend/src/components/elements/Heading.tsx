import { Box, Heading as ChakraHeading, HeadingProps as ChakraHeadingProps } from "@chakra-ui/react";

type HeadingProps = ChakraHeadingProps & {
  icon?: React.ReactNode;
};

export default function Heading({ children, icon, ...props }: HeadingProps) {
  return (
    <ChakraHeading textTransform="uppercase" {...props} mb={4}>
      {icon}
      <Box ml={icon ? 4 : 0} display="inline-block">
        {children}
      </Box>
    </ChakraHeading>
  );
}
