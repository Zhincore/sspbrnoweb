/* eslint-disable sonarjs/cognitive-complexity */
import { useState, useCallback, useMemo, useEffect, forwardRef, useRef, useContext } from "react";
import { useInView } from "react-cool-inview";
import { Box, Image, BoxProps, useMergeRefs } from "@chakra-ui/react";
import { FileRecord, MediaRecord } from "$shared/APITypes";
import { getImage, getFileURL, mediaToFile, PageContext, getBackendURL } from "~/lib/server";

type Fill = { src: FileRecord | MediaRecord | string; htmlWidth?: never; htmlHeight?: never };
type WithJustWidth = { src: FileRecord | MediaRecord; htmlWidth: number; htmlHeight?: never };
type WithJustHeight = { src: FileRecord | MediaRecord; htmlWidth?: never; htmlHeight: number };
type WithHeightAndWidth = {
  src: string | FileRecord | MediaRecord;
  htmlWidth: number;
  htmlHeight: number;
};

type AsImage = {
  loading?: "lazy" | "eager";
  forceImage?: boolean;
  isVideo?: false;
  autoPlay?: never;
  controls?: never;
  muted?: never;
};
type AsVideo = {
  loading?: never;
  forceImage?: false;
  isVideo?: true;
  autoPlay?: boolean;
  controls?: boolean;
  muted?: boolean;
};

type MediaProps = (Fill | WithJustWidth | WithJustHeight | WithHeightAndWidth) &
  (AsImage | AsVideo) &
  Omit<BoxProps, "h" | "w"> & {
    alt?: string;
    className?: string;
    thumbnail?: boolean;
    spinner?: React.ReactNode;

    priority?: boolean;
    onLoad?: () => void;
  };

const emptyUrl = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";

export default forwardRef<HTMLImageElement & HTMLVideoElement, MediaProps>(function Media(
  {
    src: aSrc,
    onLoad,
    priority,
    width,
    height,
    htmlWidth,
    htmlHeight,
    thumbnail,
    spinner,
    alt,
    className,
    isVideo,
    forceImage,
    autoPlay,
    controls,
    muted,
    loading,
    ...props
  },
  outref,
) {
  const { imageConfig: conf, submode } = useContext(PageContext);
  // State
  const [loaded, setLoaded] = useState(false);
  const [wasViewed, setViewed] = useState(false);
  const { observe, inView } = useInView({
    onEnter: () => setViewed(true),
    threshold: 0.25,
  });
  const ref = useRef<HTMLImageElement & HTMLVideoElement>(null);
  const mergeRef = useMergeRefs<HTMLImageElement & HTMLVideoElement>(ref, outref);
  const show = priority || inView || wasViewed;
  const file = (() => {
    if (typeof aSrc === "string") return;
    if ("file" in aSrc) return mediaToFile(aSrc);
    return aSrc;
  })();

  // Scaling assistance
  if (file) [htmlWidth, htmlHeight] = getScaling(file, htmlWidth, htmlHeight);
  const size = Math.min(htmlWidth!, htmlHeight!);

  // onLoad functions
  const checkLoaded = useCallback(
    (instant = false) => {
      if (loaded || !ref.current) return;

      if (ref.current.complete || ref.current.readyState >= 2) {
        if (instant) setLoaded(true);
        else {
          window.requestAnimationFrame(() => window.requestAnimationFrame(() => setLoaded(true)));
        }

        if (onLoad) onLoad();
      }
    },
    [loaded, onLoad],
  );

  useEffect(() => observe(ref.current));

  // Trigger for too quickly loaded images
  useEffect(() => checkLoaded(true));

  const imgSize = useMemo(() => {
    if (!conf || typeof aSrc === "string") return undefined;

    let _size = size;
    let lastDistance: number | null = null;

    if (!conf.sizes.includes(size)) {
      // Find closes possible size
      for (const possibleSize of conf.sizes) {
        _size = possibleSize;
        const distance = Math.abs(possibleSize - size);
        if (lastDistance === null || distance < lastDistance) {
          lastDistance = distance;
        } else if (distance > lastDistance) {
          break;
        }
      }
    }

    return _size;
  }, [aSrc, size, conf]);

  // Prepare props
  const _isVideo = !forceImage && (isVideo || file?.media?.type === "VIDEO");

  const fileSrc = file ? getFileURL(file) : (aSrc as string);
  const posterSrc = file ? getFileURL(file, true) : undefined;
  const _a = fileSrc!.split(".");
  const ext = _a[_a.length - 1];
  const keep = submode > 6 || conf?.keepExts.includes(ext) || thumbnail === false;

  const viewVidSrc = show ? fileSrc : undefined;
  const viewImgSrc = (() => {
    if (!show) return emptyUrl;
    if (submode > 6) return getBackendURL("/images");
    if (!file) return aSrc as string;
    return getFileURL(file!, thumbnail);
  })();

  const _onLoad = () => checkLoaded(false);

  if (submode <= 6) {
    if (width && !height) height = "auto";
    else if (!width && height) width = "auto";
  }

  const sharedProps = {
    width: width ?? htmlWidth + "px",
    height: height ?? htmlHeight + "px",
    htmlWidth,
    htmlHeight,
    className,
    ...props,
  };

  return (
    <>
      {loaded || spinner}

      {(_isVideo && (
        <Box
          as="video"
          ref={mergeRef}
          poster={viewImgSrc}
          src={viewVidSrc}
          onLoad={_onLoad}
          autoPlay={autoPlay}
          controls={controls}
          muted={muted}
          {...sharedProps}
        >
          {alt}
        </Box>
      )) ||
        ((keep || !file) && (
          <Image
            ref={mergeRef}
            src={viewImgSrc ?? fileSrc}
            alt={alt ?? ""}
            onLoad={_onLoad}
            loading={loading ?? "lazy"}
            {...sharedProps}
          />
        )) || (
          <picture>
            {imgSize && (
              <>
                <source srcSet={getImage(file!.id, imgSize)} />
                {conf.exts.map((aext) => (
                  <source key={aext} type={"image/" + aext} srcSet={getImage(file!.id, imgSize, aext)} />
                ))}
              </>
            )}
            <Image
              ref={mergeRef}
              src={viewImgSrc ?? (imgSize ? getImage(file!.id, imgSize, "png") : "")}
              alt={alt ?? ""}
              onLoad={_onLoad}
              loading={loading ?? "lazy"}
              {...sharedProps}
            />
          </picture>
        )}

      {!wasViewed && (
        <noscript>
          {_isVideo ? (
            <video poster={posterSrc} src={fileSrc}>
              {alt}
            </video>
          ) : (
            <img src={fileSrc} alt={alt ?? ""} />
          )}
        </noscript>
      )}
    </>
  );
});

export function getScaling(file: FileRecord, width?: number, height?: number) {
  if (file.media === null) throw new Error("Media element was given a non-media file");

  const ratio = file.media.width / file.media.height;
  if (height === undefined && width !== undefined) {
    height = width / ratio;
  } else if (width === undefined && height !== undefined) {
    width = height * ratio;
  }

  return [width ?? file.media.width, height ?? file.media.height];
}
