import { Box } from "@chakra-ui/react";
import Heading from "./Heading";

type PanelProps = {
  children?: React.ReactNode;
} & ({ title: React.ReactNode; icon?: React.ReactNode } | { title?: never; icon?: never });

export default function Panel({ children, title, icon }: PanelProps) {
  return (
    <Box
      rounded="xl"
      py={4}
      px={[6, 8]}
      my={8}
      shadow="widget"
      position="relative"
      zIndex={2}
      overflow="hidden"
      _before={{
        content: '""',
        position: "absolute",
        left: 0,
        top: 0,
        height: "full",
        width: 3,
        bgGradient: "linear(to-b, pgreen.500, pgreen.500 33%, pblue.500 33%, pblue.500 66%, pred.500 66%, pred.500)",
      }}
    >
      {title && <Heading icon={icon}>{title}</Heading>}
      {children}
    </Box>
  );
}
