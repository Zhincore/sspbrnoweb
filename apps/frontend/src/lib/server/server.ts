import qs from "qs";
import { FileRecord, MediaRecord, ContentTypes, ListingOptions } from "$shared/APITypes";
import { mediaToFile } from "$shared/utils";

export { fileToMedia, mediaToFile } from "$shared/utils";

const getPlaceHolderData = () => import("./placeholderData").then((m) => m.default);

export type ImageConfig = {
  sizes: number[];
  exts: string[];
  defaultExt: string;
  keepExts: string[];
};

export type PageData<TData> = {
  general: ContentTypes["general"];
  courses: ContentTypes["course"][];
  imageConfig: ImageConfig;
  page: TData;
};

export function getBackendURL(path = "") {
  return `${process.env.NEXT_PUBLIC_BACKEND_URL}${path}`;
}

async function fetchServer(url: string): Promise<any | null> {
  if (process.env.CI) return;
  const response = await fetch(getBackendURL(url));
  if (!response.ok) throw new Error(await response.text());
  return response.json();
}

export async function fetchPageData<TModelName extends keyof ContentTypes>(
  model?: TModelName,
  idOrListOptions?: string | ListingOptions,
): Promise<PageData<ContentTypes[TModelName] | null>> {
  if (process.env.CI) return getPlaceHolderData();

  let id = "";
  if (typeof idOrListOptions === "string") id = idOrListOptions;
  else if (idOrListOptions) id = "?" + qs.stringify(idOrListOptions);

  const [general, courses, imageConfig, page] = await Promise.all([
    fetchServer("/content/general/1"),
    fetchServer("/content/course"),
    fetchServer("/images/config"),
    model ? fetchServer(`/content/${model}/${id ?? ""}`) : null,
  ]);

  return {
    general,
    courses,
    imageConfig,
    page,
  };
}

export function getMediaUrl(media: MediaRecord, poster?: boolean, submode?: number) {
  return submode && submode > 6 ? getBackendURL("/images") : getFileURL(mediaToFile(media), poster);
}

export function getFileURL(file: FileRecord, poster?: boolean) {
  poster = poster && file.media?.type === "VIDEO";
  return getBackendURL("/files/" + (file.id + "." + (poster ? ".poster.png" : file.ext)));
}

export function getImage(fileId: string, size: number, ext?: string) {
  return getBackendURL("/images/" + size + "/" + fileId + (ext ? "." + ext : ""));
}
