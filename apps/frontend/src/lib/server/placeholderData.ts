import type { PageData } from "./server";

const getDate = () => new Date(Math.random() * Date.now());

const placeholderData: PageData<null> = {
  general: {
    id: 1,
    name: "",
    shortname: "",
    address: "",
    phone: "",
    email: "",
    dataBox: "",
    description: null,
    updatedAt: getDate(),
    cover: null,
    coverMedia: null,
    logo: null,
    logoMedia: null,
    favicon: null,
    faviconFile: null,
    socials: [],
    slides: [],
    projects: [],
  },
  courses: [],
  imageConfig: { sizes: [32, 64, 128, 256, 512], exts: ["avif", "webp", "jpg"], defaultExt: "webp", keepExts: ["svg"] },
  page: null,
};

export default placeholderData;
