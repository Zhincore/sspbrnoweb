import { createContext, useContext } from "react";
import { PageData } from "./server";

export type PageContext<T> = PageData<T> & {
  submode: number;
  setSubmode: (submode: number) => void;
};

export const PageContext = createContext<PageContext<any>>({} as PageContext<any>);

export const usePageContext = <T = null>() => useContext<PageContext<T>>(PageContext);
