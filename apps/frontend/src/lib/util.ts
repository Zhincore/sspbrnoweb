import { ColorHues } from "@chakra-ui/react";
import { Course } from "$shared/APITypes";
import { generateColorScheme } from "$shared/theme";

export function getCourseColorSchemeName(course: Course) {
  return `__course_${course.slug}`;
}

export function generateColorSchemesForCourses(courses: Course[]) {
  return courses.reduce((acc, course) => {
    if (course.color) {
      acc[getCourseColorSchemeName(course)] = generateColorScheme(course.color);
    }
    return acc;
  }, {} as Record<string, ColorHues>);
}
