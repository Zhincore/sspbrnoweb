import { GetStaticProps } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import type { SSRConfig } from "next-i18next";
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { ContentTypes } from "$shared/APITypes";
import { PageData, fetchPageData } from "./server";

export type PageProps<TData> = PageData<TData> & SSRConfig & { _icons: IconDefinition[] };

export function createGetStaticProps<TModelName extends keyof ContentTypes>(
  model?: TModelName,
  idOrListOptions?: Parameters<typeof fetchPageData>[1],
): GetStaticProps<PageProps<any>> {
  return async function getStaticProps({ locale, defaultLocale }) {
    const [data, lang] = await Promise.all([
      fetchPageData(model, idOrListOptions),
      serverSideTranslations(locale ?? defaultLocale!),
    ]);

    const icons = gatherNeededIcons(data, model);
    const iconDefs = await preloadIcons(icons);

    return {
      props: { ...lang, ...data, _icons: iconDefs },
      notFound: model ? !data.page : false,
      revalidate: 60,
    };
  };
}

const iconExtractors: { [TModel in keyof ContentTypes]?: (data: ContentTypes[TModel]) => string[] } = {
  course: (course) => [course.icon],
};

function gatherNeededIcons<TModel extends keyof ContentTypes>(
  pageData: PageData<ContentTypes[TModel] | ContentTypes[TModel][] | null>,
  model?: TModel,
) {
  const icons = new Set<string>();

  const contents: [keyof ContentTypes, any[]][] = [
    ["general", [pageData.general]],
    ["course", pageData.courses],
  ];
  if (model && pageData.page) contents.push([model, Array.isArray(pageData.page) ? pageData.page : [pageData.page]]);

  // Collect icons
  for (const [submodel, data] of Object.values(contents)) {
    const extractor = iconExtractors[submodel];
    if (!extractor) continue;

    for (const item of data) {
      const itemIcons = extractor(item);
      for (const icon of itemIcons) {
        icons.add(icon);
      }
    }
  }

  return Array.from(icons);
}

async function preloadIcons(icons: string[]) {
  const iconDefs: Promise<IconDefinition>[] = [];

  for (const icon of icons) {
    const match = icon.match(/^(?:fa-)?([\w-]+) fa-([\w-]+)$/);
    if (!match) continue;
    const [_icon, pack, iconName] = match;
    const filename = iconName[0].toUpperCase() + iconName.slice(1).replace(/-([a-z])/g, (_, s) => s.toUpperCase());

    let _pack = pack;
    if (pack.startsWith("fa")) {
      _pack = {
        s: "solid",
        r: "regular",
        b: "brands",
      }[pack[2]]!;
    }

    iconDefs.push(
      import(
        /* webpackIgnore: true */
        `@fortawesome/free-${_pack}-svg-icons/fa${filename}`
      ).then(
        (m) => m.definition,
        () => null,
      ),
    );
  }

  return Promise.all(iconDefs);
}
