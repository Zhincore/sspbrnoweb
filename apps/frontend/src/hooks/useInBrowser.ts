import { useState, useEffect } from "react";

export default function useInBrowser() {
  const [inBrowser, setInBrowser] = useState(false);

  useEffect(() => {
    setInBrowser(typeof document !== undefined);
  }, []);

  return inBrowser;
}
