import { NextApiHandler } from "next";

const allowedAddress = process.env.WEBHOOK_ADDRESS || "127.0.0.1";

const contentWebhook: NextApiHandler = async (req, res) => {
  const address = req.socket.address();
  if (!("address" in address) || address.address !== allowedAddress) {
    return res.status(403).send("Forbidden");
  }

  try {
    await res.unstable_revalidate("/");
    return res.send("Success");
  } catch (err) {
    // If there was an error, Next.js will continue
    // to show the last successfully generated page
    return res.status(500).send("Error revalidating");
  }
};

export default contentWebhook;
