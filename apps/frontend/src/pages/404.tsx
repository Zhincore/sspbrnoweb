import { Box } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPersonThroughWindow } from "@fortawesome/free-solid-svg-icons";
import { useTranslation, Trans } from "next-i18next";
import Page from "$parts/Page";
import Heading from "$elements/Heading";
import Link from "$elements/Link";
import { createGetStaticProps } from "~/lib/createGetStaticProps";

export default function Error404() {
  const { t } = useTranslation();

  return (
    <Page>
      <Heading icon={<FontAwesomeIcon icon={faPersonThroughWindow} transform="down-1" />}>{t("pageNotFound")}</Heading>

      <Box my={4} fontSize="xl">
        <Trans i18nKey="goToHomepage">
          Jděte <Link href="/">domů</Link>.
        </Trans>
      </Box>
    </Page>
  );
}

export const getStaticProps = createGetStaticProps();
