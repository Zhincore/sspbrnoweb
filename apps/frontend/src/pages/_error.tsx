import { Heading } from "@chakra-ui/react";
import { createGetStaticProps } from "~/lib/createGetStaticProps";

export default function Error() {
  return <Heading>Chyba</Heading>;
}

export const getStaticProps = createGetStaticProps();
