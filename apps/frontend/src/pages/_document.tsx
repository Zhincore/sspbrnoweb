import { ColorModeScript } from "@chakra-ui/react";
import NextDocument, { Html, Head, Main, NextScript } from "next/document";
import { theme, colors } from "$shared/theme";

export default class Document extends NextDocument {
  render() {
    return (
      <Html>
        <Head>
          <meta key="theme-color" name="theme-color" content={colors.pgreen[400]} />
        </Head>
        <body>
          <ColorModeScript initialColorMode={theme.config.initialColorMode} />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
