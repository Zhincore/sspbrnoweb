import { useState, useMemo, useRef } from "react";
import { AppProps } from "next/app";
import { appWithTranslation } from "next-i18next";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { config as faConfig, library } from "@fortawesome/fontawesome-svg-core";
import { theme, themeWithSubmode } from "$shared/theme";
import Layout from "$parts/Layout";
import { PageContext } from "~/lib/server";
import type { PageProps } from "~/lib/createGetStaticProps";
import { generateColorSchemesForCourses } from "~/lib/util";
import ColormodeHotfix from "$parts/ColormodeHotfix";

import "@fortawesome/fontawesome-svg-core/styles.css";
import "@fontsource/open-sans/400.css";
import "@fontsource/open-sans/600.css";
import "@fontsource/open-sans/700.css";

faConfig.autoAddCss = false;

export default appWithTranslation(function CustomApp({ Component, pageProps }: AppProps) {
  const pageData: PageProps<any> = pageProps;
  const [submode, setSubmode] = useState(0);
  const libraryInitRef = useRef(false);

  if (!libraryInitRef.current) {
    library.add(...pageData._icons);
    libraryInitRef.current = true;
  }

  const pageContext = useMemo(() => ({ ...pageData, submode, setSubmode }), [pageData, submode]);
  const themeWithCourses = useMemo(
    () => extendTheme({ colors: generateColorSchemesForCourses(pageData.courses) }, themeWithSubmode(theme, submode)),
    [pageData, submode],
  );

  return (
    <ChakraProvider theme={themeWithCourses}>
      <ColormodeHotfix />

      <PageContext.Provider value={pageContext}>
        <Layout>
          <Component {...(pageContext as any)} />
        </Layout>
      </PageContext.Provider>
    </ChakraProvider>
  );
});
