import { Flex, FlexProps, Button, Wrap, LightMode } from "@chakra-ui/react";
import Link from "next/link";
import { motion } from "framer-motion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserGraduate, faNewspaper, faCircle } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "next-i18next";
import { Post } from "$shared/APITypes";
import FAIcon from "$shared/components/FAIcon";
import { createGetStaticProps } from "~/lib/createGetStaticProps";
import { PageContext } from "~/lib/server";
import { getCourseColorSchemeName } from "~/lib/util";
import Media from "$elements/Media";
import Page from "$parts/Page";
import Panel from "$elements/Panel";
import PostCard from "$parts/PostCard";

const MotionFlex = motion<FlexProps>(Flex);

export default function Index(data: PageContext<Post[]>) {
  const { t } = useTranslation();

  return (
    <>
      <Flex as="header" position="relative" minH="75vh" alignItems="end" py={[4, 9]} px={[4, 16]}>
        {data.general.cover && (
          <Media
            src={data.general.cover}
            thumbnail={false}
            priority
            position="absolute"
            inset={0}
            zIndex={-1}
            width="100%"
            height="100%"
            objectFit={data.submode ? "fill" : "cover"}
          />
        )}

        <MotionFlex
          initial={{ opacity: 0, y: 100 }}
          animate={{ opacity: 1, y: 0 }}
          w="full"
          overflowX="auto"
          alignItems="center"
          justifyContent="space-between"
          bg="bgLight"
          px={8}
          rounded="xl"
        >
          {data.general.projects.map((projectLogo) => (
            <Media key={projectLogo.id} src={projectLogo} htmlHeight={64} height="64px" maxW="unset" m={6} />
          ))}
        </MotionFlex>
      </Flex>

      <Page>
        <Panel title={t("courses")} icon={<FontAwesomeIcon icon={faUserGraduate} />}>
          <Flex justifyContent="center" wrap="wrap">
            <LightMode>
              {data.courses.map((course) => (
                <Link href={`/courses/${course.slug}`} passHref key={course.slug}>
                  <Button
                    as="a"
                    colorScheme={getCourseColorSchemeName(course)}
                    textTransform="uppercase"
                    display="flex"
                    flexDirection="column"
                    justifyContent="center"
                    textAlign="center"
                    alignItems="center"
                    whiteSpace="normal"
                    p={4}
                    m={2}
                    w={[32, 48]}
                    h={[32, 48]}
                  >
                    <FAIcon
                      icon={course.icon}
                      transform="shrink-8"
                      fontSize={["4rem", "6rem"]}
                      mb={4}
                      mask={faCircle}
                    />
                    {course.name}
                  </Button>
                </Link>
              ))}
            </LightMode>
          </Flex>
        </Panel>

        <Panel title={t("news")} icon={<FontAwesomeIcon icon={faNewspaper} />}>
          <Wrap alignItems="stretch" justify="center" spacing={4}>
            {data.page.map((post) => (
              <PostCard key={post.slug} post={post} as="li" />
            ))}
          </Wrap>
        </Panel>
      </Page>
    </>
  );
}

export const getStaticProps = createGetStaticProps("post", { pageSize: 4, include: ["category", "cover"] });
