const isDev = process.env.NODE_ENV !== "production";

/** @type {import('next-i18next').UserConfig} */
module.exports = {
  i18n: {
    defaultLocale: "cs",
    locales: ["cs"],
  },
  localeExtension: "yml",
  saveMissing: isDev,
  reloadOnPrerender: isDev,
};
