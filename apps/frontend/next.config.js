/* eslint-disable @typescript-eslint/no-var-requires */
const analyze = process.env.ANALYZE === "true";
const production = process.env.NODE_ENV === "production";

const NEXT = require("next/constants");
const withPWA = require("next-pwa");
const withTM = require("next-transpile-modules")(["$shared"]);
const withBundleAnalyzer = analyze
  ? require("@next/bundle-analyzer")({
      enabled: analyze,
    })
  : (a) => a;

const { i18n } = require("./next-i18next.config");

module.exports = (phase) => {
  /**
   * @type {import('next').NextConfig}
   */
  const config = {
    i18n,
    poweredByHeader: false,
    reactStrictMode: true,
    trailingSlash: true,
    compress: false, // NGINX should do this
    basePath: process.env.NEXT_PUBLIC_BASE_PATH ?? "",
    eslint: {
      ignoreDuringBuilds: production,
    },
    typescript: {
      ignoreBuildErrors: production, // speed
    },
    pwa: {
      disable: phase === NEXT.PHASE_DEVELOPMENT_SERVER,
      dest: "public",
    },
  };
  return withBundleAnalyzer(withPWA(withTM(config)));
};
