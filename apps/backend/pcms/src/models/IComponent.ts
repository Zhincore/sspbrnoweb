export default interface IComponent<ScalarEnum extends string> {
  idField: ScalarEnum;
  include?: string[];
}
