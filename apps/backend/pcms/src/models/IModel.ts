import type { PrismaClient } from "$shared/prisma";
import IComponent from "./IComponent";

export default interface IModel<ScalarEnum extends string> extends IComponent<ScalarEnum> {
  components?: Record<string, IComponent<any>>;

  findMany(this: PrismaClient, query: Query): Promise<any>;
  findOne<TMode extends "custom" | never>(
    this: PrismaClient,
    query: TMode extends "custom" ? GetQueryCustom : GetQuery,
  ): Promise<any>;
  update(this: PrismaClient, query: UpdateQuery): Promise<any>;
  delete(this: PrismaClient, query: DeleteQuery): Promise<any>;
}

export interface ISingleton<ScalarEnum extends string> extends IModel<ScalarEnum> {
  singleton: true;
}

export interface IMultiton<ScalarEnum extends string> extends IModel<ScalarEnum> {
  singleton?: false;
  create(this: PrismaClient, query: CreateQuery): Promise<any>;
}

//

type Select = Record<string, true | { include: Record<string, true> }>;

export interface Query {
  take?: number;
  skip?: number;
  select?: Select;
  orderBy?: any;
  where?: any;
}

export interface CreateQuery {
  select?: Select;
  data: any;
}

export interface GetQuery {
  include?: Select;
  where?: any;
}
export interface GetQueryCustom {
  select?: Select;
  where?: any;
}

export interface UpdateQuery {
  data: any;
  where: any;
  select?: Select;
}

export interface DeleteQuery {
  where: any;
  select?: Select;
}
