import type { Prisma } from "$shared/prisma";
import { IMultiton } from "./IModel";
import Media from "./components/media";
import File from "./components/file";
import PostCategory from "./postcategory";

const post: IMultiton<Prisma.PostScalarFieldEnum> = {
  idField: "slug",

  components: {
    cover: Media,
    attachments: File,
    media: Media,
    category: PostCategory,
  },
  include: ["cover", "attachments", "media", "category"],

  findMany(query) {
    return this.post.findMany(query);
  },
  create(query) {
    return this.post.create(query);
  },
  findOne(query) {
    return this.post.findFirst(query);
  },
  async update(query) {
    const result = await this.post.update(query);

    // Delete unreferenced categories
    await this.postCategory.deleteMany({ where: { posts: { none: {} } } });

    return result;
  },
  delete(query) {
    return this.post.delete(query);
  },
};

export default post;
