import type { Prisma } from "$shared/prisma";
import { ISingleton } from "./IModel";
import Media from "./components/media";

const general: ISingleton<Prisma.GeneralScalarFieldEnum> = {
  idField: "id",
  singleton: true,

  components: {
    logo: Media,
    favicon: Media,
    cover: Media,
    projects: Media,
  },
  include: ["logo", "favicon", "cover", "socials", "projects"],

  findMany(query) {
    return this.general.findMany(query);
  },
  findOne(query) {
    return this.general.findFirst(query);
  },
  update(query) {
    return this.general.update(query);
  },
  delete(query) {
    return this.general.delete(query);
  },
};

export default general;
