import type { Prisma } from "$shared/prisma";
import type { IMultiton } from "./IModel";

const postCategory: IMultiton<Prisma.PostCategoryScalarFieldEnum> = {
  idField: "slug",

  findMany(query) {
    return this.postCategory.findMany(query);
  },
  create(query) {
    return this.postCategory.create(query);
  },
  findOne(query) {
    return this.postCategory.findFirst(query);
  },
  update(query) {
    return this.postCategory.update(query);
  },
  delete(query) {
    return this.postCategory.delete(query);
  },
};
export default postCategory;
