import type { Prisma } from "$shared/prisma";
import type IComponent from "../IComponent";

const media: IComponent<Prisma.MediaScalarFieldEnum> = {
  idField: "id",
  include: ["file"],
};
export default media;
