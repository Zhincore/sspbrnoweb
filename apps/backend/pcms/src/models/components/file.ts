import type { Prisma } from "$shared/prisma";
import type IComponent from "../IComponent";

const file: IComponent<Prisma.FileScalarFieldEnum> = {
  idField: "id",
  include: ["media"],
};
export default file;
