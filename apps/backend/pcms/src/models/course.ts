import type { Prisma } from "~/services/prisma/prisma.service";
import { IMultiton } from "./IModel";
import Media from "./components/media";

const course: IMultiton<Prisma.CourseScalarFieldEnum> = {
  idField: "slug",

  components: {
    media: Media,
  },
  include: ["media"],

  findMany(query) {
    return this.course.findMany(query);
  },
  create(query) {
    return this.course.create(query);
  },
  findOne(query) {
    return this.course.findFirst(query);
  },
  update(query) {
    return this.course.update(query);
  },
  delete(query) {
    return this.course.delete(query);
  },
};

export default course;
