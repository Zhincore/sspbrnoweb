import fs from "fs";
import type { Server } from "http";
import "systemd";
import { addExitCallback } from "catch-exit";
import { NestFactory } from "@nestjs/core";
import { Logger } from "@nestjs/common";
import { yellow } from "@nestjs/common/utils/cli-colors.util.js";
import { ExpressAdapter, NestExpressApplication } from "@nestjs/platform-express";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import cors from "cors";
import { ConfigService } from "./services/config/config.service";
import { PrismaService } from "./services/prisma/prisma.service";
import { AppModule } from "./api/app.module";
import { AutoClose } from "./utils/AutoClose";
import { CustomSocketIoAdapter } from "./utils/CustomSocketIoAdapter";
import { PrismaExceptionFilter } from "./utils/prisma-error.filter";

process.title = "pcms";

const logger = new Logger("bootstrap");

bootstrap();

async function bootstrap() {
  // INITIALIZE APP
  const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter());
  const config = app.get(ConfigService);
  const prismaService = app.get(PrismaService);

  const useSystemd = !!process.env.LISTEN_PID;
  const autoClose = useSystemd && config.autoExit ? new AutoClose(app, config.autoExit) : undefined;

  // Hooks
  handleExit(app, config);
  prismaService.enableShutdownHooks(app);

  // Config
  app.useGlobalFilters(new PrismaExceptionFilter());
  app.useWebSocketAdapter(new CustomSocketIoAdapter(app, autoClose));
  app.setGlobalPrefix(config.baseUrl);

  /// REGISTER PLATFROM PLUGINS
  app.use(
    cors({
      origin: config.security.allowedOrigins,
    }),
  );

  // Setup auto exit for on-demand
  if (autoClose) {
    app.use(autoClose.getMiddleware());
  }

  /// INITIALIZE DOCS
  const docBuilder = new DocumentBuilder()
    .setTitle(config.app.name)
    .setDescription(config.app.description)
    .setVersion(config.app.version)
    .build();
  const document = SwaggerModule.createDocument(app, docBuilder);
  SwaggerModule.setup(config.baseUrl, app, document);

  /// START
  if (useSystemd) {
    // Listen on SystemD socket
    await app.listen("systemd");
  } else if (config.socket) {
    // Listen on unix socket
    await app.listen(config.socket);
    if (config.socketPublic) await fs.promises.chmod(config.socket, 0o0766);
  } else {
    // Listen on a port
    await app.listen(config.port, config.host);
  }

  logger.log(`Listening on ` + yellow(await app.getUrl()));
}

//
function handleExit(app: NestExpressApplication, config: ConfigService) {
  addExitCallback((signal) => {
    // Cleanup
    logger.log("Exitting...");

    if (signal !== "exit") {
      app.close();
    }

    // Close the server
    const server: Server = app.getHttpServer();
    server.close();

    // Delete our temporary folder if it exists
    if (fs.existsSync(config.tmpPath)) {
      try {
        fs.rmSync(config.tmpPath, { recursive: true });
      } catch (e) {
        logger.warn(e, "Failed to delete tmp folder");
      }
    }
  });
}
