import Path from "path";
import { randomBytes } from "crypto";
import ms from "ms";
import type sharp from "sharp";
import { config as dotenv } from "dotenv";
import { expand } from "dotenv-expand";
import { JwtSignOptions, JwtVerifyOptions } from "@nestjs/jwt";

let config: Config;

expand(dotenv());

const getConfig = () => new Config();
export default getConfig;

export class Config {
  // Server settings
  socket = process.env.PCMS_SOCKET || null;
  socketPublic = process.env.PCMS_SOCKET_PUBLIC?.toLowerCase() === "true";
  port = process.env.PCMS_PORT ? parseInt(process.env.PCMS_PORT) : 6532;
  host = process.env.PCMS_HOST || "localhost";
  autoExit = ms(process.env.PCMS_AUTO_EXIT ? process.env.PCMS_AUTO_EXIT : "0");

  // Routing settings
  baseUrl = process.env.PCMS_BASE_URL ?? "/api";
  wsUrl = Path.join(this.baseUrl, process.env.PCMS_WS_URL ?? "/socket");

  // Admin Panel
  adminPanel?: AdminPanelConfig = !process.env.DISABLE_ADMIN_PANEL
    ? {
        path: "../admin/dist",
        url: process.env.ADMIN_BASE_URL ?? "/admin",
      }
    : undefined;

  //
  // GENERAL SETTINGS
  //
  app: ConfigAppBranding = {
    name: "PCMS", // process.env.npm_package_name
    description: process.env.npm_package_description ?? "",
    version: process.env.npm_package_version ?? "0.0.1",
  };

  security: ConfigSecurity = {
    jwtSecret: process.env.JWT_SECRET || randomBytes(32), // NOTE: 32 bytes = 256 bits for HS256 algorithm
    jwtSignOptions: {
      expiresIn: process.env.JWT_EXPIRES || "32h",
      algorithm: "HS256",
    },
    jwtVerifyOptions: {
      algorithms: ["HS256"],
    },
    corp: (process.env.CORP as any) || "same-site",
    allowedOrigins: process.env.SEC_ORIGIN ? process.env.SEC_ORIGIN.split(/, ?/) : undefined,
    // Default user settngs
    defaultUser: {
      login: process.env.DEFAULT_LOGIN || "purkynka",
      password: process.env.DEFAULT_PASSWORD || "rielae6ux3rofaTi",
    },
  };

  //
  // CORE SETTINGS
  //

  // Database settings
  databaseUrl = process.env.DATABASE_URL;

  // File path settings
  publicCachePath = process.env.PUBLIC_CACHE_PATH || "../../../data/cache/";
  uploadPath = process.env.UPLOAD_PATH || "../../../data/files/";
  tmpPath = "../../../data/.tmp/";

  //
  // SERVICE SETTINGS
  //

  // Content
  content: ConfigContent = {
    defaultPageSize: 16,
    webhookUrls: process.env.CONTENT_WEBHOOK_URLS ? process.env.CONTENT_WEBHOOK_URLS.split(/, ?/) : [],
    webhookDebounce: 15_000,
  };

  // Image optimization settings
  images: ConfigImageOptimization = {
    videoThumbSize: process.env.VIDEO_THUMB_SIZE ? parseInt(process.env.VIDEO_THUMB_SIZE) : 720,
    sizes: [32, 64, 128, 256, 512],
    sizing: "outside",
    keepExts: ["svg"],
    allowedExts: ["avif", "webp", "jpg"],
    autoformat: process.env.THUMB_AUTOFORMAT?.toLowerCase() !== "false",
  };

  constructor() {
    if (config) return config;
    config = this; // eslint-disable-line @typescript-eslint/no-this-alias
  }
}

export const getWebsocketConfig = () => {
  const _config = getConfig();
  return { path: _config.wsUrl, transports: ["websocket", "polling"] };
};

type AdminPanelConfig = {
  path: string;
  url: string;
};

type ConfigAppBranding = {
  name: string;
  description: string;
  version: string;
};

type ConfigSecurity = {
  jwtSecret: string | Buffer;
  jwtSignOptions: JwtSignOptions;
  jwtVerifyOptions: JwtVerifyOptions;
  corp: "same-site" | "same-origin" | "cross-origin";
  allowedOrigins?: string[];
  allowedHosts?: string[];
  defaultUser: ConfigDefaultUser;
};

type ConfigContent = {
  defaultPageSize: number;
  webhookUrls: string[];
  webhookDebounce: number;
};

type ConfigDefaultUser = {
  login: string;
  password: string;
};

type ConfigImageOptimization = (
  | { defaultExt?: never; autoformat: boolean }
  | { defaultExt: keyof sharp.FormatEnum; autoformat?: never }
) & {
  videoThumbSize: number;
  sizes: number[];
  sizing: "inside" | "outside";
  keepExts: string[];
  allowedExts: (keyof sharp.FormatEnum)[];
};
