import { Module } from "@nestjs/common";
import { AuthModule } from "~/api/auth/auth.module";
import { SocketGateway } from "./socket.gateway";

@Module({
  imports: [AuthModule],
  providers: [SocketGateway],
})
export class SocketModule {}
