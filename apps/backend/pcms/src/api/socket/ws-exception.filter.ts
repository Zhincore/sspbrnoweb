import { Catch, ArgumentsHost, HttpException, Logger } from "@nestjs/common";
import { BaseWsExceptionFilter } from "@nestjs/websockets";
import { Prisma } from "~/services/prisma/prisma.service";

const logger = new Logger("WsExceptionFilter");

@Catch()
export class WsExceptionFilter extends BaseWsExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const callback = host.getArgByIndex(2);
    if (callback && typeof callback === "function") {
      let error: any = exception;

      if (exception instanceof HttpException) {
        error = exception.getResponse();
      } else {
        if (exception instanceof Prisma.PrismaClientValidationError) {
          logger.error(exception);
          error = {
            name: "PrismaClientValidationError",
            message: exception.message,
          };
        } else if (exception instanceof Prisma.PrismaClientKnownRequestError) {
          error = {
            name: "PrismaClientKnownRequestError",
            code: exception.code,
            meta: exception.meta,
            message: exception.message,
          };
        }
      }

      callback({ data: null, error });
    }
  }
}
