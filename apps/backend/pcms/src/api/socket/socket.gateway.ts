import os from "os";
import { applyDecorators, UseInterceptors, UseGuards, UseFilters } from "@nestjs/common";
import { SubscribeMessage, WebSocketGateway } from "@nestjs/websockets";
import { JwtAuthGuard } from "~/api/auth/guards/jwtAuth.guard";
import { getWebsocketConfig } from "~/config";
import { WsExceptionFilter } from "./ws-exception.filter";
import { WsProtocolTransformer } from "./ws-protocol.transformer";

export function CustomWebSocketGateway() {
  return applyDecorators(
    UseFilters(WsExceptionFilter),
    UseInterceptors(WsProtocolTransformer),
    UseGuards(JwtAuthGuard),
    WebSocketGateway(getWebsocketConfig()),
  );
}

@CustomWebSocketGateway()
export class SocketGateway {
  @SubscribeMessage("sys:info")
  info() {
    return {
      hostname: os.hostname(),
      arch: os.arch(),
      platform: os.platform(),
      osRelease: os.release(),
      nodeVersion: process.version,
      isProduction: process.env.NODE_ENV === "production",
    };
  }
}
