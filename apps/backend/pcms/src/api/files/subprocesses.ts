import { spawn } from "child_process";
import Debug from "debug"; // eslint-disable-line import/no-named-as-default

const debug = Debug("Files");
const mimeDebug = debug.extend("mimeProbe");
const mediaDebug = debug.extend("mediaProbe");
const posterDebug = debug.extend("videoPoster");

/**
 * probeMime
 */
export function probeMime(path: string): Promise<string> {
  let output = "";
  const file = spawn("file", ["-bnNhi", path]);

  file.stderr.on("data", mimeDebug);
  file.stdout.on("data", (data) => (output += data.toString("utf-8")));
  return new Promise((resolve, reject) =>
    file.on("close", (code) => {
      output = output.trim();
      if (code !== 0 || !output) return reject("Mime probing failed");

      resolve(output);
    }),
  );
}

/**
 * probeMedia
 */
export function probeMedia(path: string, video: boolean): Promise<{ width: number; height: number; length?: number }> {
  let output = "";
  const ffprobe = spawn("ffprobe", [
    "-v",
    "fatal",
    "-select_streams",
    "v:0",
    "-print_format",
    "json",
    "-show_format",
    "-show_entries",
    "stream=width,height",
    path,
  ]);

  ffprobe.stderr.on("data", mediaDebug);
  ffprobe.stdout.on("data", (data) => (output += data.toString("utf-8")));
  return new Promise((resolve, reject) =>
    ffprobe.on("close", (code) => {
      const data = JSON.parse(output.trim());
      if (code !== 0 || !("streams" in data) || !data.streams.length) return reject(new Error("Failed to probe media"));

      const [stream] = data.streams;
      resolve({
        width: stream.width,
        height: stream.height,
        length: video ? parseFloat(data.format.duration) : undefined,
      });
    }),
  );
}

/**
 * generatePoster
 */
export function generatePoster(path: string, output: string, size: number, sizing: "increase" | "decrease") {
  const ffmpeg = spawn("ffmpeg", [
    "-i",
    path,
    "-map",
    "v:0",
    "-v",
    "fatal",
    "-vf",
    `select=gt(scene\\,0.4),scale=${size}:${size}:force_original_aspect_ratio=${sizing}`,
    "-pix_fmt",
    "yuv420p",
    "-frames",
    "1",
    "-vsync",
    "vfr",
    "-y",
    output,
  ]);
  ffmpeg.stderr.on("data", posterDebug);
  ffmpeg.stdout.on("data", posterDebug);

  return new Promise<boolean>((resolve) =>
    ffmpeg.on("close", (code) => {
      resolve(!code);
    }),
  );
}
