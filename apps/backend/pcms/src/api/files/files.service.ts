import Path from "path";
import fs from "fs";
import Mime from "mime";
import sharp from "sharp";
import { Injectable, HttpException, Logger } from "@nestjs/common";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { FileRecord } from "$shared/APITypes";
import { PrismaService } from "~/services/prisma/prisma.service";
import { ConfigService } from "~/services/config/config.service";
import events from "~/events";
import { probeMime, probeMedia, generatePoster } from "./subprocesses";

const logger = new Logger("FilesService");

@Injectable()
export class FilesService {
  private mkdirSeq: Promise<unknown> = Promise.resolve();

  constructor(
    private readonly prisma: PrismaService,
    private readonly config: ConfigService,
    private readonly emitter: EventEmitter2,
  ) {}

  async getFolderId(id: number | string) {
    id = isNaN(+id) ? id + "" : +id;
    if (typeof id === "string") return this.resolvePath(id);
    return id;
  }

  private async getFilePath(id: string) {
    return this.getUploadFilepath(
      await this.prisma.file.findUnique({
        where: { id },
        select: { id: true, ext: true },
        rejectOnNotFound: true,
      }),
    );
  }

  async get(id: string, name = false) {
    if (name) {
      return this.prisma.file.findUnique({
        where: { id },
        select: { name: true },
      });
    }

    return this.prisma.file.findUnique({
      where: { id },
      include: { media: true },
    });
  }

  async list(id: number | string) {
    id = await this.getFolderId(id);

    return this.prisma.folder.findUnique({
      where: { id },
      select: {
        files: {
          include: {
            media: true,
          },
        },
        folders: true,
        id: true,
        parentId: true,
      },
    });
  }

  async rename(id: string | number, name: string) {
    if (!name) return;
    const query = {
      data: { name },
      select: { id: true },
    };

    if (!isNaN(+id)) id = +id;

    if (typeof id === "number") await this.prisma.folder.update({ where: { id }, ...query });
    else if (typeof id === "string") await this.prisma.file.update({ where: { id }, ...query });
    this.emitter.emit(events.files.updated, { id });
  }

  async mkdir(path: string) {
    const result = await this.resolvePath(path.replace(/\/+/g, "/"), true);
    this.emitter.emit(events.files.updated, result);
    return result;
  }

  async mv(target: number, items: (string | number)[]) {
    const files: string[] = [];
    const folders: number[] = [];

    for (const item of items) {
      if (typeof item === "number") folders.push(item);
      else files.push(item);
    }

    const result = await this.prisma.folder.update({
      where: { id: target },
      data: {
        files: files.length ? { connect: files.map((id) => ({ id })) } : undefined,
        folders: folders.length ? { connect: folders.map((id) => ({ id })) } : undefined,
      },
      select: { id: true },
    });

    this.emitter.emit(events.files.updated, result);
    return result;
  }

  async rm(id: string | number) {
    if (typeof id === "number") return this.rmdir(id);

    const result = await this._rm(id);
    this.emitter.emit(events.files.updated, { id });
    return result;
  }

  private async _rm(id: string) {
    const file = await this.prisma.file.findUnique({
      where: { id },
      include: {
        media: true,
      },
      rejectOnNotFound: true,
    });

    await this.prisma.file.delete({ where: { id }, select: { id: true } });

    await this.unlink(file);
    return true;
  }

  private async unlink(file: FileRecord) {
    return fs.promises.unlink(this.getUploadFilepath(file));
  }

  async rmdir(aId: number | string) {
    const id = await this.getFolderId(aId);

    // Get the files that will get deleted
    const deletedFiles = await this.getAllFiles(id);

    // Delete the folder and all children
    await this.prisma.folder.delete({
      where: { id },
      select: { id: true },
    });
    this.emitter.emit(events.files.updated, { id });

    // Delete the actual files
    for (const file of deletedFiles) {
      await this.unlink(file);
    }

    return true;
  }

  private async getAllFiles(id: number): Promise<FileRecord[]> {
    return this.prisma.folder
      .findUnique({
        where: { id },
        select: {
          files: { include: { media: true } },
          folders: { select: { id: true } },
        },
        rejectOnNotFound: true,
      })
      .then(async ({ files, folders }) => {
        const subfiles = await Promise.all(folders.map((folder) => this.getAllFiles(folder.id)));

        return [...files, ...subfiles.flat()];
      });
  }

  async resolvePath(path: string, mkdir = false) {
    let lastResult = { id: 1 };

    // Recursively create folders
    for (const folder of Path.normalize(path)
      .split("/")
      .filter((v) => v && !v.match(/^(\.+)$/))) {
      const where = {
        parentId_name: {
          parentId: lastResult.id,
          name: folder,
        },
      };

      if (mkdir) {
        const promise = this.mkdirSeq.then(() =>
          this.prisma.folder.upsert({
            where,
            create: {
              name: folder,
              parent: { connect: lastResult },
            },
            update: {},
            select: { id: true },
          }),
        );
        this.mkdirSeq = promise;
        lastResult = await promise;
      } else {
        lastResult = await this.prisma.folder.findUnique({
          where,
          select: { id: true },
          rejectOnNotFound: true,
        });
      }
    }

    return lastResult.id;
  }

  async upload(currentPath: string, targetPath: string) {
    try {
      // Prevent exiting the root
      if (Path.normalize(targetPath).startsWith("..")) throw new HttpException("Invalid Path", 400);

      // Parse target path
      const filename = Path.basename(targetPath).trim();
      if (!filename) throw new HttpException("Filename Expected", 400);

      // Register in DB
      const file = await this.create(currentPath, Path.dirname(targetPath), filename);
      const publicPath = this.getUploadFilepath(file);

      // Make sure target folder exists
      await fs.promises.mkdir(this.getUploadFilepath(), { recursive: true });
      // Move the new file to it's location
      await fs.promises.rename(currentPath, publicPath);
    } finally {
      // Make sure the original file doesn't exist
      if (fs.existsSync(currentPath)) await fs.promises.unlink(currentPath);
    }
  }

  async create(realPath: string, virtDirname: string, filename: string, id?: string) {
    // Create needed folders
    const mkdirPromise = this.resolvePath(virtDirname, true);
    // Get file info
    const statPromise = fs.promises.stat(realPath);
    const mime = await probeMime(realPath);
    const isVideo = mime.startsWith("video");
    const isMedia = isVideo || mime.startsWith("image");
    const ext = Mime.getExtension(mime) ?? Path.extname(filename).replace(".", "");

    // Await multiple stuff
    const [media, { size }, folderId] = await Promise.all([
      // Get media info if needed
      !isMedia ? null : this.probeMedia(realPath, isVideo, ext),
      // Await file stat
      statPromise,
      // Await folders
      mkdirPromise,
    ]);

    const result = await this.prisma.file.create({
      data: {
        id,
        ext,
        size,
        mime,
        name: filename,
        folder: { connect: { id: folderId } },
        media: media ? { create: { ...media, type: isVideo ? "VIDEO" : "IMAGE" } } : undefined,
      },
      select: { id: true, ext: true },
    });

    // thumbnails for videos
    if (isVideo) await this.generateVideoPoster(result.id, realPath);

    this.emitter.emit(events.files.updated, result);
    return result;
  }

  private async probeMedia(
    path: string,
    isVideo: boolean,
    ext: string,
  ): Promise<{ width: number; height: number; length?: number }> {
    // Try to skip ffprobe if possible
    if (!isVideo) {
      const format = sharp.format[ext as keyof sharp.FormatEnum];
      if (format && format.input.file) {
        try {
          const meta = await sharp(path).metadata();
          if (meta.width && meta.height) return { height: meta.height, width: meta.width };
        } catch (err) {
          logger.warn(err);
        }
      }
    }
    return probeMedia(path, isVideo);
  }

  async generateVideoPoster(id: string, filepath?: string | null) {
    if (!filepath) filepath = await this.getFilePath(id);

    if (!fs.existsSync(filepath)) throw new HttpException("File doesn't exist", 404);
    await fs.promises.mkdir(this.getPosterFilepath(), { recursive: true });

    return generatePoster(
      filepath,
      this.getPosterFilepath(id),
      this.config.images.videoThumbSize,
      this.config.images.sizing === "inside" ? "increase" : "decrease",
    );
  }

  async fsck() {
    const files = (
      await this.prisma.file.findMany({
        select: { id: true, ext: true },
      })
    ).reduce<Record<string, { id: string; ext: string }>>((acc, file) => {
      acc[file.id + "." + file.ext] = file;
      return acc;
    }, {});

    const [found, lost] = await Promise.all([this.fsckRecoverFS(files), this.fsckCleanDB(files)]);

    return { found, lost };
  }

  async fsckRecoverFS(files: Record<string, { id: string; ext: string }>) {
    const fsFiles = await fs.promises.readdir(this.getUploadFilepath());
    const found: Promise<{ id: string; ext: string }>[] = [];

    for (const filename of fsFiles) {
      // Check if the file exists in DB
      if (filename in files) continue;
      // If it doesn't, create it
      const id = filename.split(".")[0];
      found.push(this.create(Path.join(this.getUploadFilepath(), filename), "/lost+found", filename, id));
    }

    return (await Promise.all(found)).filter((v) => v) as { id: string; ext: string }[];
  }

  async fsckCleanDB(_files: Record<string, { id: string; ext: string }>) {
    // TODO
    return [];
  }

  getUploadFilepath(file?: { id: string; ext: string }) {
    return Path.join(process.cwd(), this.config.uploadPath, file ? file.id + "." + file.ext : "");
  }
  getPosterFilepath(id?: string) {
    return Path.join(process.cwd(), this.config.publicCachePath, "posters", id ? id + ".poster.png" : "");
  }
}
