import Path from "path";
import fs from "fs";
import * as Mime from "mime";
import {
  UseInterceptors,
  UploadedFiles,
  Controller,
  Post,
  Get,
  HttpException,
  Param,
  Header,
  UseGuards,
  StreamableFile,
} from "@nestjs/common";
import { FilesInterceptor } from "@nestjs/platform-express";
import { RequiresRole } from "~/api/auth/decorators/RequiresRole";
import { JwtAuthGuard } from "~/api/auth/guards/jwtAuth.guard";
import { FilesService } from "./files.service";

@Controller("/files")
export class FilesController {
  constructor(private readonly service: FilesService) {}

  @Get("/:file")
  @Header("Cache-Control", "public, max-age=31536000, immutable")
  async get(@Param("file") filename: string) {
    if (!filename) throw new HttpException("No file specified", 400);

    const root = this.service.getUploadFilepath();
    const path = Path.resolve(root, filename);

    if (!path.startsWith(root)) throw new HttpException("Invalid path", 400);

    const id = Path.basename(path).split(".")[0];
    const record = await this.service.get(id, true);

    if (!record) throw new HttpException("File not found", 404);

    const stream = fs.createReadStream(path);
    return new StreamableFile(stream, {
      type: Mime.getType(filename) ?? undefined,
      disposition: `attachment; filename="${encodeURIComponent(record.name)}"`,
    });
  }

  @RequiresRole("EDITOR")
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FilesInterceptor("files"))
  @Post("/")
  async upload(@UploadedFiles() files: Express.Multer.File[]) {
    if (!files) throw new HttpException("Expected file(s)", 400);

    for (const file of files) {
      // Pass the file to file service
      const filePath = decodeURIComponent(Buffer.from(file.originalname, "base64").toString("utf-8"));
      await this.service.upload(file.path, filePath);
    }
    return {};
  }
}
