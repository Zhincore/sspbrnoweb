import { SubscribeMessage, WebSocketServer } from "@nestjs/websockets";
import { OnEvent } from "@nestjs/event-emitter";
import { Server, Socket } from "socket.io";
import { RequiresRole } from "~/api/auth/decorators/RequiresRole";
import { CustomWebSocketGateway } from "~/api/socket/socket.gateway";
import events from "~/events";
import { FilesService } from "./files.service";

@CustomWebSocketGateway()
export class FilesGateway {
  @WebSocketServer()
  private server?: Server;

  constructor(private readonly service: FilesService) {}

  @SubscribeMessage("files:get")
  get(_client: Socket, args: Parameters<FilesService["get"]>) {
    return this.service.get(...args);
  }

  @SubscribeMessage("files:list")
  list(_client: Socket, args: Parameters<FilesService["list"]>) {
    return this.service.list(...args);
  }

  @SubscribeMessage("files:rename")
  @RequiresRole("EDITOR")
  rename(_client: Socket, args: Parameters<FilesService["rename"]>) {
    return this.service.rename(...args);
  }

  @SubscribeMessage("files:mkdir")
  @RequiresRole("EDITOR")
  mkdir(_client: Socket, args: Parameters<FilesService["mkdir"]>) {
    return this.service.mkdir(...args);
  }

  @SubscribeMessage("files:mv")
  @RequiresRole("EDITOR")
  mv(_client: Socket, args: Parameters<FilesService["mv"]>) {
    return this.service.mv(...args);
  }

  @SubscribeMessage("files:rmdir")
  @RequiresRole("EDITOR")
  rmdir(_client: Socket, args: Parameters<FilesService["rmdir"]>) {
    return this.service.rmdir(...args);
  }

  @SubscribeMessage("files:rm")
  @RequiresRole("EDITOR")
  rm(_client: Socket, args: Parameters<FilesService["rm"]>) {
    return this.service.rm(...args);
  }

  @SubscribeMessage("files:generateVideoPoster")
  generateVideoPoster(_client: Socket, args: Parameters<FilesService["generateVideoPoster"]>) {
    return this.service.generateVideoPoster(...args);
  }

  @OnEvent(events.files.updated)
  handleFilesUpdated(payload: any) {
    if (this.server) this.server.emit("files:updated", payload);
  }
}
