import { Module } from "@nestjs/common";
import { MulterModule } from "@nestjs/platform-express";
import { ConfigModule } from "~/services/config/config.module";
import { ConfigService } from "~/services/config/config.service";
import { FilesService } from "./files.service";
import { FilesGateway } from "./files.gateway";
import { FilesController } from "./files.controller";

@Module({
  providers: [FilesService, FilesGateway],
  controllers: [FilesController],
  exports: [FilesService],
  imports: [
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        dest: configService.tmpPath,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class FilesModule {}
