import { Module } from "@nestjs/common";
import { ContentController } from "./content.controller";
import { ContentService } from "./content.service";
import { ContentWebhookService } from "./contentWebhook.service";
import { ContentGateway } from "./content.gateway";

@Module({
  controllers: [ContentController],
  providers: [ContentService, ContentWebhookService, ContentGateway],
})
export class ContentModule {}
