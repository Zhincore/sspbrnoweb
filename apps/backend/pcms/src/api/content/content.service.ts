import { Injectable, HttpException } from "@nestjs/common";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { ListingOptions } from "$shared/APITypes";
import { PrismaService } from "~/services/prisma/prisma.service";
import { ConfigService } from "~/services/config/config.service";
import events from "~/events";

import Model from "~/models/IModel";
import GeneralModel from "~/models/general";
import PostModel from "~/models/post";
import CourseModel from "~/models/course";

import PostCategory from "~/models/postcategory";

export const models = {
  general: GeneralModel,
  post: PostModel,
  course: CourseModel,

  postCategory: PostCategory,
};
export type ModelName = keyof typeof models;

@Injectable()
export class ContentService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly config: ConfigService,
    private readonly emitter: EventEmitter2,
  ) {}

  /// List
  async list<TModelName extends ModelName>(
    modelName: TModelName,
    options?: ListingOptions,
  ): Promise<typeof models[TModelName][]> {
    const model = this.getModel(modelName);

    const { defaultPageSize } = this.config.content;
    const pageSize = (isNaN(options?.pageSize ?? NaN) ? null : options!.pageSize) ?? defaultPageSize;
    const page = (isNaN(options?.page ?? NaN) ? null : options!.page) ?? 0;

    return model.findMany.call(this.prisma, {
      where: options?.where,
      orderBy: options?.orderBy,
      select: options && options.fields ? fieldsToSelectComponents(model, options.fields) : undefined,
      include: options && options.include ? fieldsToSelectComponents(model, options.include) : undefined,
      take: options?.pageSize === null ? undefined : pageSize,
      skip: options?.pageSize === null ? undefined : pageSize * page,
    });
  }

  /// Create
  async create(modelName: ModelName, data: any) {
    const model = this.getModel(modelName);
    if (model.singleton) throw new HttpException("Cannot create a singleton", 400);

    const promise = model.create.call(this.prisma, {
      data: data,
      select: { [model.idField]: true },
    });

    return promise.finally(() => this.emitter.emit(events.content.updated, { modelName }));
  }

  /// Read
  async get<TModelName extends ModelName>(
    modelName: TModelName,
    id?: string | number,
    fields?: string[],
  ): Promise<typeof models[TModelName]> {
    const model = this.getModel(modelName);
    const customFields = fields && fields.length;
    const modelInclude = model.include && model.include.length;

    const select = {
      select: customFields ? fieldsToSelectComponents(model, fields) : undefined,
      include: modelInclude ? fieldsToSelectComponents(model, model.include!) : undefined,
    };

    /// Singleton
    if (model.singleton) return model.findOne.call(this.prisma, select);

    /// Multiton
    return model.findOne.call(this.prisma, { where: { [model.idField]: id ?? undefined }, ...select });
  }

  /// Create/Update
  async set(modelName: ModelName, patch: Record<string, any>, id?: string | number, fields?: string[]) {
    const model = this.getModel(modelName);

    // If singleton, get the row's ID
    if (model.singleton) {
      const singleton = await model.findOne.call(this.prisma, {
        select: { [model.idField]: true },
      });
      if (!singleton) return null;

      id = singleton[model.idField] as string | number;
    } else if (id === null || id === undefined) {
      id = patch[model.idField];
    }

    const promise: Promise<Record<string, any>> = model.update.call(this.prisma, {
      where: { [model.idField]: id },
      data: patch,
      select: fieldsToSelect(fields) || { [model.idField]: true },
    });

    return promise.finally(() => this.emitter.emit(events.content.updated, { modelName, id }));
  }

  /// Delete
  async delete(modelName: ModelName, id: string | number) {
    const model = this.getModel(modelName);
    if (model.singleton) throw new HttpException("Cannot delete a singleton", 400);

    const promise = model.delete.call(this.prisma, {
      where: { [model.idField]: id },
      select: { [model.idField]: true },
    });

    return promise.finally(() => this.emitter.emit(events.content.updated, { modelName, id }));
  }

  getModel(modelName: ModelName) {
    if (!(modelName in models)) throw new HttpException(`Unknown model ${modelName}`, 400);
    const model = models[modelName];
    if (!(modelName in this.prisma)) throw new HttpException(`Table of ${modelName} model doesn't exist`, 500);
    return model;
  }
}

export function fieldsToSelectComponents(model: Model<any>, fields: string[]) {
  return fields.reduce<Record<string, true | { include: Record<string, true> }>>((acc, field) => {
    acc[field] = true;

    const component = "components" in model && model.components && model.components[field];
    if (component && "include" in component) {
      acc[field] = { include: fieldsToSelect(component.include)! };
    }
    return acc;
  }, {});
}

export function fieldsToSelect(fields?: string[]) {
  return fields === undefined
    ? undefined
    : fields.reduce<Record<string, true>>((acc, field) => {
        acc[field] = true;
        return acc;
      }, {});
}
