import { Controller, Get, Param, Query, HttpException } from "@nestjs/common";
import { ListingOptions } from "$shared/APITypes";
import { ContentService, ModelName } from "./content.service";

@Controller("/content")
export class ContentController {
  constructor(private readonly service: ContentService) {}

  @Get("/:model")
  async list(@Param("model") model: ModelName, @Query() query: ListingOptions) {
    return this.service.list(model, {
      ...query,
      pageSize: query.pageSize ? parseInt(query.pageSize as any) : query.pageSize,
      page: query.page ? parseInt(query.page as any) : query.page,
    });
  }

  @Get("/:model/:id")
  async get(@Param("model") model: ModelName, @Param("id") id: string | number, @Query("fields") fields?: string[]) {
    const result = await this.service.get(model, isNaN(+id) ? id : +id, fields);
    if (!result) throw new HttpException("Not found", 404);
    return result;
  }
}
