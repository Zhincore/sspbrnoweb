import { Injectable, Logger } from "@nestjs/common";
import { OnEvent } from "@nestjs/event-emitter";
import { ConfigService } from "~/services/config/config.service";
import events from "~/events";
import { ModelName } from "./content.service";

// https://github.com/microsoft/TypeScript/issues/43329#issuecomment-811606238
const _importDynamic = new Function("modulePath", "return import(modulePath)");
const gotPromise: Promise<typeof import("got")> = _importDynamic("got");

const logger = new Logger("ContentWebhookService");

@Injectable()
export class ContentWebhookService {
  private timeout?: NodeJS.Timeout;

  constructor(private readonly config: ConfigService) {}

  @OnEvent(events.content.updated)
  onContentUpdated(data: { modelName?: ModelName; id?: string | number }) {
    const { webhookUrls, webhookDebounce } = this.config.content;

    if (!webhookUrls.length) return;

    if (this.timeout) clearTimeout(this.timeout);

    this.timeout = setTimeout(async () => {
      this.timeout = undefined;
      const { got } = await gotPromise;
      for (const url of webhookUrls) {
        got.post(url, { json: data }).catch((err) => {
          logger.warn(err, "Failed to send webhook");
        });
      }
    }, webhookDebounce);
  }
}
