import { SubscribeMessage, WebSocketServer } from "@nestjs/websockets";
import { OnEvent } from "@nestjs/event-emitter";
import { Socket, Server } from "socket.io";
import { RequiresRole } from "~/api/auth/decorators/RequiresRole";
import { CustomWebSocketGateway } from "~/api/socket/socket.gateway";
import events from "~/events";
import { ContentService } from "./content.service";

@CustomWebSocketGateway()
export class ContentGateway {
  @WebSocketServer()
  private server?: Server;

  constructor(private readonly service: ContentService) {}

  @SubscribeMessage("content:get")
  get(_client: Socket, args: Parameters<ContentService["get"]>) {
    return this.service.get(...args);
  }

  @SubscribeMessage("content:set")
  @RequiresRole("EDITOR")
  set(_client: Socket, args: Parameters<ContentService["set"]>) {
    return this.service.set(...args);
  }

  @SubscribeMessage("content:list")
  list(_client: Socket, args: Parameters<ContentService["list"]>) {
    return this.service.list(...args);
  }

  @SubscribeMessage("content:create")
  @RequiresRole("EDITOR")
  create(_client: Socket, args: Parameters<ContentService["create"]>) {
    return this.service.create(...args);
  }

  @SubscribeMessage("content:delete")
  @RequiresRole("EDITOR")
  delete(_client: Socket, args: Parameters<ContentService["delete"]>) {
    return this.service.delete(...args);
  }

  @OnEvent(events.content.updated)
  handleContentUpdated(payload: any) {
    if (this.server) this.server.emit("content:updated", payload);
  }
}
