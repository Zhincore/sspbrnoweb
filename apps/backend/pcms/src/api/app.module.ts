import { Module } from "@nestjs/common";
import { EventEmitterModule } from "@nestjs/event-emitter";
import { PrismaModule } from "~/services/prisma/prisma.module";
import { ConfigModule } from "~/services/config/config.module";
import { AdminModule } from "./admin.module";
import { AuthModule } from "./auth/auth.module";
import { SocketModule } from "./socket/socket.module";
import { FilesModule } from "./files/files.module";
import { ImagesModule } from "./images/images.module";
import { ContentModule } from "./content/content.module";
import { AppController } from "./app.controller";

@Module({
  imports: [
    AdminModule,
    ConfigModule,
    PrismaModule,
    EventEmitterModule.forRoot(),
    AuthModule,
    SocketModule,
    FilesModule,
    ImagesModule,
    ContentModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
