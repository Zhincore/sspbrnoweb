import Path from "path";
import { Module } from "@nestjs/common";
import { ServeStaticModule } from "@nestjs/serve-static";
import getConfig from "~/config";

const config = getConfig();

@Module({
  imports: config.adminPanel
    ? [
        ServeStaticModule.forRoot({
          serveRoot: config.adminPanel.url,
          rootPath: Path.join(process.cwd(), config.adminPanel.path),
          serveStaticOptions: {
            cacheControl: true,
            immutable: false,
          },
        }),
      ]
    : [],
})
export class AdminModule {}
