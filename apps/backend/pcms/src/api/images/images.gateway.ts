import { SubscribeMessage } from "@nestjs/websockets";
import { CustomWebSocketGateway } from "~/api/socket/socket.gateway";
import { ImagesService } from "./images.service";

@CustomWebSocketGateway()
export class ImagesGateway {
  constructor(private readonly service: ImagesService) {}

  @SubscribeMessage("images:getConfig")
  getImageConfig() {
    return this.service.imageConfig;
  }
}
