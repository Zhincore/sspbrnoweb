import Path from "path";
import fs from "fs";
import type sharp from "sharp";
import { Controller, Get, HttpException, Param, Query, Header, StreamableFile } from "@nestjs/common";
import { ImagesService } from "./images.service";

@Controller("/images")
export class ImagesController {
  constructor(private readonly service: ImagesService) {}

  @Get("/")
  getHello() {
    return new StreamableFile(
      Buffer.from(
        "UklGRpwBAABXRUJQVlA4IJABAACwDgCdASpgAGAAP3Wmwls8P6ckN/XtU/AuiUAZtxwRY4ttzaj3p+7mSc+IXeLFm8CMIfUSDvcSyLN+Uo2ZoF8jTX+n83+pqVwzdDqfmjB5QNYlaTtoLY+X5PPpDHa3rL4c1D3Qb5Tj4jigrGmZeEFLpCyFjrbYknVaKKcEJoAA/usH8IGx6SPJOi3x5AS77nsY0nltQ2khyJoP4WUgvH+KMV9gx/akEMe8aop11UPYh3jGPpw29GWIi89ZVD+kbpS2nzAa0lyKeCBR3lCJGbuR/8VI+uHti5XiRegKGQYWeAZoDgKEj2LbMgnil+rzLzpA8ol7yIL3oBW0rUT0jAxKtozR7GhR9kK6s5H7fyiXvEbHtP/Wr4AyciAhVBrSGhMdS+gYx0BA20JMN0KvIGOpEGtE1MXBmj26qEmgGz16RUreqQVbVFcwSFdeHpB2V/xeTsbQAgIgWU0CcGn8uxwWqbX2OevNevTLDhNgKCIzh+NLI/lbZku0wJztTjLtqBHMsJOvH2nJp+3wy3ygAAAA",
        "base64",
      ),
      {
        disposition: "filename=kov.avif",
        type: "image/avif",
      },
    );
  }

  @Get("/config")
  getConfig() {
    return this.service.imageConfig;
  }

  @Get("/:size/:file")
  @Header("Cache-Control", "public, max-age=31536000, immutable")
  async get(
    @Param("size") size: string,
    @Param("file") filename: string,
    @Query("sizing") sizing?: "inside" | "outside",
  ) {
    const [id, ext] = filename.split(".") as [string, keyof sharp.FormatEnum | undefined];
    let _ext = ext;

    const rootpath = this.service.getThumbnailFilepath(+size);
    let path = Path.resolve(rootpath, filename);
    if (!path.startsWith(rootpath)) throw new HttpException("Invalid file", 400);

    if (!fs.existsSync(path)) {
      const result = await this.service.optimizeImage(id, +size, sizing, undefined, ext);
      if (!result) throw new HttpException("The file is registered but doesn't exist", 404);
      path = result;
      if (!_ext) getExtFromPath(path);
    }

    if (!_ext) {
      try {
        const link = await fs.promises.readlink(path);
        path = link;
        _ext = getExtFromPath(link);
      } catch (err) {
        // Ignored
      }
    }

    const stream = fs.createReadStream(path);
    return new StreamableFile(stream, {
      type: _ext === "svg" ? "image/svg+xml" : "image/" + ext,
    });
  }
}

function getExtFromPath(path: string) {
  return Path.basename(path).split(".")[0] as keyof sharp.FormatEnum;
}
