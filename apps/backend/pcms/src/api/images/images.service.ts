import Path from "path";
import fs from "fs";
import { randomUUID } from "crypto";
import sharp from "sharp";
import { Injectable, HttpException } from "@nestjs/common";
import { PrismaService } from "~/services/prisma/prisma.service";
import { ConfigService } from "~/services/config/config.service";
import { FilesService } from "~/api/files/files.service";

@Injectable()
export class ImagesService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly config: ConfigService,
    private readonly fileService: FilesService,
  ) {}

  get imageConfig() {
    return {
      sizes: this.config.images.sizes,
      exts: this.config.images.allowedExts,
      defaultExt: this.config.images.defaultExt,
      keepExts: this.config.images.keepExts,
    };
  }

  async optimizeImage(
    id: string,
    size: number,
    sizing?: "inside" | "outside",
    filepath?: string,
    ext?: keyof sharp.FormatEnum,
  ): Promise<string> {
    const conf = this.config.images;
    if (!conf.sizes.includes(+size)) throw new HttpException("Invalid size", 404);

    const media = await this.prisma.media.findUnique({
      where: { id },
      select: {
        type: true,
        file: { select: { id: true, ext: true } },
      },
      rejectOnNotFound: true,
    });
    filepath = filepath ?? this.fileService.getUploadFilepath(media.file);
    const _sizing = sizing ?? this.config.images.sizing;

    if (media.type === "VIDEO") filepath = this.fileService.getPosterFilepath(id);
    if (!fs.existsSync(filepath)) throw new Error(`File or poster ${id} doesn't exist`);

    if (!conf.autoformat && (!ext || !conf.allowedExts.includes(ext))) ext = conf.defaultExt!;
    const format: sharp.AvailableFormatInfo | undefined = sharp.format[media.file.ext as keyof sharp.FormatEnum];
    if (!format || !format.input.file || (ext && conf.keepExts.includes(ext))) return filepath;

    await fs.promises.mkdir(this.getThumbnailFilepath(size), { recursive: true });

    const outputFile = this.getThumbnailFilepath(size, id, ext);
    if (fs.existsSync(outputFile)) return outputFile;

    const image = sharp(filepath);
    const thumbnail = sharp(filepath).resize(size, size, { fit: _sizing, withoutEnlargement: true });
    const saveThumbnail: SaveThumbnail = (_ext, _path) => thumbnail.toFormat(_ext, { mozjpeg: true }).toFile(_path);

    /// Static ext
    if (ext) {
      await saveThumbnail(ext, outputFile);
      return outputFile;
    }

    /// Automatic ext
    return this.autoOptimize(saveThumbnail, image, outputFile);
  }

  private async autoOptimize(saveThumbnail: SaveThumbnail, image: sharp.Sharp, outputFile: string) {
    const conf = this.config.images;

    // Generate all allowed exts
    const outputPromises: Promise<[sharp.OutputInfo, string]>[] = [];
    for (const format of conf.allowedExts) {
      const tmpFile = Path.join(this.config.tmpPath, randomUUID());
      outputPromises.push(saveThumbnail(format, tmpFile).then((d) => [d, tmpFile]));
    }
    const outputs = await Promise.all(outputPromises);

    // Find smallest output
    const findSmallest = (_outputs: [sharp.OutputInfo, string][]) =>
      _outputs.slice(1).reduce((prev, curr) => (prev[0].size < curr[0].size ? prev : curr), _outputs[0]);
    let result = findSmallest(outputs);

    // If the result is jpg, check if the original had alpha
    if (result[0].format === "jpeg") {
      const { isOpaque } = await image
        .resize(64, 64, { withoutEnlargement: true })
        .toBuffer()
        .then((smol) => sharp(smol).stats());

      // If it had alpha, choose different format
      if (!isOpaque) {
        const jpeglessOutputs = [...outputs];
        jpeglessOutputs.splice(outputs.indexOf(result), 1);
        result = findSmallest(jpeglessOutputs);
      }
    }

    const outputFileWExt = outputFile + "." + result[0].format;

    if (fs.existsSync(outputFileWExt)) {
      // File already existed
      await Promise.all([
        ...outputs.map((o) => o !== result && fs.promises.unlink(o[1])),
        !fs.existsSync(outputFile) && fs.promises.symlink(outputFileWExt, outputFile),
      ]);
    } else {
      await Promise.all([
        // Delete loosers
        ...outputs.map((o) => o !== result && fs.promises.unlink(o[1])),
        // Save the winner
        fs.promises.rename(result[1], outputFileWExt),
        // Create symbolic link without extension
        !fs.existsSync(outputFile) && fs.promises.symlink(outputFileWExt, outputFile),
      ]);
    }

    return outputFileWExt;
  }

  getThumbnailFilepath(size?: number, id?: string, ext?: string) {
    ext = ext ? "." + ext : "";
    return Path.join(process.cwd(), this.config.publicCachePath, "thumbnails", size + "", size && id ? id + ext : "");
  }
}

type SaveThumbnail = (_ext: keyof sharp.FormatEnum, _path: string) => Promise<sharp.OutputInfo>;
