import { Module } from "@nestjs/common";
import { FilesModule } from "~/api/files/files.module";
import { ImagesController } from "./images.controller";
import { ImagesService } from "./images.service";
import { ImagesGateway } from "./images.gateway";

@Module({
  imports: [FilesModule],
  providers: [ImagesService, ImagesGateway],
  controllers: [ImagesController],
})
export class ImagesModule {}
