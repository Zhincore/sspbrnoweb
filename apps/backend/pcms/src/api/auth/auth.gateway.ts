import { Socket } from "socket.io";
import { SubscribeMessage, ConnectedSocket } from "@nestjs/websockets";
import { UserData } from "~/api/auth/auth.service";
import { CustomWebSocketGateway } from "~/api/socket/socket.gateway";

@CustomWebSocketGateway()
export class AuthGateway {
  @SubscribeMessage("auth:info")
  info(@ConnectedSocket() client: Socket): UserData {
    return (client.request as any).user;
  }
}
