import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import argon2 from "argon2";
import { Request } from "express";
import { User } from "$shared/prisma";
import { PrismaService } from "~/services/prisma/prisma.service";

export type UserData = Omit<User, "password">;
export type PassportRequest = Request & { user: UserData };
export type JwtPayload = { sub: UserData["username"]; user: UserData };

@Injectable()
export class AuthService {
  constructor(private readonly prisma: PrismaService, private jwtService: JwtService) {}

  async validateUser(username: string, password: string): Promise<UserData | undefined> {
    return this.prisma.user
      .findUnique({
        where: { username },
      })
      .then(async (user) => {
        if (user && (await argon2.verify(user.password, password))) {
          return this.userToData(user);
        }
      });
  }

  async validatePayload(payload: JwtPayload) {
    return this.prisma.user.findFirst({ where: payload.user }).then((user) => user && this.userToData(user));
  }

  async verifyToken(token: string) {
    return this.jwtService.verifyAsync<JwtPayload>(token);
  }

  async login(user: UserData) {
    const payload: JwtPayload = { sub: user.username, user };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }

  private userToData(user: User): UserData {
    return {
      username: user.username,
      role: user.role,
    };
  }
}
