import { Injectable, CanActivate, ExecutionContext, InternalServerErrorException } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { UserRole } from "$shared/prisma";
import { PassportRequest } from "../auth.service";

export const roleSymbol = Symbol("role");

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const role = this.reflector.get<UserRole>(roleSymbol, context.getHandler());
    if (!role) {
      return true;
    }
    const request: PassportRequest = context.switchToHttp().getRequest();
    const user = request.user;

    const roles = Object.keys(UserRole);
    const userRoleId = roles.indexOf(user.role);
    if (userRoleId === -1) throw new InternalServerErrorException("User's role is not valid.");
    const roleId = roles.indexOf(role);
    if (roleId === -1) throw new InternalServerErrorException("Required role is not valid.");

    return userRoleId <= roleId;
  }
}
