import { applyDecorators, UseGuards, SetMetadata } from "@nestjs/common";
import { UserRole } from "$shared/prisma";
import { RoleGuard, roleSymbol } from "../guards/role.guard";

export const RequiresRole = (role: UserRole) => applyDecorators(UseGuards(RoleGuard), SetMetadata(roleSymbol, role));
