import { ExtractJwt, Strategy } from "passport-jwt";
import { Socket } from "socket.io";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "~/services/config/config.service";
import { JwtPayload, AuthService } from "../auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(config: ConfigService, private readonly service: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        // Socket.io support
        (req: any) => {
          if ("handshake" in req) {
            return (req as Socket).handshake.auth.access_token;
          }
        },
        ExtractJwt.fromAuthHeaderAsBearerToken(),
      ]),
      secretOrKey: config.security.jwtSecret,
      jwtSignOptions: config.security.jwtSignOptions,
    });
  }

  async validate(payload: JwtPayload, done: (err: any, result: any) => void) {
    return this.service.validatePayload(payload).then((data) => {
      if (!data) return done(new UnauthorizedException("Invalid token"), false);
      done(null, data);
    });
  }
}
