import { Controller, Post, Get, Request, UseGuards } from "@nestjs/common";
import { LocalAuthGuard } from "./guards/localAuth.guard";
import { JwtAuthGuard } from "./guards/jwtAuth.guard";
import { AuthService, PassportRequest } from "./auth.service";

@Controller("/auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("/login")
  @UseGuards(LocalAuthGuard)
  login(@Request() req: PassportRequest) {
    return this.authService.login(req.user);
  }

  @Get("/user")
  @UseGuards(JwtAuthGuard)
  user(@Request() req: PassportRequest) {
    return req.user;
  }
}
