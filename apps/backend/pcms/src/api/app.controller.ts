import cluster from "cluster";
import { Controller, Get, Header } from "@nestjs/common";

@Controller()
export class AppController {
  @Get("/health")
  @Header("x-thread-id", (cluster.worker?.id ?? 0) + "")
  getHealth(): string {
    return "Hello :)";
  }
}
