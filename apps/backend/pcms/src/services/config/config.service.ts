import { Injectable } from "@nestjs/common";
import { Config } from "~/config";

@Injectable()
export class ConfigService extends Config implements Readonly<Config> {}
