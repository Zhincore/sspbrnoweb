import argon2 from "argon2";
import type { PrismaClient } from "$shared/prisma";
import { Config } from "~/config";

export default async (config: Config, prisma: PrismaClient) => {
  return Promise.all([
    // General
    prisma.general.upsert({
      where: { id: 1 },
      update: {},
      create: { id: 1 },
    }),

    // Default user
    prisma.user
      .aggregate({
        _count: { username: true },
      })
      .then(({ _count }) => {
        if (_count.username) return;
        return argon2.hash(config.security.defaultUser.password).then((password) => {
          return prisma.user.create({
            data: {
              username: config.security.defaultUser.login,
              password,
              role: "ADMIN",
            },
          });
        });
      }),

    // Root folder
    prisma.folder.upsert({
      where: { id: 1 },
      update: {},
      create: {
        id: 1,
        name: "/",
      },
      select: { id: true },
    }),
  ]);
};
