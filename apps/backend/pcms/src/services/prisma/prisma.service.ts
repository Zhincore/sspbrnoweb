import { INestApplication, Injectable, OnModuleInit } from "@nestjs/common";
import { PrismaClient } from "$shared/prisma";
import { ConfigService } from "~/services/config/config.service";
import seed from "./seed";

export { Prisma } from "$shared/prisma";

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  constructor(private readonly config: ConfigService) {
    super({
      datasources: { db: { url: config.databaseUrl } },
      errorFormat: process.env.NODE_ENV === "production" ? "minimal" : undefined,
    });
  }

  async onModuleInit() {
    await seed(this.config, this);
  }

  async enableShutdownHooks(app: INestApplication) {
    this.$on("beforeExit", async () => {
      await app.close();
    });
  }
}
