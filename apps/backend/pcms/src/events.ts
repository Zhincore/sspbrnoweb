export default {
  files: {
    updated: "onFilesUpdated",
  },
  content: {
    updated: "onContentUpdated",
  },
};
