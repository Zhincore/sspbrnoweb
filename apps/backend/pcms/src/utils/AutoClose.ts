import { INestApplicationContext } from "@nestjs/common";
import { Handler } from "express";

export class AutoClose {
  private timeout: NodeJS.Timeout | undefined;
  private readonly wakeLocks = new Set<number>();
  private lastWakeLock = 0;

  constructor(private readonly app: INestApplicationContext, public readonly delay = 10 * 60_000) {
    this.resetTimeout();
  }

  private get wakeLocked() {
    return this.wakeLocks.size > 0;
  }

  getWakeLock() {
    const id = ++this.lastWakeLock;
    this.wakeLocks.add(id);
    this.resetTimeout();
    return id;
  }

  releaseWakeLock(id: number) {
    const result = this.wakeLocks.delete(id);
    this.resetTimeout();
    return result;
  }

  resetTimeout() {
    // Clear previous timeout
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
    // If not wake-locked create timeout
    if (!this.wakeLocked && this.delay) this.timeout = setTimeout(() => this.app.close(), this.delay);
  }

  getMiddleware(): Handler {
    return (_req, _res, next) => {
      this.resetTimeout();
      return next();
    };
  }
}
