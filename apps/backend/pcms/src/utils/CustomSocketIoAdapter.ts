import { INestApplicationContext } from "@nestjs/common";
import { IoAdapter } from "@nestjs/platform-socket.io";
import { ServerOptions, Server } from "socket.io";
import { AutoClose } from "./AutoClose";

export class CustomSocketIoAdapter extends IoAdapter {
  constructor(app: INestApplicationContext, private readonly autoClose?: AutoClose) {
    super(app);
  }

  createIOServer(port: number, options?: ServerOptions) {
    const server: Server = super.createIOServer(port, options);

    if (this.autoClose) {
      server.use((socket, next) => {
        // Prevent shut down while admin connected
        const wakeLock = this.autoClose!.getWakeLock();

        socket.on("disconnect", () => {
          this.autoClose!.releaseWakeLock(wakeLock);
        });

        return next();
      });
    }

    return server;
  }

  mapPayload(aPayload: unknown) {
    const payload = super.mapPayload(aPayload);

    // Ensure the payload is array to spread it as args for controllers
    if (!Array.isArray(payload.data)) payload.data = [payload.data];

    return payload;
  }
}
