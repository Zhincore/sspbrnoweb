import { useEffect } from "react";
import { useToast } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";

import server from "~/lib/server";

export default function ServerStatus() {
  const toast = useToast();
  const { t } = useTranslation();

  useEffect(() => {
    const listeners: Record<string, (...args: any[]) => void> = {
      connecting: () => {
        const toastId = toast({
          title: t("connecting"),
          duration: null,
        })!;

        server.connectPromise.then(
          () => {
            toast.update(toastId, {
              status: "success",
              title: t("connectSuccess"),
            });
          },
          () => toast.close(toastId),
        );
      },

      connect_error: (error) => {
        const msg = server.getErrorMessage(error, t);
        toast({ title: t("connectError") + (msg ? ` (${msg})` : ""), status: "error" });
      },

      disconnect: (isIntended) => {
        if (!isIntended) toast({ title: t("disconnect"), status: "error" });
      },

      error: (error) => {
        toast({ title: `${t("serverError")}: ${server.getErrorMessage(error, t)}`, status: "error" });
      },
    };

    server.on("connecting", listeners.connecting);
    server.on("connect_error", listeners.connect_error);
    server.on("disconnect", listeners.disconnect);
    server.on("error", listeners.error);

    return () => {
      server.off("connecting", listeners.connecting);
      server.off("connect_error", listeners.connect_error);
      server.off("disconnect", listeners.disconnect);
      server.off("error", listeners.error);
    };
  });

  return <></>;
}
