import { useState, useEffect } from "react";
import { IconButton, Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { FixedSizeGrid } from "react-window";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import FAIcon from "$shared/components/FAIcon";
import usePromise from "$shared/hooks/usePromise";

const getFuse = () => import("fuse.js").then((m) => m.default);
const getFas = () =>
  import(
    /* webpackExports: ["fas"] */
    "@fortawesome/free-solid-svg-icons"
  ).then((m) => m.fas);
const getFar = () =>
  import(
    /* webpackExports: ["far"] */
    "@fortawesome/free-regular-svg-icons"
  ).then((m) => m.far);
const getFab = () =>
  import(
    /* webpackExports: ["fab"] */
    "@fortawesome/free-brands-svg-icons"
  ).then((m) => m.fab);

const SEARCH_DEBOUNCE = 250;

type IconSelectorProps = {
  selected?: string | null;
  onSelect: (icon: string) => void;
};

export const getIcons = () => Promise.all([getFas(), getFar(), getFab()]).then((a) => a.flatMap(Object.values));

export default function IconSelector({ selected, onSelect }: IconSelectorProps) {
  const { t } = useTranslation();
  const [icons] = usePromise<IconDefinition[]>(() => getIcons());
  const [filteredIcons, setFilteredIcons] = useState(icons);
  const [search, setSearch] = useState("");

  useEffect(() => {
    if (!search || !icons) return setFilteredIcons(icons);

    let timeout: NodeJS.Timeout | undefined = setTimeout(() => {
      timeout = undefined;

      getFuse().then((Fuse) => {
        const fuse = new Fuse(icons, {
          keys: ["prefix", "iconName"],
        });
        setFilteredIcons(fuse.search(search).map((result) => result.item));
      });
    }, SEARCH_DEBOUNCE);

    return () => {
      if (timeout) clearTimeout(timeout);
    };
  }, [search, icons]);

  return (
    <>
      <InputGroup>
        <InputLeftElement>
          <FAIcon icon={faSearch} />
        </InputLeftElement>
        <Input placeholder={t("search")} value={search} onChange={({ target }) => setSearch(target.value)} />
      </InputGroup>

      {filteredIcons && (
        <FixedSizeGrid
          columnWidth={32}
          columnCount={8}
          rowHeight={32}
          rowCount={filteredIcons.length / 8}
          width={256}
          height={256}
        >
          {({ style, columnIndex, rowIndex }) => {
            const icon = filteredIcons[rowIndex * 8 + columnIndex];
            if (!icon) return <div style={style} />;
            const value = `${icon.prefix} fa-${icon.iconName}`;
            return (
              <IconButton
                isActive={selected === value}
                onClick={() => onSelect(value)}
                style={style}
                aria-label={`${icon.prefix} ${icon.iconName}`}
                size="sm"
                variant="ghost"
                icon={<FAIcon icon={icon} />}
              />
            );
          }}
        </FixedSizeGrid>
      )}
    </>
  );
}
