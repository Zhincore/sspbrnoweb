import { Alert, AlertIcon, AlertTitle, AlertDescription } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import type { sys } from "~/lib/server";

export default function DevModWarn({ info }: { info: sys.SysInfo | null }) {
  const { t } = useTranslation();

  return (
    <>
      {info && !info.isProduction && (
        <Alert status="warning" variant="left-accent">
          <AlertIcon />
          <AlertTitle mr={2}>{t("devModeWarn")}</AlertTitle>
          <AlertDescription>{t("devModeDesc")}</AlertDescription>
        </Alert>
      )}
    </>
  );
}
