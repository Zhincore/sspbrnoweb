import { useRouter } from "next/router";
import { ContentTypes } from "$shared/APITypes";
import Page from "$parts/Page";
import ContentEditor, { ContentEditorProps } from "$elements/ContentEditor";

type ContentEditorPageProps<ContentType extends keyof ContentTypes> = Pick<
  ContentEditorProps<ContentType>,
  "children" | "contentType"
> & {
  plural: string;
};

export default function ContentEditorPage<ContentType extends keyof ContentTypes>({
  children,
  contentType,
  plural,
}: ContentEditorPageProps<ContentType>) {
  const {
    query: { id },
  } = useRouter();
  const isCreating = id === "_new";

  return (
    <Page name={isCreating ? `new_${contentType}` : contentType} breadcrumb={["content", plural]}>
      <ContentEditor
        key={id as string}
        contentType={contentType}
        create={isCreating}
        id={isCreating ? undefined : (id as string | undefined) ?? ""}
      >
        {children}
      </ContentEditor>
    </Page>
  );
}
