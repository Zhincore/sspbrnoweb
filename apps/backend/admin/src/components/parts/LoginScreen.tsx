import { useState, useCallback } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import {
  useToast,
  useBoolean,
  Button,
  IconButton,
  Stack,
  VStack,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Heading,
  Container,
} from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
// TODO: import LangSelect from "$parts/LangSelect";
import Logo from "$elements/Logo";
import Karel from "$public/assets/illustration.webp";
import { auth } from "~/lib/server";
import Media from "$elements/Media";
import ScreenOverlay from "$elements/ScreenOverlay";

export default function LoginScreen() {
  const toast = useToast();
  const [showPassword, { toggle: toggleShowPassword }] = useBoolean(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { t } = useTranslation();

  const onSubmit = useCallback(
    (ev: React.FormEvent) => {
      ev.preventDefault();
      if (!username || !password) return;

      const toastId = toast({
        title: t("logging"),
        duration: null,
      })!;

      const authPromise = auth.login(username, password);
      authPromise.then(
        (error) => {
          if (error) toast.update(toastId, { title: t("wrongLogin"), status: "error" });
          else toast.update(toastId, { title: t("logged"), status: "success" });
        },
        () => toast.update(toastId, { title: t("connectError"), status: "error" }),
      );
      authPromise.catch(console.error);
    },
    [username, password, t, toast],
  );

  return (
    <ScreenOverlay initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
      <Media
        src={Karel.src}
        priority
        position="fixed"
        top={0}
        left={0}
        bottom={0}
        right={0}
        cssWidth="100%"
        cssHeight="100%"
        zIndex={-1}
        objectFit="cover"
        objectPosition="center"
        filter="grayscale(0.75) blur(0.25vmax) contrast(0.5) brightness(0.5)"
      />

      <Stack spacing={8} direction={["column-reverse", "row"]}>
        <VStack as="form" spacing={8} onSubmit={onSubmit} p={6} borderRadius="lg">
          <Heading as="h1" size="lg">
            {t("pleaseLogin")}
          </Heading>

          <FormControl isRequired>
            <FormLabel htmlFor="username">{t("username")}</FormLabel>
            <Input
              id="username"
              name="username"
              value={username}
              onChange={({ target }) => setUsername(target.value)}
            />
          </FormControl>

          <FormControl isRequired>
            <FormLabel htmlFor="password">{t("password")}</FormLabel>
            <InputGroup>
              <Input
                id="password"
                name="password"
                type={showPassword ? "text" : "password"}
                autoComplete="current-password"
                value={password}
                onChange={({ target }) => setPassword(target.value)}
              />
              <InputRightElement>
                <IconButton
                  onClick={toggleShowPassword}
                  variant="ghost"
                  aria-label="Toggle show password"
                  icon={<FontAwesomeIcon icon={showPassword ? faEye : faEyeSlash} />}
                />
              </InputRightElement>
            </InputGroup>
          </FormControl>

          <Button isDisabled={!username || !password} w="full" colorScheme="pgreen" type="submit">
            {t("login")}
          </Button>
        </VStack>

        <VStack alignItems="left">
          <Logo height={140} />
          <Container>{t("pcmsDescription")}</Container>
        </VStack>
      </Stack>
    </ScreenOverlay>
  );
}
