import Link from "next/link";
import { motion, Variant } from "framer-motion";
import { Flex, FlexProps, Box, Heading, Breadcrumb, BreadcrumbItem, BreadcrumbLink } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import LoadingScreen from "$parts/LoadingScreen";

type PageState = "hidden" | "show" | "hide";
type PageAnimation = Record<PageState, Variant>;

type PageProps = {
  children?: React.ReactNode;
  loading?: boolean;
  name?: string;
  breadcrumb?: string[];
  animation?: PageAnimation;
};

const defaultAntimation: PageAnimation = {
  hidden: { opacity: 0 },
  show: { opacity: 1 },
  hide: { opacity: 0 },
};

const MotionFlex = motion<FlexProps>(Flex);

export default function Page({ children, name, loading, breadcrumb, animation }: PageProps) {
  const { t } = useTranslation();
  const _breadcrumb = ["home", ...(breadcrumb || []), ...(name ? [name] : [])];

  return (
    <MotionFlex
      variants={animation ?? defaultAntimation}
      initial="hidden"
      animate="show"
      exit="hide"
      w="100%"
      minH="100%"
      position="absolute"
      top={0}
      left={0}
      py={2}
      px={[4, 6]}
      pb={8}
      flexDirection="column"
    >
      {name && (
        <Box as="header" mb={4}>
          <Heading as="h1" size="2xl">
            {t(name)}
          </Heading>

          {breadcrumb && (
            <Breadcrumb my={2}>
              {_breadcrumb.map((item, i) => {
                const isCurrent = i === _breadcrumb.length - 1;
                const link = <BreadcrumbLink isCurrentPage={isCurrent}>{t(item)}</BreadcrumbLink>;
                return (
                  <BreadcrumbItem key={i}>
                    {isCurrent ? (
                      link
                    ) : (
                      <Link href={"/" + _breadcrumb.slice(1, i + 1).join("/")} passHref>
                        {link}
                      </Link>
                    )}
                  </BreadcrumbItem>
                );
              })}
            </Breadcrumb>
          )}
        </Box>
      )}

      <Flex as="section" w="100%" minH="100%" direction="column" flexGrow={1}>
        {loading ? <LoadingScreen /> : children}
      </Flex>
    </MotionFlex>
  );
}
