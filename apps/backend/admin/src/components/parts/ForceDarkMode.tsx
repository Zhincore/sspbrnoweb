import { useEffect } from "react";
import { useColorMode } from "@chakra-ui/react";

// Chakra UI broke with React 18, this forces dark mode
export default function ForceDarkMode() {
  const { colorMode, setColorMode } = useColorMode();

  useEffect(() => {
    if (colorMode === "light") setColorMode("dark");
  });

  return <></>;
}
