import { useEffect } from "react";
import { Flex, FlexProps, Box, useDisclosure, useBreakpointValue, useId } from "@chakra-ui/react";
import { motion } from "framer-motion";
import Header from "./Header";
import Navigation from "./Navigation";
// import LangSelect from "$parts/LangSelect";

const MotionFlex = motion<FlexProps>(Flex);

type LayoutProps = {
  children?: React.ReactNode;
};

export default function Layout({ children }: LayoutProps) {
  const navId = useId(undefined, "nav");
  const defaultIsOpen = useBreakpointValue([false, true], "md");
  const navDisclousure = useDisclosure({ defaultIsOpen, id: navId });
  const { onOpen, onClose } = navDisclousure;

  useEffect(() => {
    if (defaultIsOpen) onOpen();
    else onClose();
  }, [defaultIsOpen, onOpen, onClose]);

  return (
    <MotionFlex initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} direction="column" minH="100vh">
      <Header navDisclousure={{ ...navDisclousure, id: navId }} />

      <Flex flexGrow={1}>
        <Navigation isOpen={navDisclousure.isOpen} id={navId} />

        <Box as="main" position="relative" w="100%">
          {children}
        </Box>
      </Flex>
    </MotionFlex>
  );
}
