import { Flex, Heading, VisuallyHidden, IconButton, Button, useDisclosure, UseDisclosureProps } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faSignOut } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import Logo from "$elements/Logo";
import server from "~/lib/server";

type HeaderProps = {
  navDisclousure: UseDisclosureProps;
};

export default function Header({ navDisclousure }: HeaderProps) {
  const { t } = useTranslation();
  const { getButtonProps } = useDisclosure(navDisclousure);

  return (
    <Flex
      position="sticky"
      top={0}
      zIndex={99}
      justifyContent="space-between"
      alignItems="center"
      px={4}
      py={2}
      height={20}
      bg="bg"
    >
      <Flex alignItems="center">
        <IconButton
          size="lg"
          aria-label="Toggle menu"
          icon={<FontAwesomeIcon icon={faBars} size="lg" />}
          variant="ghost"
          {...getButtonProps()}
        />

        <Heading as="h1" ml={6}>
          <Logo height={16} width="auto" />
          <VisuallyHidden>P CMS</VisuallyHidden>
        </Heading>
      </Flex>

      <Button variant="outline" leftIcon={<FontAwesomeIcon icon={faSignOut} />} onClick={() => server.auth.logout()}>
        {t("logout")}
      </Button>
    </Flex>
  );
}
