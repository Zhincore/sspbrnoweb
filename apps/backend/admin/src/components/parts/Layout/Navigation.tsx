import Link from "next/link";
import { useRouter } from "next/router";
import { Box, BoxProps, Stack, StackItem, Button } from "@chakra-ui/react";
import { motion } from "framer-motion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faColumns, faInfoCircle, faFileAlt, faUserGraduate, faBox } from "@fortawesome/free-solid-svg-icons";
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { useTranslation } from "react-i18next";

const MotionBox = motion<BoxProps>(Box);

type NavItem = {
  icon: IconDefinition;
  title: string;
  href: string;
};

const NAV: { title: string; items: NavItem[] }[] = [
  {
    title: "dashboard",
    items: [
      {
        icon: faColumns,
        title: "dashboard",
        href: "/",
      },
    ],
  },
  {
    title: "content",
    items: [
      {
        icon: faFileAlt,
        title: "posts",
        href: "/content/posts",
      },
      {
        icon: faUserGraduate,
        title: "courses",
        href: "/content/courses",
      },
    ],
  },
  {
    title: "tools",
    items: [
      {
        icon: faBox,
        title: "files",
        href: "/files",
      },
    ],
  },
  {
    title: "settings",
    items: [
      {
        icon: faInfoCircle,
        title: "general",
        href: "/settings/general",
      },
    ],
  },
];

type NavigationProps = {
  isOpen: boolean;
  id: string;
};

export default function Navigation({ isOpen, id }: NavigationProps) {
  const { asPath } = useRouter();
  const { t } = useTranslation();

  return (
    <MotionBox
      id={id}
      animate={{
        width: isOpen ? "auto" : 0,
        translateX: isOpen ? 0 : "-100%",
        transition: {
          type: "spring",
          damping: 15,
          stiffness: 75,
        },
      }}
      overflow="hidden"
      as="nav"
      bg="bg"
      zIndex={99}
      willChange="width, transform"
      position={["fixed", "sticky"]}
      height="100%"
      top={20}
      flexShrink={0}
    >
      <Stack as="ul" direction="column" m={3} minW="250px" overflowY="auto" maxH="100%">
        {NAV.map((menu, i) => (
          <StackItem as="li" key={i}>
            <Box>{t(menu.title)}</Box>

            <Stack as="ul" direction="column" title={t(menu.title)} m={3}>
              {menu.items.map((item, j) => (
                <StackItem key={j} as="li">
                  <Link href={item.href} passHref>
                    <Button
                      as="a"
                      variant="ghost"
                      justifyContent="flex-start"
                      isFullWidth
                      isActive={item.href === "/" ? asPath === "/" : asPath.startsWith(item.href)}
                      leftIcon={<FontAwesomeIcon icon={item.icon} fixedWidth className="mr-4 self-center" />}
                    >
                      {t(item.title)}
                    </Button>
                  </Link>
                </StackItem>
              ))}
            </Stack>
          </StackItem>
        ))}
      </Stack>
    </MotionBox>
  );
}
