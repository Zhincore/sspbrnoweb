import { Spinner, ChakraProps } from "@chakra-ui/react";
import ScreenOverlay from "$elements/ScreenOverlay";

export default function LoadingScreen(props: ChakraProps) {
  return (
    <ScreenOverlay initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} {...props}>
      <Spinner size="xl" color="pgreen.400" />
    </ScreenOverlay>
  );
}
