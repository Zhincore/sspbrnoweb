import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ButtonGroup,
  Button,
} from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import ContentEditor from "./";

type PreventNavigationProps = {
  editor: ContentEditor<any>;
};

export default function PreventNavigation({ editor }: PreventNavigationProps) {
  const [blockedRoute, setBlockedRoute] = useState<string>();
  const { t } = useTranslation();
  const router = useRouter();

  useEffect(() => {
    const listener = (ev: BeforeUnloadEvent | string) => {
      if (editor.isCreating || !editor.hasChanged || blockedRoute) return;

      // Browser handler
      if (typeof ev !== "string") {
        return ev.preventDefault();
      }
      // Next handler
      setBlockedRoute(ev);
      throw new Error("Changes not saved");
    };

    router.events.on("routeChangeStart", listener);
    window.addEventListener("beforeunload", listener, { capture: true });

    return () => {
      router.events.off("routeChangeStart", listener);
      window.removeEventListener("beforeunload", listener, { capture: true });
    };
  }, [blockedRoute, editor.hasChanged, editor.isCreating, router.events]);

  const onLeave = () => {
    if (!blockedRoute) return;
    router.push(blockedRoute);
  };
  const onSave = async () => {
    await editor.save();
    onLeave();
  };
  const onClose = () => setBlockedRoute(undefined);

  return (
    <Modal isOpen={!!blockedRoute} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{t("confirmLeave")}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>{t("changesUnsaved")}</ModalBody>

        <ModalFooter>
          <ButtonGroup>
            <Button variant="ghost" onClick={() => setBlockedRoute(undefined)}>
              {t("cancel")}
            </Button>

            <Button colorScheme="red" onClick={onLeave}>
              {t("revert")}
            </Button>

            <Button colorScheme="pgreen" onClick={onSave}>
              {t("saveChanges")}
            </Button>
          </ButtonGroup>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
