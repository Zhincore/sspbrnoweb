import { Component } from "react";
import router from "next/router";
import { EventEmitter } from "eventemitter3";
import TypedEmitter from "typed-emitter";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import server, { content } from "~/lib/server";
import ContentForm from "./ContentForm";

type ContentValue<ContentType extends keyof ContentTypes, TValue> =
  | ContentTypes[ContentType][KeysOfType<ContentTypes[ContentType], TValue>]
  | TValue;

export type ContentEditorProps<ContentType extends keyof ContentTypes> = {
  parent?: ContentEditor<any>;
  children: (editor: ContentEditor<ContentType>) => React.ReactNode;
  id?: string | number;
  create?: boolean; // NOTE: Don't change create prop during lifetime, change the `key` prop to reset
  isSingleton?: boolean;
  contentType: ContentType;
  afterSave?: (data: ContentTypes[ContentType]) => void | Promise<void>;
  onRevert?: (data: ContentTypes[ContentType] | null) => void | Promise<void>;
};

type ContentEditorState<ContentType extends keyof ContentTypes> = {
  data: ContentTypes[ContentType] | null;
  changes: Partial<ContentTypes[ContentType]>;
  loading: boolean;
};

type ContentEditorEvents = {
  update: () => void;
  saving: () => void;
  revert: () => void;
  savePromise: (promise: Promise<unknown>) => void;
};

const CONTENT_GET = "content:get";

export default class ContentEditor<ContentType extends keyof ContentTypes> extends Component<
  ContentEditorProps<ContentType>,
  ContentEditorState<ContentType>
> {
  state: ContentEditorState<ContentType> = {
    data: this.props.create
      ? null
      : server.getCachedData<ContentTypes[ContentType]>(
          CONTENT_GET,
          this.props.contentType,
          this.props.id ?? undefined,
        ),
    changes: {},
    loading: false,
  };
  readonly events = new EventEmitter() as TypedEmitter<ContentEditorEvents>;
  private dto: any = {};
  private saving = false;
  private cleaners: ((...args: any[]) => any)[] = [];
  private readonly children: Set<ContentEditor<any>> = new Set();
  ready = false;

  get data() {
    return this.state.data;
  }
  get type() {
    return this.props.contentType;
  }
  get isLoading() {
    return this.state.loading;
  }
  get isCreating() {
    return !!this.props.create;
  }
  get isSingleton() {
    return this.props.isSingleton || (!this.isCreating && this.props.id === undefined);
  }
  get hasChanged(): boolean {
    return this.hasSelfChanged || this.eachChild((child) => child.hasChanged).some((v) => v);
  }
  get hasSelfChanged() {
    return !!Object.keys(this.state.changes).length;
  }

  constructor(props: ContentEditorProps<ContentType>) {
    super(props);
    this.state.loading = !this.state.data && !this.props.create;
  }

  registerChild(child: ContentEditor<any>) {
    this.children.add(child);
  }

  unregisterChild(child: ContentEditor<any>) {
    this.children.delete(child);
  }

  private eachChild<TReturn>(fn: (child: ContentEditor<any>) => TReturn) {
    return Array.from(this.children, fn);
  }

  componentDidMount() {
    const onUpdate = this.onUpdate.bind(this);
    server.connection.on("content:updated", onUpdate);

    const onKeyDown = this.onKeyDown.bind(this);
    document.addEventListener("keydown", onKeyDown);

    // Cleaners
    this.cleaners.push(
      // content:get updates
      server.getCallListener<ContentTypes[ContentType]>(
        this.onGet.bind(this),
        CONTENT_GET,
        this.props.contentType,
        this.props.id ?? undefined,
      ),

      () => server.connection.off("content:updated", onUpdate),

      () => document.removeEventListener("keydown", onKeyDown),
    );

    if (this.props.parent) this.props.parent.registerChild(this);
    if (!this.state.data && !this.isCreating) this.update();
    else this.ready = true;
  }

  componentWillUnmount() {
    if (this.props.parent) this.props.parent.unregisterChild(this);
    this.cleaners.forEach((fn) => fn());
  }

  private onUpdate({ modelName, id }: { modelName: string; id: any }) {
    // Ignore updates of different content
    if (modelName !== this.props.contentType || (this.props.id && this.props.id !== id)) return;

    const newId = this.getId();
    if (!this.props.parent && id !== newId) {
      // Update URL if it got changed
      this.dto = {};
      return this.setState({ changes: {}, loading: true }, () => this.redirectToContent(newId));
    }

    this.update();
  }

  private onGet(data: ContentTypes[ContentType]) {
    this.ready = true;
    if (!this.saving) return this.setState({ data, loading: false });

    this.events.emit("update");
    this.dto = {};
    this.setState({ data, loading: false, changes: {} });
    this.saving = false;
  }

  private onKeyDown(ev: KeyboardEvent) {
    // Ctrl+S
    if (ev.ctrlKey && ev.key == "s") {
      this.save();
      ev.preventDefault();
    }
  }

  async update() {
    if (this.isCreating || this.props.id === "") return; // props.id becomes "" when unloading a page
    if (this.props.parent) await this.props.parent.update();
    return server.call(CONTENT_GET, this.props.contentType, this.props.id ?? undefined);
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  async save(ev?: React.FormEvent) {
    if (ev) ev.preventDefault();
    if (this.saving || !this.hasChanged) return;
    this.saving = true;

    this.events.emit("saving");
    await Promise.all(this.eachChild((child) => child.save())).catch(console.error);

    return new Promise((resolve, reject) =>
      this.setState({ loading: true }, () => {
        const savePromise = server.call<ContentTypes[ContentType]>(
          "content:" + (this.props.create ? "create" : "set"),
          this.props.contentType,
          this.dto,
          this.props.id ?? undefined,
        );

        this.events.emit("savePromise", savePromise);

        if (this.props.afterSave) {
          savePromise.then((result) => this.props.afterSave!(result));
        }

        savePromise
          .then((result) => {
            resolve(result);
            if (this.isCreating && !this.props.parent) {
              const newId = this.getId(result);
              this.redirectToContent(newId);
            }
          })
          .catch(reject);
        // Now the server should emit content:updated after which we fetch new data
      }),
    );
  }

  getId(data?: ContentTypes[ContentType]) {
    const idField = content.idFields[this.props.contentType] as any;
    return data ? (data as any)[idField] : this.getValue<any>(idField);
  }

  redirectToContent(id: any) {
    // Change url to the new one
    if (!id || this.props.id === id) return;

    const path = router.asPath
      .slice((process.env.NEXT_PUBLIC_BASE_PATH ?? "").length)
      .replace((this.props.id || "_new") + "", id);
    router.replace(path);
  }

  async revert() {
    await Promise.all(this.eachChild((child) => child.revert()));
    if (this.props.onRevert) this.props.onRevert(this.state.data);

    return new Promise<void>((resolve) =>
      this.setState({ changes: {} }, () => {
        this.dto = {};
        this.events.emit("revert");
        resolve();
      }),
    );
  }

  getValue<TValue>(key: KeysOfType<ContentTypes[ContentType], TValue>): TValue | undefined {
    const { data, changes } = this.state;
    let value: any;
    if (key in changes) value = changes[key];
    else if (data && key in data) value = data[key];
    return value;
  }

  setValue<TValue>(
    key: KeysOfType<ContentTypes[ContentType], TValue>,
    value: ContentValue<ContentType, TValue>,
    dtoValue?: any,
  ) {
    if (this.isLoading) return;
    const { ...changes } = this.state.changes;

    // If the change reverts changes delete the change
    if (this.state.data && value === this.state.data[key]) {
      delete changes[key];
      delete this.dto[key];
    } else {
      changes[key] = value as any;
      this.dto[key] = typeof dtoValue === "undefined" ? value : dtoValue;
    }

    if (this.props.parent) this.props.parent.setState({});
    this.setState({ changes: changes });
  }

  render() {
    const children = this.props.children(this);
    return this.props.parent ? (
      children
    ) : (
      <ContentForm data={this.state.data} editor={this}>
        {children}
      </ContentForm>
    );
  }
}
