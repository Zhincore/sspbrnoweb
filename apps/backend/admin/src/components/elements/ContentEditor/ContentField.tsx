import { Box, BoxProps } from "@chakra-ui/react";

type ContentFieldProps = Omit<BoxProps, "w"> & {
  w: string | string[];
  children: React.ReactNode;
};

export function ContentField({ w, ...props }: ContentFieldProps) {
  return <Box w={["100%", "100%", ...(Array.isArray(w) ? w : [w])]} px={[0, 2]} py={[2, 4]} {...props} />;
}
