import { useRef, useMemo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { FormControl, FormLabel, FormHelperText, IconButton } from "@chakra-ui/react";
import { CreatableSelect, ActionMeta, ControlProps, chakraComponents } from "chakra-react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPalette } from "@fortawesome/free-solid-svg-icons";
import slugify from "slugify";
import { ContentTypes, PostCategory } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import useServiceData from "~/hooks/useServiceData";
import { slugifyOptions } from "~/lib/utils";
import ContentEditor from "../ContentEditor";

type CategoryFieldProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  label?: string;
  name: KeysOfType<ContentTypes[ContentType], PostCategory | null>;
  helperText?: string;
  isRequired?: boolean;
};

export function CategoryField<ContentType extends keyof ContentTypes>({
  label,
  name,
  editor,
  helperText,
  isRequired,
}: CategoryFieldProps<ContentType>) {
  const { t } = useTranslation();
  const [categs] = useServiceData<PostCategory[]>("content:list", "postCategory");
  const fieldId = editor.type + "-" + name;
  const value = editor.getValue(name);
  const createdRef = useRef(false);

  const onChange = useCallback(
    (item: PostCategory | null, meta?: ActionMeta<PostCategory>) => {
      if (meta) {
        createdRef.current = meta.action === "create-option";
      }
      const created = createdRef.current;
      const current = (editor.data ? editor.data[name] : null) as PostCategory | null;

      editor.setValue(name, item, getCategDTO(created, item, current));
    },
    [editor, name],
  );

  const customComponents = useMemo(() => {
    const chooseColor = (currentValue: PostCategory | null, ev?: React.MouseEvent) => {
      if (!currentValue) return;

      ev?.stopPropagation();

      const input = document.createElement("input");
      input.setAttribute("type", "color");
      input.setAttribute("value", currentValue.color);
      input.onchange = () => {
        onChange({ ...currentValue, color: input.value });
      };
      input.click();
    };

    return {
      Control: ({ children, ...props }: ControlProps<PostCategory, false>) => (
        <chakraComponents.Control {...props}>
          {props.hasValue && (
            <IconButton
              icon={<FontAwesomeIcon icon={faPalette} />}
              h="auto"
              aria-label="change color"
              onClick={(ev) => chooseColor(props.getValue()[0], ev)}
            />
          )}
          {children}
        </chakraComponents.Control>
      ),
    };
  }, [onChange]);

  return (
    <FormControl isRequired={isRequired}>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>
      <CreatableSelect
        id={fieldId}
        menuPortalTarget={document.body}
        styles={{
          menuPortal: (base) => ({ ...base, zIndex: 10 }),
        }}
        chakraStyles={{
          control: (base) => ({ ...base, bg: value?.color ?? undefined }),
        }}
        colorScheme="pgreen"
        selectedOptionColor="pgreen.400"
        value={value ?? undefined}
        onChange={onChange}
        isMulti={false}
        options={categs ?? []}
        isClearable={!isRequired}
        name={name}
        components={customComponents}
        placeholder={t(label || name)}
        noOptionsMessage={() => t("emptyState")}
        getOptionLabel={(v) => v.name}
        getOptionValue={(v) => v.slug}
        getNewOptionData={(input) => ({ color: "#000000", name: input, slug: slugify(input, slugifyOptions) })}
      />
      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}
    </FormControl>
  );
}

function getCategDTO(created: boolean, newItem: PostCategory | null, currentItem: PostCategory | null) {
  if (newItem) {
    if (created) return { create: newItem };
    else if (newItem.slug === currentItem?.slug) return { update: newItem };
    else return { connect: { slug: newItem.slug } };
  } else if (currentItem) {
    return { disconnect: true };
  }
}
