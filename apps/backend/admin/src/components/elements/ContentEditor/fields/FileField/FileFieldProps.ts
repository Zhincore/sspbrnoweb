import { FileRecord, MediaRecord, ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import ContentEditor from "../../ContentEditor";

type FileFieldPropsSingleFile<ContentType extends keyof ContentTypes> = {
  single: true;
  media?: false;
  name: KeysOfType<ContentTypes[ContentType], FileRecord | null | undefined>;
};
type FileFieldPropsSingleMedia<ContentType extends keyof ContentTypes> = {
  single: true;
  media: true;
  name: KeysOfType<ContentTypes[ContentType], MediaRecord | null | undefined>;
};
type FileFieldPropsSingle<ContentType extends keyof ContentTypes> =
  | FileFieldPropsSingleFile<ContentType>
  | FileFieldPropsSingleMedia<ContentType>;

type FileFieldPropsMultiFile<ContentType extends keyof ContentTypes> = {
  single?: false;
  media?: false;
  name: KeysOfType<ContentTypes[ContentType], FileRecord[] | undefined>;
};
type FileFieldPropsMultiMedia<ContentType extends keyof ContentTypes> = {
  single?: false;
  media: true;
  name: KeysOfType<ContentTypes[ContentType], MediaRecord[] | undefined>;
};
type FileFieldPropsMulti<ContentType extends keyof ContentTypes> =
  | FileFieldPropsMultiFile<ContentType>
  | FileFieldPropsMultiMedia<ContentType>;

export type FileFieldProps<ContentType extends keyof ContentTypes> = (
  | FileFieldPropsSingle<ContentType>
  | FileFieldPropsMulti<ContentType>
) & {
  editor: ContentEditor<ContentType>;
  label?: string;
  allowedTypes?: string[];
  mimePrefix?: string[];
  helperText?: string;
  isRequired?: boolean;
};
