import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import { FormControl, FormLabel, FormHelperText } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import YAML from "js-yaml";
import { Prisma } from "$shared/prisma";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import ContentEditor from "../ContentEditor";

const Editor = dynamic(() => import("$elements/MDEditor/Editor"), { ssr: false });

type JsonYamlFieldProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  label?: string;
  name: KeysOfType<ContentTypes[ContentType], Prisma.JsonValue | null>;
  isRequired?: boolean;
  height?: number | string;
  placeholder?: string;
  helperText?: string;
};

export function JsonYamlField<ContentType extends keyof ContentTypes>({
  label,
  name,
  editor,
  isRequired,
  height,
  placeholder,
  helperText,
}: JsonYamlFieldProps<ContentType>) {
  const { t } = useTranslation();
  const [yaml, setYaml] = useState("");
  const fieldId = editor.type + "-" + name;
  useEffect(() => {
    const update = () => {
      const value = editor.getValue(name);
      const _obj = value ?? [];
      if (_obj) setYaml(YAML.dump(_obj));
    };
    update();
    editor.events.on("update", update);
    editor.events.on("revert", update);
    return () => {
      editor.events.off("update", update);
      editor.events.off("revert", update);
    };
  }, [name, editor, editor.ready]);

  const onChange = (val: string) => {
    setYaml(val);
    try {
      const _obj = YAML.load(val);
      if (_obj) editor.setValue(name, _obj);
    } catch (err) {
      if (err instanceof Error && err.name.startsWith("YAML")) return;
      throw err;
    }
  };

  return (
    <FormControl isRequired={isRequired}>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>

      <Editor
        id={fieldId}
        value={yaml}
        onChange={onChange}
        height={height}
        placeholder={placeholder ?? t(label || name)}
        mode="yaml"
      />

      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}
    </FormControl>
  );
}
