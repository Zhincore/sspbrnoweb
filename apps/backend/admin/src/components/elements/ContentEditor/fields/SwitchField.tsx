import { useTranslation } from "react-i18next";
import { Switch, FormControl, FormLabel, FormHelperText } from "@chakra-ui/react";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import ContentEditor from "../ContentEditor";

type SwitchFieldProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  label?: string;
  name: KeysOfType<ContentTypes[ContentType], boolean | null>;
  helperText?: string;
};

export function SwitchField<ContentType extends keyof ContentTypes>({
  label,
  name,
  editor,
  helperText,
}: SwitchFieldProps<ContentType>) {
  const { t } = useTranslation();
  const fieldId = editor.type + "-" + name;
  const value = editor.getValue(name);

  return (
    <FormControl>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>
      <Switch
        id={fieldId}
        size="lg"
        name={name}
        isChecked={value ?? false}
        onChange={({ target }) => editor.setValue(name, target.checked)}
      />
      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}
    </FormControl>
  );
}
