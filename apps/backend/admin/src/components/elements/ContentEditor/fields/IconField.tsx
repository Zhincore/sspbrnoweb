import { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  FormControl,
  FormLabel,
  Button,
  FormHelperText,
  Popover,
  PopoverArrow,
  PopoverTrigger,
  PopoverContent,
} from "@chakra-ui/react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import FAIcon from "$shared/components/FAIcon";
import usePromise from "$shared/hooks/usePromise";
import IconSelector, { getIcons } from "$parts/IconSelector";
import ContentEditor from "../ContentEditor";

type IconFieldProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  isRequired?: boolean;
  label?: string;
  name: KeysOfType<ContentTypes[ContentType], string | null>;
  helperText?: string;
};

export function IconField<ContentType extends keyof ContentTypes>({
  label,
  name,
  isRequired,
  editor,
  helperText,
}: IconFieldProps<ContentType>) {
  const [icons] = usePromise(() =>
    getIcons().then((a) => {
      library.add(...a);
      return a;
    }),
  );
  const { t } = useTranslation();
  const [isOpen, setIsOpen] = useState(false);
  const open = () => setIsOpen(true);
  const close = () => setIsOpen(false);
  const fieldId = editor.type + "-" + name;
  const value = editor.getValue(name);

  return (
    <FormControl isRequired={isRequired}>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>

      <Popover isLazy isOpen={isOpen} onOpen={open} onClose={close}>
        <PopoverTrigger>
          <Button
            variant="outline"
            w="full"
            justifyContent="start"
            leftIcon={<FAIcon icon={(icons ? value : null) ?? faSearch} />}
            overflow="hidden"
          >
            {value}
          </Button>
        </PopoverTrigger>
        <PopoverContent w="auto">
          <PopoverArrow />
          <IconSelector
            selected={value}
            onSelect={(icon) => {
              editor.setValue(name, icon);
              close();
            }}
          />
        </PopoverContent>
      </Popover>

      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}
    </FormControl>
  );
}
