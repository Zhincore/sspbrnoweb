import { useEffect, useRef, useCallback } from "react";
import {
  FormControl,
  FormLabel,
  FormHelperText,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Input,
  InputProps,
  IconButton,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRedo } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import ContentEditor from "../ContentEditor";

const CREATE_FROM_DEBOUNCE = 50;

type TextFieldProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  label?: string;
  name: KeysOfType<ContentTypes[ContentType], string | null>;
  createFrom?: KeysOfType<ContentTypes[ContentType], string | null>;
  filter?: (value: string) => string | Promise<string>;
  type?: InputProps["type"];
  isInline?: boolean;
  isRequired?: boolean;
  placeholder?: string;
  helperText?: string;
  icon?: React.ReactNode;
};

export function TextField<ContentType extends keyof ContentTypes>({
  label,
  name,
  type,
  editor,
  isRequired,
  placeholder,
  helperText,
  icon,
  filter,
  createFrom,
}: TextFieldProps<ContentType>) {
  const { t } = useTranslation();
  const fieldId = editor.type + "-" + name;
  const value = editor.getValue(name);
  const supvalue = createFrom ? editor.getValue(createFrom) : null;
  const lastValueRef = useRef(supvalue);

  const onChange = useCallback(
    async (_value: string) => {
      if (editor.isLoading) return;
      if (filter) _value = await filter(_value);
      editor.setValue(name, _value);
      return _value;
    },
    [editor, filter, name],
  );
  useEffect(() => {
    // debounce
    let timeout: NodeJS.Timeout | undefined = setTimeout(() => {
      if (typeof supvalue === "string" && (!value || lastValueRef.current === value)) {
        onChange(supvalue).then((v) => (lastValueRef.current = v));
      }
      timeout = undefined;
    }, CREATE_FROM_DEBOUNCE);

    return () => {
      if (timeout) clearTimeout(timeout);
    };
  }, [supvalue, value, onChange]);

  return (
    <FormControl isRequired={isRequired}>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>

      <InputGroup>
        {icon && <InputLeftElement pointerEvents="none">{icon}</InputLeftElement>}
        <Input
          id={fieldId}
          type={type}
          placeholder={placeholder ? t(placeholder) : t(label || name)}
          name={name}
          value={value ?? ""}
          onChange={({ target }) => onChange(target.value)}
        />

        {createFrom && (
          <InputRightElement>
            <IconButton
              variant="ghost"
              aria-label={t("reset")}
              title={t("reset")}
              icon={<FontAwesomeIcon icon={faRedo} />}
              onClick={() => editor.setValue(name, "")}
            />
          </InputRightElement>
        )}
      </InputGroup>

      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}
    </FormControl>
  );
}
