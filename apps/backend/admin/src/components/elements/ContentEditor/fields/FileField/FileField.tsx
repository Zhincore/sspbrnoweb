import { useState, useMemo } from "react";
import dynamic from "next/dynamic";
import { useTranslation } from "react-i18next";
import {
  Box,
  FormControl,
  FormLabel,
  FormHelperText,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ButtonGroup,
  Button,
  IconButton,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFile, faImage, faFolderOpen, faTimes, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FileRecord, MediaRecord, ContentTypes } from "$shared/APITypes";
import { ensureArray } from "~/lib/utils";
import server from "~/lib/server";
import { FileFieldProps } from "./FileFieldProps";

const FileIcon = dynamic(() => import("$elements/FileBrowser/FileItem/FileIcon"));
const FileSelector = dynamic(() => import("$elements/FileBrowser/FileSelector"));
const FileBrowser = dynamic(() => import("$elements/FileBrowser"));

// WARNING: getValue<any> and setValue<any> bypasses type-checking of content
export function FileField<ContentType extends keyof ContentTypes>({
  editor,
  name,
  label,
  media,
  single,
  helperText,
  allowedTypes,
  mimePrefix,
  isRequired,
}: FileFieldProps<ContentType>) {
  type Value = MediaRecord | FileRecord | MediaRecord[] | FileRecord[];

  const [selected, setSelected] = useState<FileRecord[]>([]);
  const [isViewing, setViewing] = useState(false);
  const [isEditing, setEditing] = useState(false);
  const { t } = useTranslation();
  const selection: Value = editor.getValue<any>(name);
  const fieldId = editor.type + "-" + name;
  const multi = Array.isArray(selection);
  const files = useMemo(() => {
    const selections = multi ? selection : [selection];
    const _files = selection ? selections : [];
    if (media) return (_files as MediaRecord[]).map(server.files.mediaToFile);
    return _files as FileRecord[];
  }, [media, multi, selection]);
  const value = useMemo(() => {
    if (multi || !selection) return t("filesSelected", { count: selection ? selection.length : 0 });
    return "name" in selection ? selection.name : selection.file.name;
  }, [selection, multi, t]);

  const onOpen = () => {
    if (single) return setEditing(true);
    setViewing(true);
  };

  const onClear = () => {
    editor.setValue<any>(name, single ? null : ([] as any), single ? { disconnect: true } : { set: [] });
  };

  const onAdd = (_file?: FileRecord) => {
    let newSelection: MediaRecord[] | FileRecord[] = (_file ? [_file] : selected).filter(Boolean);
    if (!newSelection.length) return;

    if (media) newSelection = newSelection.map(server.files.fileToMedia);

    editor.setValue<any>(
      name,
      single ? newSelection[newSelection.length - 1] : ([...ensureArray(selection || []), ...newSelection] as any),
      single
        ? { connect: { id: newSelection[newSelection.length - 1].id } }
        : { set: [...ensureArray(selection || []), ...newSelection].map(({ id }: any) => ({ id })) },
    );
    setSelected([]);
  };

  const icon = media ? faImage : faFile;
  return (
    <FormControl isRequired={isRequired}>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>

      <InputGroup>
        <InputLeftElement pointerEvents="none">
          {files.length ? (
            <FileIcon file={files[0]} height={24} iconMargin={0} rounded="none" key={files[0]?.id ?? ""} />
          ) : (
            <FontAwesomeIcon fixedWidth icon={icon} />
          )}
        </InputLeftElement>

        <Input
          id={fieldId}
          type="text"
          name={name}
          value={value}
          onChange={(ev) => {
            ev.preventDefault();
            onOpen();
          }}
          onClick={() => onOpen()}
        />

        <InputRightElement w={20} justifyContent="flex-end">
          <IconButton
            display={files.length ? undefined : "none"}
            variant="ghost"
            aria-label={t("clearSelection")}
            title={t("clearSelection")}
            onClick={onClear}
            icon={<FontAwesomeIcon icon={faTimes} />}
          />

          <IconButton
            variant="ghost"
            aria-label={t("changeSelection")}
            title={t("changeSelection")}
            onClick={onOpen}
            icon={<FontAwesomeIcon icon={faFolderOpen} />}
          />
        </InputRightElement>
      </InputGroup>

      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}

      {/* SELECTED FILES VIEWER */}
      <Modal isOpen={isViewing} onClose={() => setViewing(false)} size="6xl">
        <ModalOverlay />
        <ModalContent alignSelf="stretch">
          <ModalHeader>{t(label || name)}</ModalHeader>
          <ModalCloseButton />
          <ModalBody h="100%">
            <Box py={2}>
              <Button
                colorScheme="pgreen"
                onClick={() => setEditing(true)}
                leftIcon={<FontAwesomeIcon icon={faPlus} />}
              >
                {t("addFiles")}
              </Button>
            </Box>

            <FileSelector cd={() => null} path="/" files={files} />
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="pgreen" onClick={() => setViewing(false)}>
              {t("done")}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      {/* ADD FILES */}
      <Modal isOpen={isEditing} onClose={() => setEditing(false)} size="6xl">
        <ModalOverlay />
        <ModalContent alignSelf="stretch">
          <ModalHeader>{t(label || name)}</ModalHeader>
          <ModalCloseButton />
          <ModalBody h="100%">
            <FileBrowser
              singleSelect={single}
              fileSelect
              setSelected={setSelected}
              selected={selected}
              allowedTypes={allowedTypes}
              hideFiles={(!single && selection && (selection as FileRecord[]).map((v) => v.id)) || undefined}
              mimePrefix={mimePrefix}
              onFileOpen={(file) => {
                onAdd(file);
                setEditing(false);
              }}
            />
          </ModalBody>

          <ModalFooter>
            <ButtonGroup>
              <Button
                colorScheme="pgreen"
                isDisabled={!selected.length}
                onClick={() => {
                  onAdd();
                  setEditing(false);
                }}
              >
                {t("select")}
              </Button>
              <Button variant="ghost" onClick={() => setEditing(false)}>
                {t("cancel")}
              </Button>
            </ButtonGroup>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </FormControl>
  );
}
