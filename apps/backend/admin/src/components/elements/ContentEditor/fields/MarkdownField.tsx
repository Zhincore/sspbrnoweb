import dynamic from "next/dynamic";
import { FormControl, FormLabel, FormHelperText } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import ContentEditor from "../ContentEditor";

const MDEditor = dynamic(() => import("$elements/MDEditor"), { ssr: false });

type MarkdownFieldProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  label?: string;
  name: KeysOfType<ContentTypes[ContentType], string | null>;
  isRequired?: boolean;
  height?: number | string;
  placeholder?: string;
  helperText?: string;
};

export function MarkdownField<ContentType extends keyof ContentTypes>({
  label,
  name,
  editor,
  isRequired,
  height,
  placeholder,
  helperText,
}: MarkdownFieldProps<ContentType>) {
  const { t } = useTranslation();
  const value = editor.getValue(name);
  const fieldId = editor.type + "-" + name;

  return (
    <FormControl isRequired={isRequired}>
      <FormLabel htmlFor={fieldId}>{t(label || name)}</FormLabel>

      <MDEditor
        id={fieldId}
        value={value ?? ""}
        onChange={(val) => editor.isLoading || editor.setValue(name, val)}
        height={height}
        placeholder={placeholder ?? t(label || name)}
      />

      {helperText && <FormHelperText>{t(helperText)}</FormHelperText>}
    </FormControl>
  );
}
