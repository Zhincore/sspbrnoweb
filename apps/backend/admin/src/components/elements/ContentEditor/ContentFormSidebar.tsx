import { useState } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import { VStack, StackItem, ButtonGroup, Button, Stat, StatLabel, StatNumber } from "@chakra-ui/react";
import { ContentTypes } from "$shared/APITypes";
import { content } from "~/lib/server";
import ModalSimple from "$elements/ModalSimple";
import ContentEditor from "./";

type ContentFormSidebarProps<ContentType extends keyof ContentTypes> = {
  editor: ContentEditor<ContentType>;
  data: ContentTypes[ContentType];
};

export default function ContentFormSidebar<ContentType extends keyof ContentTypes>({
  editor,
  data,
}: ContentFormSidebarProps<ContentType>) {
  const { t } = useTranslation();
  const router = useRouter();
  const [deleting, setDeleting] = useState(false);

  return (
    <VStack minW="max(20%, 240px)">
      {data && (
        <StackItem p={2} w="full">
          {"createdAt" in data && (
            <Stat>
              <StatLabel>{t("createdAt")}:</StatLabel>
              <StatNumber>{new Date(data.createdAt).toLocaleString()}</StatNumber>
            </Stat>
          )}
          {"updatedAt" in data && (
            <Stat>
              <StatLabel>{t("updatedAt")}:</StatLabel>
              <StatNumber>{new Date(data.updatedAt).toLocaleString()}</StatNumber>
            </Stat>
          )}
        </StackItem>
      )}

      <VStack w="full">
        <Button
          type="submit"
          isFullWidth
          colorScheme="pgreen"
          loadingText={t("saving")}
          isLoading={editor.isLoading}
          isDisabled={!editor.hasChanged}
        >
          {t(editor.isCreating ? "create" : "saveChanges")}
        </Button>
        <Button type="button" isFullWidth isDisabled={!editor.hasChanged} onClick={() => editor.revert()}>
          {t("revert")}
        </Button>
        {!editor.isCreating && !editor.isSingleton && (
          <>
            <Button
              variant="ghost"
              isFullWidth
              isLoading={editor.isLoading}
              loadingText={t("saving")}
              type="button"
              onClick={() => setDeleting(true)}
            >
              {t("delete")}
            </Button>

            <ModalSimple
              isOpen={deleting}
              onClose={() => setDeleting(false)}
              closeButton
              header={t("confirmDelete")}
              body={t("confirmDelete")}
              footer={
                <ButtonGroup>
                  <Button variant="ghost" onClick={() => setDeleting(false)}>
                    {t("cancel")}
                  </Button>

                  <Button
                    colorScheme="red"
                    onClick={() =>
                      content.remove(editor.props.contentType, editor.props.id).then(() => router.push("./"))
                    }
                  >
                    {t("delete")}
                  </Button>
                </ButtonGroup>
              }
            />
          </>
        )}
      </VStack>
    </VStack>
  );
}
