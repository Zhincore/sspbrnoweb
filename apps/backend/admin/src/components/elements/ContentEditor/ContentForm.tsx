import { useEffect } from "react";
import { Stack, StackItem, useToast } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import type ContentEditor from "./ContentEditor";
import ContentFormSidebar from "./ContentFormSidebar";
import PreventNavigation from "./PreventNavigation";

type ContentFormData = {
  editor: ContentEditor<any>;
  data: any;
  children?: React.ReactNode;
};

export default function ContentForm({ editor, data, children }: ContentFormData) {
  const { t } = useTranslation();
  const toast = useToast();

  useEffect(() => {
    const listener = (promise: Promise<unknown>) => {
      const toastId = toast({ title: t("saving") })!;
      promise.then(
        () => toast.update(toastId, { title: t("saved"), status: "success" }),
        () => toast.update(toastId, { title: t("savingError"), status: "error" }),
      );
    };

    editor.events.on("savePromise", listener);

    return () => {
      editor.events.off("savePromise", listener);
    };
  }, [editor, t, toast]);

  const onSubmit = (ev: React.FormEvent) => {
    ev.preventDefault();
    editor.save();
  };

  return (
    <Stack direction={["column", "row"]} as="form" autoComplete="off" onSubmit={onSubmit} spacing={6}>
      <StackItem flex={1} display="flex" flexDirection="row" flexWrap="wrap">
        {children}
      </StackItem>

      <ContentFormSidebar data={data} editor={editor} />

      <PreventNavigation editor={editor} />
    </Stack>
  );
}
