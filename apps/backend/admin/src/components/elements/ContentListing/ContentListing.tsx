import { useRouter } from "next/router";
import Link from "next/link";
import { useState, useMemo, useEffect } from "react";
import {
  Box,
  Flex,
  Table,
  Thead,
  Tbody,
  Th,
  Td,
  Tr,
  TableRowProps,
  ButtonGroup,
  Button,
  Stack,
  InputGroup,
  Input,
  InputLeftElement,
} from "@chakra-ui/react";
import { motion, AnimatePresence } from "framer-motion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMagnifyingGlass,
  faTrashAlt,
  faPlus,
  faCaretUp,
  faCaretDown,
  faLemon,
} from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import { ContentTypes } from "$shared/APITypes";
import { KeysOfType } from "$shared/utils";
import { content } from "~/lib/server";
import ModalSimple from "$elements/ModalSimple";

const getFuse = () => import("fuse.js").then((m) => m.default);

const MotionTr = motion<TableRowProps>(Tr);

const SEARCH_DEBOUNCE = 250;

type FieldType = "number" | "date";

type ContentListingProps<ContentType extends keyof ContentTypes> = {
  contentType: ContentType;
  defaultShownFields: KeysOfType<ContentTypes[ContentType], any>[];
  defaultSortField?: KeysOfType<ContentTypes[ContentType], any>;
  defaultSortDir?: "asc" | "desc";
  fieldTypes?: Partial<Record<keyof ContentTypes[ContentType], FieldType>>;
};

export default function ContentListing<ContentType extends content.ListableContentTypesEnum>({
  contentType,
  defaultShownFields,
  defaultSortField,
  defaultSortDir,
  fieldTypes,
}: ContentListingProps<ContentType>) {
  const idField = content.idFields[contentType];

  const router = useRouter();
  const { t } = useTranslation();
  const [shownFields /*, setShownFields*/] = useState(defaultShownFields);
  const [sortDir, setSortDir] = useState(defaultSortDir ?? "desc");
  const [sortField, setSortField] = useState<keyof ContentTypes[ContentType]>(defaultSortField ?? shownFields[0]);
  const [data, updateData] = content.useList<ContentType>(
    contentType,
    useMemo(() => ({ pageSize: null, fields: [...shownFields, idField] }), [shownFields, idField]),
  );
  const [deleting, setDeleting] = useState<any>();
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState(data);

  const onSort = (column: number) => {
    const field = shownFields[column];
    if (field === sortField) setSortDir(sortDir === "asc" ? "desc" : "asc");
    setSortField(field);
  };

  useEffect(() => {
    updateData();
  }, [updateData]);

  useEffect(() => {
    if (!search || !data) return setFilteredData(data);

    let timeout: NodeJS.Timeout | undefined = setTimeout(() => {
      timeout = undefined;

      getFuse().then((Fuse) => {
        const fuse = new Fuse(data, {
          keys: content.scalarFields[contentType],
        });
        setFilteredData(fuse.search(search).map((result) => result.item));
      });
    }, SEARCH_DEBOUNCE);

    return () => {
      if (timeout) clearTimeout(timeout);
    };
  }, [contentType, search, data]);

  const sortedData = useMemo(() => {
    if ((!sortField && !sortDir) || !filteredData) return filteredData;

    const sorted = [...filteredData].sort((a, b) => {
      const valA: unknown = a[sortField];
      const valB: unknown = b[sortField];
      if (!fieldTypes || !(sortField in fieldTypes)) return (valA as string).localeCompare(valB as string);
      const type = fieldTypes[sortField];

      if (type === "number") return (valA as number) - (valB as number);
      if (type === "date") return new Date(valA as string).getTime() - new Date(valB as string).getTime();

      return 0;
    });
    return sortDir === "asc" ? sorted : sorted.reverse();
  }, [fieldTypes, sortField, sortDir, filteredData]);

  const processField = (field: KeysOfType<ContentTypes[ContentType], any>, value: any) => {
    if (!fieldTypes || !(field in fieldTypes)) return value;

    if (fieldTypes[field] === "date") return new Date(value).toLocaleString();

    return value;
  };

  return (
    <>
      <Stack direction={["column", "row"]} mb={4} justify="space-between">
        <div />

        <InputGroup maxW={96}>
          <InputLeftElement>
            <FontAwesomeIcon icon={faMagnifyingGlass} />
          </InputLeftElement>
          <Input
            aria-label={t("search")}
            placeholder={t("search")}
            value={search}
            onChange={({ target }) => setSearch(target.value)}
          />
        </InputGroup>

        <Link href={router.asPath + "_new"} passHref>
          <Button colorScheme="pgreen" leftIcon={<FontAwesomeIcon icon={faPlus} />}>
            {t(`new_${contentType}`)}
          </Button>
        </Link>
      </Stack>

      <Box maxW="100%" overflowX="auto" overflowY="visible">
        <Table>
          <Thead>
            <Tr>
              {/* <Th select={{ isSelected: false }} /> */}

              {shownFields.map((field, i) => (
                <Th key={i} onClick={() => onSort(i)} cursor="pointer">
                  {t(field)}

                  <Box as="span" position="relative" visibility={sortField === field ? "visible" : "hidden"}>
                    <FontAwesomeIcon icon={sortDir === "asc" ? faCaretDown : faCaretUp} fixedWidth />
                  </Box>
                </Th>
              ))}

              <Th>{t("actions")}</Th>
            </Tr>
          </Thead>
          <Tbody>
            <AnimatePresence key={sortedData ? "a" : "b"}>
              {sortedData && sortedData.length ? (
                sortedData.map((item) => (
                  <MotionTr
                    key={String(item[idField as keyof typeof item])}
                    layout
                    onClick={() => router.push(router.asPath + item[idField as keyof typeof item])}
                    cursor="pointer"
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1, transition: { duration: 0.2 } }}
                    exit={{ opacity: 0, transition: { duration: 0.2 } }}
                  >
                    {/* <Td select={{ rowIndex: i, isSelected: false }}/> */}

                    {shownFields.map((field, j) => (
                      <Td key={j}>{processField(field, item[field])}</Td>
                    ))}

                    <Td onClick={(ev) => ev.stopPropagation()}>
                      <Button
                        variant="danger"
                        title={t("delete")}
                        onClick={() => setDeleting(item[idField as keyof typeof item])}
                      >
                        <FontAwesomeIcon icon={faTrashAlt} />
                      </Button>
                    </Td>
                  </MotionTr>
                ))
              ) : (
                <MotionTr
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1, transition: { duration: 0.2 } }}
                  exit={{ opacity: 0, transition: { duration: 0.2 } }}
                >
                  <Td colSpan={shownFields.length + 1} textAlign="center" py={[16, 32]}>
                    <Flex direction="column" fontSize="150%">
                      <Box as={FontAwesomeIcon} icon={faLemon} size="3x" mb={8} />
                      <Box as={"span"}>{t("emptyState")}</Box>
                    </Flex>
                  </Td>
                </MotionTr>
              )}
            </AnimatePresence>
          </Tbody>
        </Table>
      </Box>

      <ModalSimple
        isOpen={!!deleting}
        onClose={() => setDeleting(undefined)}
        header={t("confirmDelete")}
        body={t("changesUnsaved")}
        footer={
          <ButtonGroup>
            <Button
              colorScheme="pred"
              onClick={() => {
                content.remove(contentType, deleting);
                setDeleting(undefined);
              }}
            >
              {t("delete")}
            </Button>
            <Button variant="outline" onClick={() => setDeleting(undefined)}>
              {t("cancel")}
            </Button>
          </ButtonGroup>
        }
      />
    </>
  );
}
