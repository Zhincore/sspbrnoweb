import { Box, BoxProps } from "@chakra-ui/react";
import LogoImg from "$public/assets/pcms.svg";

export default function Logo(props: BoxProps) {
  return <Box as={LogoImg} title="P CMS" viewBox="0 72 395 93" {...props} />;
}
