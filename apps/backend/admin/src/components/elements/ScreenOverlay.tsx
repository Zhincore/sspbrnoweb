import { chakra, Center, CenterProps } from "@chakra-ui/react";
import { motion } from "framer-motion";

const MotionCenter = motion<CenterProps>(Center);

export default chakra(MotionCenter, {
  baseStyle: {
    position: "fixed",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 49,
    bg: "bg",
  },
  label: "ScreenOverlay",
});
