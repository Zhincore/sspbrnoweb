import { IconButton, IconButtonProps, Tooltip, TooltipProps } from "@chakra-ui/react";

type TooltipIconButtonProps = Omit<IconButtonProps, "aria-label" | "label"> & {
  label: string;
  hasArrow?: TooltipProps["hasArrow"];
  placement?: TooltipProps["placement"];
};

export default function TooltipIconButton({ label, hasArrow, placement, ...props }: TooltipIconButtonProps) {
  return (
    <Tooltip label={label} hasArrow={hasArrow ?? true} placement={placement ?? "top"}>
      <IconButton aria-label={label} {...props} />
    </Tooltip>
  );
}
