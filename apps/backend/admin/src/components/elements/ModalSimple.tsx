import dynamic from "next/dynamic";
import { ModalProps, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody } from "@chakra-ui/react";
import type { CloseButtonProps } from "@chakra-ui/react";

const ModalCloseButton = dynamic<CloseButtonProps>(() =>
  import(
    /* webpackExports: ["ModalCloseButton"] */
    "@chakra-ui/react"
  ).then((m) => m.ModalCloseButton),
);

type ModalSimpleProps = Omit<ModalProps, "children"> & {
  header?: React.ReactNode;
  body?: React.ReactNode;
  footer?: React.ReactNode;

  closeButton?: boolean;
};

export default function ModalSimple({ header, body, footer, closeButton, ...props }: ModalSimpleProps) {
  return (
    <Modal {...props}>
      <ModalOverlay />
      <ModalContent>
        {header && <ModalHeader>{header}</ModalHeader>}
        {closeButton && <ModalCloseButton />}
        {body && <ModalBody>{body}</ModalBody>}

        {footer && <ModalFooter>{footer}</ModalFooter>}
      </ModalContent>
    </Modal>
  );
}
