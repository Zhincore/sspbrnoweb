import { useState, useEffect, useMemo, useCallback, useRef } from "react";
import dynamic from "next/dynamic";
import { Flex } from "@chakra-ui/react";
import { FileRecord, FileNodeRecord } from "$shared/APITypes";
import server from "~/lib/server";
import FileSelector, { getNextFile } from "./FileSelector";
import FilesToolbar from "./FilesToolbar";

const FileViewer = dynamic(() => import("./FileViewer"), { ssr: false });

type FileBrowserPropsControlledFile = {
  fileSelect: true;
  setSelected: (files: FileRecord[]) => void;
  selected: FileRecord[];
};
type FileBrowserPropsControlled = {
  fileSelect?: false;
  setSelected: (id: (string | number)[]) => void;
  selected: (string | number)[];
};

type FileBrowserPropsUncontrolled = {
  fileSelect?: never;
  setSelected?: never;
  selected?: never;
};

type FileBrowserProps = (FileBrowserPropsControlledFile | FileBrowserPropsControlled | FileBrowserPropsUncontrolled) & {
  path?: string; // If defined, switches to controlled mode
  singleSelect?: boolean;
  allowedTypes?: string[];
  mimePrefix?: string[];
  hideFiles?: (string | number)[];
  onSelect?: (id: string | number) => void;
  onFileOpen?: (file: FileRecord) => void;
  onChange?: (path: string) => void;
};

export default function FileBrowser({
  path: aPath,
  onChange,
  setSelected,
  singleSelect,
  fileSelect,
  onFileOpen,
  selected,
  allowedTypes,
  mimePrefix,
  hideFiles,
}: FileBrowserProps) {
  const [ownSelected, setOwnSelected] = useState<(string | number)[]>([]);
  const [viewing, setViewing] = useState<FileRecord>();
  const [path, _setPath] = useState(aPath ?? "/");
  const [folder, updateFolder] = server.files.useFolder(path);
  const dirs = useMemo(() => path.split("/").filter((v) => v), [path]);
  const fileSelector = useRef<FileSelector>(null);

  useEffect(() => {
    updateFolder();
  }, [updateFolder, path]);

  const files: FileNodeRecord[] | null = useMemo(() => {
    if (!folder) return null;
    return [...folder.folders, ...folder.files].filter(
      (file) =>
        (!hideFiles || !hideFiles.includes(file.id)) &&
        ("ext" in file
          ? (!mimePrefix || mimePrefix.some((prefix) => file.mime.startsWith(prefix))) &&
            (!allowedTypes ||
              allowedTypes.some((type) => type === file.mime.split(";")[0] || type.slice(1) === file.ext))
          : true),
    );
  }, [folder, allowedTypes, hideFiles, mimePrefix]);

  useEffect(() => {
    if (aPath && path !== aPath) _setPath(aPath);
  }, [path, aPath]);

  const setPath = useCallback(
    (_path: string) => {
      if (onChange) onChange(_path);
      if (!aPath) _setPath(_path);
    },
    [aPath, onChange],
  );
  const doUpload = useCallback(
    (isFolder = false) =>
      fileSelector.current!.upload(path, isFolder, [
        ...(allowedTypes || []),
        ...(mimePrefix || []).map((v) => v + "/*"),
      ]),
    [allowedTypes, mimePrefix, path],
  );

  const selectedMapped = fileSelect ? selected.map(({ id }) => id) : selected;
  const _selected = !selected ? ownSelected : selectedMapped;

  const _setExtSelected = fileSelect
    ? (id: (string | number)[]) =>
        (setSelected as (selectedFiles: FileRecord[]) => void)(
          id.map((_id) => files?.find((file) => _id === file.id)).filter(Boolean) as FileRecord[],
        )
    : (setSelected as (id: (string | number)[]) => void);
  const _setSelected = !setSelected ? setOwnSelected : _setExtSelected;

  const _onFileOpen = onFileOpen || ((file: FileRecord) => setViewing(file));

  const onViewNext = (direction: -1 | 1 = 1) => {
    if (!folder || !viewing) return;
    const next = getNextFile(folder.files, viewing.id, direction);
    if (!next || !("size" in next)) return;

    setViewing(next);
    fileSelector.current?.select([next.id]);
  };

  return (
    <Flex w="100%" h="100%" direction="column" flexGrow={1}>
      <FilesToolbar
        parentFolderId={folder?.parentId ?? null}
        selection={_selected}
        ownSelection={ownSelected}
        fileSelector={fileSelector.current!}
        {...{ doUpload, dirs, path, setPath }}
      />

      <FileSelector
        files={files ?? []}
        path={path}
        ref={fileSelector}
        setSelected={_setSelected}
        singleSelect={singleSelect}
        fileSelect={fileSelect}
        onFileOpen={_onFileOpen}
        cd={(_folder) => setPath("/" + [...dirs, _folder].join("/"))}
      />

      <FileViewer
        file={viewing}
        onClose={() => setViewing(undefined)}
        onNext={() => onViewNext()}
        onPrev={() => onViewNext(-1)}
      />
    </Flex>
  );
}
