import { Component } from "react";
import dynamic from "next/dynamic";
import { EventEmitter } from "eventemitter3";
import TypedEmitter from "typed-emitter";
import { FileNodeRecord, FileRecord } from "$shared/APITypes";
import server from "~/lib/server";

const FileSelectorRenderer = dynamic(() => import("./FileSelectorRenderer"), { ssr: false });

type FileSelectorProps = {
  files?: FileNodeRecord[];
  cd: (folder: string) => void;
  path: string | null;
  setSelected?: (selected: (string | number)[]) => void;
  onFileOpen?: (file: FileRecord) => void;
  singleSelect?: boolean;
  fileSelect?: boolean;
};

type FileSelectorState = {
  selection: (string | number)[];
};

export type FileSelectorEvents = {
  filesDeleted: () => void;
  uploading: (promise: Promise<unknown>) => void;
};

export default class FileSelector extends Component<FileSelectorProps, FileSelectorState> {
  lastSelect?: string | number;
  state: FileSelectorState = {
    selection: [],
  };
  events = new EventEmitter() as TypedEmitter<FileSelectorEvents>;

  componentDidUpdate(oldProps: FileSelectorProps) {
    if (this.props.path !== oldProps.path) {
      this.select(null);
    }
  }

  render() {
    const { files, path } = this.props;
    const { selection } = this.state;

    return <FileSelectorRenderer path={path} files={files} selection={selection} selector={this} />;
  }

  async upload(path = "/", ...args: Parameters<typeof selectFiles>) {
    const files = await selectFiles(...args);

    const promise = server.files.upload(files, path);
    this.events.emit("uploading", promise);

    return promise;
  }

  open(id?: string | number) {
    if (!this.props.files) return false;
    const { selection } = this.state;
    id = id === undefined && selection.length === 1 ? selection[0] : id;
    if (!id === undefined) return false;

    const target = this.props.files.find((item) => item.id === id)!;

    // If directory, then enter it
    if (!("size" in target)) this.props.cd(target.name);
    // If file, trigger open handler
    else if (this.props.onFileOpen) this.props.onFileOpen(target);

    return true;
  }

  // WARNING: Causes "This page is slowing down browser" when used on many files
  delete(ids?: (string | number)[]) {
    if (!this.props.files) return false;
    for (const id of ids ?? this.state.selection) {
      if (!this.props.files.some((file) => file.id === id)) continue;
      server.files.rm(id);
    }
    this.select(null);
    this.events.emit("filesDeleted");
    return true;
  }

  select(ids: (string | number)[] | null, additive?: boolean, noRemove?: boolean) {
    let { selection } = this.state;

    if (!ids) selection.length = 0;
    else if (this.props.singleSelect) {
      selection = [ids[ids.length - 1]];
    } else if (!additive) {
      selection = ids;
    } else if (noRemove) {
      selection = [...new Set([...selection, ...ids])];
    } else {
      selection = [...selection, ...ids].filter((s) => !(selection.includes(s) && ids.includes(s)));
    }

    this.setState({ selection });
    this.lastSelect = selection[selection.length - 1];
    if (this.props.setSelected) this.props.setSelected([...selection]);
  }

  move(target: number, file?: string | number) {
    const selection = new Set(this.state.selection);
    if (file) selection.add(file);
    return server.files.mv(target, Array.from(selection));
  }

  toggleSelectAll() {
    if (this.state.selection.length) this.select(null);
    else if (this.props.files)
      this.select(
        this.props.files.map((file) => file.id),
        true,
      );
  }

  getNextFile(direction: 1 | -1 = 1) {
    return getNextFile(this.props.files, this.lastSelect, direction);
  }

  onSelect(ev: React.MouseEvent | React.KeyboardEvent, id: string | number) {
    const { files } = this.props;
    if (!files) return;

    if ("key" in ev) ev.stopPropagation();
    ev.preventDefault();

    // shift-select
    if (ev.shiftKey) {
      const lastId = this.lastSelect;
      if (!lastId || id === lastId) return this.select([id]);

      const startIndex = files.findIndex((val) => val.id === lastId);
      const endIndex = files.findIndex((val) => val.id === id);
      const indexDelta = endIndex - startIndex;
      const direction = Math.sign(indexDelta);
      const toSelect = [];
      for (let i = 1; i < indexDelta * direction; i++) {
        toSelect.push(files[startIndex + i * direction].id);
      }
      // shift-select
      this.select([...toSelect, id], true);
    }
    // normal/ctrl-select
    else this.select([id], ev.ctrlKey);
  }
  onKeyboard(ev: React.KeyboardEvent) {
    const { files, cd } = this.props;

    if (!files || !files.length) return;

    switch (ev.code) {
      // Enter/Open selected directory/file
      case "Enter": {
        if (this.open()) ev.preventDefault();
        break;
      }

      // Go up
      case "Backspace": {
        cd("..");
        break;
      }

      // Deselect all
      case "Escape": {
        this.select(null);
        break;
      }

      // Toggle select all
      case "KeyA": {
        if (!ev.ctrlKey) break;
        this.toggleSelectAll();
        ev.preventDefault();
        break;
      }

      // Delete selected files
      case "Delete": {
        if (this.delete()) ev.preventDefault();
        break;
      }

      // Select next/previous file
      case "ArrowLeft":
      case "ArrowRight": {
        const direction = ev.key === "ArrowLeft" ? -1 : 1;
        const next = this.getNextFile(direction);
        if (!next) break;
        this.onSelect.apply(this, [ev, next.id]);
        break;
      }
    }
  }
}

export function getNextFile<TFiles extends FileNodeRecord>(
  files?: TFiles[],
  currentId?: string | number,
  direction: 1 | -1 = 1,
) {
  if (!currentId || !files) return;

  const currentIndex = files.findIndex((item) => item.id === currentId);
  return files[(files.length + currentIndex + direction) % files.length];
}

function selectFiles(isFolder = false, allowedTypes?: string[]) {
  const input = document.createElement("input");
  input.setAttribute("type", "file");
  input.setAttribute("multiple", "");
  if (allowedTypes) input.setAttribute("accept", allowedTypes.join(", "));
  if (isFolder) input.setAttribute("webkitdirectory", "");

  return new Promise<FileList | null>((resolve) => {
    input.onchange = (ev) => {
      ev.preventDefault();
      resolve((ev.target as HTMLInputElement).files);
    };
    input.click();
  });
}
