import { useState, useEffect, useMemo } from "react";
import dynamic from "next/dynamic";
import { Box, SimpleGrid, GridItem, GridItemProps, Center, CenterProps, useToast } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
import { AnimatePresence, motion } from "framer-motion";
import { useTranslation } from "react-i18next";
import { FileNodeRecord } from "$shared/APITypes";
import server from "~/lib/server";
import FileItem from "../FileItem";
import FileSelector, { FileSelectorEvents } from "./FileSelector";

const LoadingScreen = dynamic(() => import("$parts/LoadingScreen"));

const MotionGridItem = motion<GridItemProps>(GridItem);
const MotionCenter = motion<CenterProps>(Center);

type FileSelectorRendererProps = {
  files?: FileNodeRecord[];
  selection: (string | number)[];
  path: string | null;
  selector: FileSelector;
};

export default function FileSelectorRenderer({ files, selection, path, selector }: FileSelectorRendererProps) {
  const { t } = useTranslation();
  const toast = useToast();
  const [dropping, setDropping] = useState(false);
  const key = useMemo(() => path, [files]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const listeners: Pick<FileSelectorEvents, "filesDeleted" | "uploading"> = {
      filesDeleted: () => {
        toast({
          title: t("filesDeleted"),
          status: "success",
        });
      },

      uploading: (promise) => {
        const toastId = toast({
          title: t("uploading"),
          duration: null,
        })!;
        promise.then(
          () => {
            toast.update(toastId, { title: t("uploadSuccess"), status: "success" });
          },
          () => {
            toast.update(toastId, { title: t("uploadFail"), status: "error" });
          },
        );
      },
    };

    selector.events.on("filesDeleted", listeners.filesDeleted);
    selector.events.on("uploading", listeners.uploading);

    return () => {
      for (const [event, listener] of Object.entries(listeners)) {
        selector.events.off(event as keyof FileSelectorEvents, listener);
      }
    };
  }, [t, toast, selector]);

  const onDrop = (ev: React.DragEvent) => {
    if (!ev.dataTransfer.files.length || !path) return;
    server.files.upload(ev.dataTransfer.files, path);
    ev.preventDefault();
    setDropping(false);
  };

  const onDragOver = (ev: React.DragEvent) => {
    if (!ev.dataTransfer.types.includes("Files") || !path) return;
    ev.dataTransfer.dropEffect = "move";
    ev.preventDefault();
    if (!dropping) setDropping(true);
  };

  return (
    <Box
      tabIndex={0}
      position="relative"
      flexGrow={1}
      h="100%"
      w="100%"
      p={3}
      bg="blackAlpha.400"
      borderRadius="lg"
      onClick={(ev) => ev.isDefaultPrevented() || selector.select(null)}
      onDrop={onDrop}
      onDragOver={onDragOver}
      onDragLeave={() => setDropping(false)}
      onKeyDown={(ev) => selector.onKeyboard(ev)}
    >
      <Center
        position="absolute"
        top={0}
        left={0}
        bottom={0}
        right={0}
        zIndex={20}
        pointerEvents="none"
        bg="blackAlpha.600"
        opacity={dropping ? 0.75 : 0}
        fontSize="4xl"
      >
        <FontAwesomeIcon icon={faUpload} />
      </Center>

      <AnimatePresence>
        {key !== path && <LoadingScreen position="absolute" bg="blackAlpha.600" />}

        {files &&
          (files.length ? (
            <Box key={key} overflowY="auto" position="absolute" top={0} left={0} bottom={0} right={0}>
              <SimpleGrid as="ul" columns={[2, 3, 5, 6, 7, 8]} gap={2} p={4} pb={16}>
                {files.map((file, i) => (
                  <MotionGridItem
                    initial={{ y: 20, opacity: 0 }}
                    animate={{ y: 0, opacity: 1, transition: { delay: 0.05 * i } }}
                    exit={{ y: -20, opacity: 0, transition: { delay: 0.05 * i } }}
                    key={file.id}
                    layoutId={file.id + ""}
                  >
                    <FileItem
                      data-id={file.id}
                      file={file}
                      selected={selection.includes(file.id)}
                      onMove={(fileId, target) => selector.move(target, fileId)}
                      onClick={(ev) => selector.onSelect(ev, file.id)}
                      onDoubleClick={() => selector.open(file.id)}
                    />
                  </MotionGridItem>
                ))}
              </SimpleGrid>
            </Box>
          ) : (
            <MotionCenter
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              position="absolute"
              top={0}
              left={0}
              flexDirection="column"
              w="100%"
              h="100%"
              fontSize="4xl"
            >
              <FontAwesomeIcon icon={faFolderOpen} size="2x" />
              <Box>{t("noFiles")}</Box>
            </MotionCenter>
          ))}
      </AnimatePresence>
    </Box>
  );
}
