import { useState, useRef } from "react";
import {
  Flex,
  IconButton,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverCloseButton,
  PopoverArrow,
  PopoverHeader,
  PopoverBody,
  FormControl,
  FormLabel,
  visuallyHiddenStyle,
  Input,
  useId,
  useDisclosure,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import { files } from "~/lib/server";

type CreateFolderProps = {
  path: string;
  children: React.ReactNode;
};

export default function CreateFolder({ path, children }: CreateFolderProps) {
  const [value, setValue] = useState("");
  const { isOpen, onClose, onOpen } = useDisclosure({ onClose: () => setValue("") });
  const id = useId(undefined, "createfolder");
  const inputRef = useRef<HTMLInputElement>(null);
  const { t } = useTranslation();

  const onSubmit = (ev: React.FormEvent) => {
    ev.preventDefault();
    if (!value) return;

    files.mkdir(path + "/" + value);
    onClose();
  };

  return (
    <Popover isOpen={isOpen} onClose={onClose} onOpen={onOpen} initialFocusRef={inputRef}>
      <PopoverTrigger>{children}</PopoverTrigger>
      <PopoverContent>
        <PopoverArrow />
        <PopoverCloseButton />
        <PopoverHeader>{t("createFolder")}</PopoverHeader>
        <PopoverBody>
          <Flex as="form" onSubmit={onSubmit}>
            <FormControl>
              <FormLabel htmlFor={id} style={visuallyHiddenStyle}>
                {t("title")}
              </FormLabel>
              <Input
                ref={inputRef}
                roundedRight={0}
                id={id}
                placeholder={t("title")}
                value={value}
                onChange={({ target }) => setValue(target.value)}
              />
            </FormControl>
            <IconButton
              variant="solid"
              height={10}
              roundedLeft={0}
              colorScheme="pgreen"
              type="submit"
              aria-label="submit"
              isDisabled={!value}
              icon={<FontAwesomeIcon icon={faCheck} fixedWidth />}
            />
          </Flex>
        </PopoverBody>
      </PopoverContent>
    </Popover>
  );
}
