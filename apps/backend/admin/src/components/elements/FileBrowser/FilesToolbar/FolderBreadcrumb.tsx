import { Button, ButtonGroup } from "@chakra-ui/react";
import server from "~/lib/server";

type FolderBreadcrumbProps = {
  dirs: string[];
  setPath: (path: string) => void;
};

export default function FolderBreadcrumb({ dirs, setPath }: FolderBreadcrumbProps) {
  return (
    <ButtonGroup m={2} isAttached>
      {["/", ...dirs].map((dir, i) => (
        <Button
          key={i + dir}
          onClick={() => setPath(server.normalizePath(dirs.slice(0, i).join("/")))}
          isActive={i === dirs.length}
        >
          {dir}
        </Button>
      ))}
    </ButtonGroup>
  );
}
