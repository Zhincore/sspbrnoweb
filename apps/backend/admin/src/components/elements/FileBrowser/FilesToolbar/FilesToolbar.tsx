import { useTranslation } from "react-i18next";
import { Box, Flex, ButtonGroup } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFolderPlus,
  faFolder,
  faFolderOpen,
  faArrowUp,
  faFile,
  faTrashAlt,
  faCaretUp,
} from "@fortawesome/free-solid-svg-icons";
import { faSquare, faCheckSquare } from "@fortawesome/free-regular-svg-icons";
import server from "~/lib/server";
import TooltipIconButton from "$elements/TooltipIconButton";
import type FileSelector from "../FileSelector";
import CreateFolder from "./CreateFolder";
import FolderBreadcrumb from "./FolderBreadcrumb";

type FilesToolbarProps = {
  dirs: string[];
  path: string;
  parentFolderId: number | null;
  setPath: (path: string) => void;
  doUpload: (isFolder?: boolean) => void;
  selection?: any[];
  ownSelection: (number | string)[];
  fileSelector: FileSelector;
};

export default function FilesToolbar({
  dirs,
  path,
  parentFolderId,
  doUpload,
  setPath,
  fileSelector,
  selection,
  ownSelection,
}: FilesToolbarProps) {
  const { t } = useTranslation();

  return (
    <Flex wrap="wrap" alignItems="center" justify="between" mb={1}>
      <TooltipIconButton
        label={t("parentFolder")}
        icon={<FontAwesomeIcon icon={faArrowUp} />}
        onClick={() => setPath("/" + dirs.slice(0, -1).join("/"))}
      />

      <FolderBreadcrumb dirs={dirs} setPath={setPath} />

      <Box textAlign="right" flexGrow={1} fontSize="sm" m={2}>
        {t("filesSelected", { count: selection?.length ?? 0 })}
      </Box>

      <ButtonGroup variant="ghost" size="lg" m={1}>
        <TooltipIconButton
          label={t("toggleSelectAll")}
          onClick={() => fileSelector.toggleSelectAll()}
          icon={<FontAwesomeIcon icon={selection?.length ? faSquare : faCheckSquare} fixedWidth />}
        />

        <TooltipIconButton
          label={t("moveFilesUp")}
          isDisabled={!ownSelection.length || !parentFolderId}
          onClick={() => parentFolderId && server.files.mv(parentFolderId, ownSelection)}
          icon={<FontAwesomeIcon icon={faCaretUp} mask={faFolderOpen} fixedWidth transform="shrink-3 down-4" />}
        />
        <TooltipIconButton
          label={t("deleteSelection")}
          isDisabled={!selection?.length}
          onClick={() => fileSelector.delete()}
          icon={<FontAwesomeIcon icon={faTrashAlt} fixedWidth />}
        />
      </ButtonGroup>
      <Box mx={3} my={2} h={4} borderLeft="1px solid gray" />
      <ButtonGroup variant="ghost" size="lg" m={1}>
        <CreateFolder path={path}>
          {/* wrapped because PopoverTrigger in CreateFolder needs ref */}
          <div>
            <TooltipIconButton label={t("createFolder")} icon={<FontAwesomeIcon icon={faFolderPlus} fixedWidth />} />
          </div>
        </CreateFolder>
        <TooltipIconButton
          label={t("uploadFolders")}
          onClick={() => doUpload(true)}
          icon={<FontAwesomeIcon icon={faArrowUp} mask={faFolder} transform="shrink-6 down-3" />}
        />
        <TooltipIconButton
          label={t("uploadFiles")}
          onClick={() => doUpload()}
          icon={<FontAwesomeIcon icon={faArrowUp} mask={faFile} transform="shrink-6 down-3" />}
        />
      </ButtonGroup>
    </Flex>
  );
}
