import { useMemo } from "react";
import { Box, BoxProps, Icon, Center, useBoolean } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import {
  faFile,
  faFolder,
  faFolderOpen,
  faFileVideo,
  faFileAudio,
  faFileImage,
  faFileCode,
  faFileArchive,
  faFilePdf,
  faFileAlt,
  faCaretRight,
} from "@fortawesome/free-solid-svg-icons";
import { FileRecord, FileNodeRecord } from "$shared/APITypes";
import { getVideoLength } from "~/lib/utils";
import Media from "$elements/Media";

const DEFAULT_HEIGHT = 96;
const DEFAULT_ICON_MARGIN = 16;
const DEFAULT_ICON = faFile;
const FOLDER_ICON = faFolder;
const FOLDER_OPEN_ICON = faFolderOpen;
const FILE_ICONS: Record<string, IconDefinition> = {
  video: faFileVideo,
  audio: faFileAudio,
  image: faFileImage,
  code: faFileCode,
  archive: faFileArchive,
  pdf: faFilePdf,
  text: faFileAlt,
};

const typeOverride = {
  code: ["text/html", "text/x-shellscript"],
  archive: [
    "application/zip",
    "application/gzip",
    "application/x-compressed-tar",
    "application/vnd.rar",
    "application/x-7z-compressed",
    "application/x-freearc",
    "application/x-bzip",
    "application/x-bzip2",
  ],
  pdf: ["application/pdf"],
};

type FileIconProps = BoxProps & {
  file: FileNodeRecord;
  selected?: boolean;
  height?: number;
  iconMargin?: number;
  rounded?: BoxProps["rounded"];
  isOpen?: boolean;
};

export default function FileIcon({ file, selected, height, iconMargin, rounded, isOpen, ...props }: FileIconProps) {
  const [previewLoaded, { on: onPreviewLoaded }] = useBoolean(false);
  const icon = useMemo(() => {
    const folderIcon = isOpen ? FOLDER_OPEN_ICON : FOLDER_ICON;
    return "size" in file ? getIcon(file) : folderIcon;
  }, [file, isOpen]);

  const _height = height ?? DEFAULT_HEIGHT;
  const _iconMargin = iconMargin ?? DEFAULT_ICON_MARGIN;

  return (
    <Box height={_height + "px"} position="relative" _groupHover={{ filter: "brightness(125%)" }} {...props}>
      <Icon
        as={FontAwesomeIcon}
        icon={icon}
        fixedWidth
        fontSize={_height - _iconMargin * 2}
        marginTop={_iconMargin / 4}
        opacity={+!previewLoaded}
        transition="opacity .2s, color .2s"
        color={selected ? "pgreen.400" : "gray.200"}
      />
      {"media" in file && file.media && (
        <Center position="absolute" top={0} left={0} bottom={0} right={0}>
          <Box
            position="relative"
            overflow="hidden"
            rounded={rounded ?? "md"}
            ring={selected ? 3 : 0}
            ringColor="pgreen.500"
            transition="box-shadow .1s"
          >
            {file.media.type === "VIDEO" && (
              <>
                <Icon
                  as={FontAwesomeIcon}
                  icon={faCaretRight}
                  size="2x"
                  position="absolute"
                  top="50%"
                  left="50%"
                  transform="translate(-50%, -50%)"
                  zIndex={1}
                />
                <Box
                  position="absolute"
                  bottom={0}
                  right={0}
                  px={1}
                  fontSize="sm"
                  bg="blackAlpha.500"
                  zIndex={1}
                  roundedTopLeft="md"
                >
                  {getVideoLength(file.media.length!)}
                </Box>
              </>
            )}

            <Media
              height={_height}
              src={file}
              thumbnail
              forceImage
              onLoad={onPreviewLoaded}
              key={file.id}
              pointerEvents="none"
              opacity={file.media.type === "VIDEO" && previewLoaded ? 0.75 : 1}
            />
          </Box>
        </Center>
      )}
    </Box>
  );
}

function getIcon(file: FileRecord) {
  const fullmime = file.mime.split("; ");
  const mime = fullmime[0].split("/");
  let type = mime[0];
  for (const [_type, _mimes] of Object.entries(typeOverride)) {
    if (!_mimes.includes(fullmime[0])) continue;
    type = _type;
    break;
  }
  return FILE_ICONS[type] || DEFAULT_ICON;
}
