import { useState } from "react";
import { Box, BoxProps, Heading } from "@chakra-ui/react";
import type { FileNodeRecord } from "$shared/APITypes";
import server from "~/lib/server";
import FileIcon from "./FileIcon";

type FileItemProps = BoxProps & {
  file: FileNodeRecord;
  selected?: boolean;
  onMove?: (fileId: string | number, target: number) => void;
};

const customMime = "application/x-pcms-file-id";

export default function FileItem({ file, selected, onMove, ...props }: FileItemProps) {
  const [isDraggingOver, setDraggingOver] = useState(false);
  const name = "size" in file ? file.name : file.name.replace(/\/$/, "").split("/").pop()!;

  const onDragStart = (ev: React.DragEvent) => {
    ev.dataTransfer.effectAllowed = "all";
    if ("size" in file) {
      const fileUrl = server.getFileURL(file) ?? "";
      ev.dataTransfer.setData("text/uri-list", fileUrl);
      ev.dataTransfer.setData("text/plain", fileUrl);
    }
    ev.dataTransfer.setData(customMime, file.id + "");
  };

  const canDrop = (ev: React.DragEvent) => onMove && !("size" in file) && ev.dataTransfer.getData(customMime);

  const onDrop = (ev: React.DragEvent) => {
    setDraggingOver(false);

    if (!onMove || !canDrop(ev)) return;
    ev.preventDefault();

    const fileId = ev.dataTransfer.getData(customMime);
    onMove(isNaN(+fileId) ? fileId : +fileId, file.id as number);
  };

  return (
    <Box
      onDragStart={onDragStart}
      onDragOver={(ev) => canDrop(ev) && ev.preventDefault()}
      onDragEnter={(ev) => canDrop(ev) && setDraggingOver(true)}
      onDragLeave={() => setDraggingOver(false)}
      onDrop={onDrop}
      draggable={true}
      p={2}
      data-group
      cursor="pointer"
      textAlign="center"
      select="none"
      borderRadius="md"
      {...props}
    >
      <FileIcon file={file} selected={selected} isOpen={isDraggingOver} pointerEvents="none" />

      <Box position="relative" h={10} zIndex={10} mt={3} pointerEvents="none">
        <Box position="absolute" h="100%" w="100%" borderRadius="md" _groupHover={{ h: "auto" }}>
          <Heading
            isTruncated
            size="sm"
            borderRadius="md"
            p={2}
            bg={selected ? "pgreen.600" : "transparent"}
            whiteSpace="nowrap"
            lineHeight={1.1}
            transition="background .1s"
            _groupHover={{
              whiteSpace: "normal",
              h: "auto",
              bg: selected ? "pgreen.400" : "blackAlpha.600",
            }}
          >
            {name}
          </Heading>
        </Box>
      </Box>
    </Box>
  );
}
