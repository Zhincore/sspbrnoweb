import { IconButton, ButtonGroup } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCaretLeft, faCaretRight, faDownload } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";

type ViewerButtonsProps = {
  onClose: () => void;
  onNext?: () => void;
  onPrev?: () => void;
  onDownload: () => void;
};

export default function ViewerButtons({ onClose, onNext, onPrev, onDownload }: ViewerButtonsProps) {
  const { t } = useTranslation();

  return (
    <>
      <ButtonGroup position="absolute" top={5} right={5} size="lg" textShadow="lg">
        <IconButton
          variant="ghost"
          onClick={onDownload}
          aria-label={t("download")}
          title={t("download")}
          icon={<FontAwesomeIcon icon={faDownload} />}
        />

        <IconButton
          variant="ghost"
          onClick={onClose}
          aria-label={t("close")}
          title={t("close")}
          icon={<FontAwesomeIcon icon={faTimes} size="lg" />}
        />
      </ButtonGroup>

      <IconButton
        variant="ghost"
        position="absolute"
        top="50%"
        left={5}
        translateY="-50%"
        textShadow="lg"
        size="lg"
        opacity={0.75}
        onClick={(ev) => {
          ev.stopPropagation();
          if (onPrev) onPrev();
        }}
        title={t("prev")}
        aria-label={t("prev")}
        icon={<FontAwesomeIcon icon={faCaretLeft} size="lg" />}
      />

      <IconButton
        variant="ghost"
        position="absolute"
        top="50%"
        right={5}
        translateY="-50%"
        textShadow="lg"
        size="lg"
        opacity={0.75}
        onClick={(ev) => {
          ev.stopPropagation();
          if (onNext) onNext();
        }}
        title={t("next")}
        aria-label={t("next")}
        icon={<FontAwesomeIcon icon={faCaretRight} size="lg" />}
      />
    </>
  );
}
