import { Component, createRef } from "react";
import { Portal, Box, BoxProps } from "@chakra-ui/react";
import { motion, Variant } from "framer-motion";
import { FileRecord } from "$shared/APITypes";
import server from "~/lib/server";
import ViewerButtons from "./ViewerButtons";
import FileView from "./FileView";

const MotionBox = motion<BoxProps>(Box);

type ViewerProps = {
  file?: FileRecord;
  onNext?: () => void;
  onPrev?: () => void;
  onClose: () => void;
};

type ViewerState = {
  direction: 1 | -1;
  dragging: boolean;
};

type ViewerStateState = "open" | "closed";
const mainVariants: Record<ViewerStateState, Variant> = {
  closed: { opacity: 0, pointerEvents: "none" },
  open: { opacity: 1, pointerEvents: "auto" },
};

export default class FileViewer extends Component<ViewerProps, ViewerState> {
  private readonly ref = createRef<HTMLDivElement>();
  private readonly mediaRef = createRef<HTMLImageElement & HTMLVideoElement>();
  private lastFile?: FileRecord;

  state: ViewerState = {
    direction: 1,
    dragging: false,
  };

  componentDidUpdate(prevProps: ViewerProps) {
    // Focus on enter
    if (this.props.file && this.ref.current) this.ref.current.focus();

    if (prevProps.file === this.props.file) return;
    this.lastFile = this.props.file;

    const el = this.mediaRef.current;
    // Stop playback on leave
    if (el && !this.props.file && "pause" in el) el.pause();
  }

  onNext() {
    if (!this.props.onNext) return;

    this.setState({ direction: 1 });
    setTimeout(() => this.props.onNext!());
  }

  onPrev() {
    if (!this.props.onPrev) return;

    this.setState({ direction: -1 });
    setTimeout(() => this.props.onPrev!());
  }

  onKeyDown(ev: React.KeyboardEvent) {
    switch (ev.code) {
      case "ArrowRight": {
        this.onNext();
        break;
      }

      case "ArrowLeft": {
        this.onPrev();
        break;
      }

      case "Space": {
        const el = this.mediaRef.current;
        if (el && "play" in el) el.play();
        break;
      }

      case "Escape": {
        this.props.onClose();
        break;
      }
    }
  }

  render() {
    const { file, onClose, onNext, onPrev } = this.props;
    const reset = () => {
      this.lastFile = undefined;
    };

    const lastFile = file || this.lastFile;

    const download = () => {
      const url = lastFile && server.getFileURL(lastFile);
      if (!url) return;
      const a = document.createElement("a");
      a.setAttribute("download", lastFile.name);
      a.setAttribute("href", url);
      a.setAttribute("target", "_blank");
      a.click();
    };

    return (
      <Portal>
        <MotionBox
          variants={mainVariants}
          transition={{ type: "tween" }}
          initial="closed"
          animate={file ? "open" : "closed"}
          position="fixed"
          top={0}
          left={0}
          width="100vw"
          height="100vh"
          bg="blackAlpha.800"
          zIndex={500}
          onClick={() => this.state.dragging || onClose()}
          onAnimationComplete={() => file || reset()}
          onKeyDown={this.onKeyDown.bind(this)}
          tabIndex={0}
          ref={this.ref}
        >
          {lastFile && (
            <FileView
              direction={this.state.direction}
              dragging={this.state.dragging}
              setDragging={(dragging) => this.setState({ dragging })}
              file={lastFile}
              mediaRef={this.mediaRef}
              fileViewer={this}
            />
          )}

          <ViewerButtons
            onClose={onClose}
            onNext={onNext ? this.onNext.bind(this) : undefined}
            onPrev={onPrev ? this.onPrev.bind(this) : undefined}
            onDownload={download}
          />
        </MotionBox>
      </Portal>
    );
  }
}
