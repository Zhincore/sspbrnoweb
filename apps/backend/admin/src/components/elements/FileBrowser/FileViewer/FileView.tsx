import { RefObject } from "react";
import dynamic from "next/dynamic";
import { Box, Flex, FlexProps, Center, Spinner } from "@chakra-ui/react";
import { AnimatePresence, motion } from "framer-motion";
import usePromise from "$shared/hooks/usePromise";
import { Prisma } from "$shared/prisma";
import server from "~/lib/server";
import Media from "$elements/Media";
import FileIcon from "../FileItem/FileIcon";
import type FileViewer from "./";

const MotionFlex = motion<Omit<FlexProps, "onDragEnd">>(Flex);

const Editor = dynamic(() => import("$elements/MDEditor/Editor"), { ssr: false });

type FileViewProps = {
  file: Prisma.FileGetPayload<{ include: { media: true } }>;
  mediaRef: RefObject<HTMLImageElement & HTMLVideoElement>;
  direction: 1 | -1;
  setDragging: (dragging: boolean) => void;
  dragging?: boolean;
  fileViewer?: FileViewer;
};

export default function FileView({ file, direction, mediaRef, setDragging, fileViewer }: FileViewProps) {
  const fileUrl = server.getFileURL(file);
  const isText = file.mime.startsWith("text");
  const [content] = usePromise(async () => fileUrl && fetch(fileUrl).then((r) => r.text()), isText);
  const width = window ? window.innerWidth : 0;

  const View = () => {
    if (file.media) {
      return (
        <Box maxH="full" maxW="full">
          <Media
            ref={mediaRef}
            draggable={false}
            onDragStart={(ev: React.MouseEvent) => ev.preventDefault()}
            controls
            priority
            thumbnail={false}
            src={file}
            alt={file.name}
            objectFit="contain"
            maxH="full"
            maxW="full"
            spinner={<Spinner size="xl" color="pgreen.400" />}
          />
        </Box>
      );
    } else if (isText && content) {
      return (
        <Box
          w="50%"
          maxW="100%"
          onClick={(ev) => {
            ev.preventDefault();
            ev.stopPropagation();
            return false;
          }}
        >
          <Editor mode="none" readOnly={true} value={content} onChange={() => null} />
        </Box>
      );
    }
    return (
      <>
        <FileIcon file={file} />

        {file.mime.startsWith("audio") && <audio src={fileUrl} ref={mediaRef} controls />}
      </>
    );
  };

  return (
    <AnimatePresence initial={false}>
      <MotionFlex
        key={file.id}
        initial={{ x: direction * width }}
        animate={{ x: 0 }}
        exit={{ x: -direction * width }}
        transition={{ type: "tween" }}
        drag={false /* TODO: Fix the jumping on first click */}
        dragConstraints={{ right: 0, left: 0 }}
        dragTransition={{ bounceStiffness: 400, bounceDamping: 100 }}
        onDragStart={() => setDragging(true)}
        onDragEnd={(_, { offset: { x } }) => {
          Math.abs(x) > 200 && (x < 0 ? fileViewer?.onNext() : fileViewer?.onPrev());
          setTimeout(() => setDragging(false));
        }}
        h="100vh"
        w="100vw"
        position="fixed"
        top={0}
        left={0}
        justify="center"
        alignItems="center"
        cursor={undefined /*dragging ? "grabbing" : "grab"*/}
      >
        <Center flexDirection="column" h="full" w="full">
          <View />

          <Box position="absolute" top={0} left={0} w="full" textAlign="center" bg="blackAlpha.500" p={1}>
            {file.name}
          </Box>
        </Center>
      </MotionFlex>
    </AnimatePresence>
  );
}
