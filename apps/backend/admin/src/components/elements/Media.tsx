/* eslint-disable sonarjs/cognitive-complexity */
import { useState, useCallback, useMemo, useEffect, useRef } from "react";
import { useInView } from "react-cool-inview";
import { Box, Image, BoxProps, useMergeRefs, forwardRef } from "@chakra-ui/react";
import { FileRecord, MediaRecord } from "$shared/APITypes";
import { mediaToFile } from "$shared/utils";
import server from "~/lib/server";

type Fill = { src: FileRecord | MediaRecord | string; width?: never; height?: never };
type WithJustWidth = { src: FileRecord | MediaRecord; width: number; height?: never };
type WithJustHeight = { src: FileRecord | MediaRecord; width?: never; height: number };
type WithHeightAndWidth = {
  src: string | FileRecord;
  width: number;
  height: number;
};

type AsImage = {
  loading?: "lazy" | "eager";
  forceImage?: boolean;
  isVideo?: false;
  autoPlay?: never;
  controls?: never;
  muted?: never;
};
type AsVideo = {
  loading?: never;
  forceImage?: false;
  isVideo?: true;
  autoPlay?: boolean;
  controls?: boolean;
  muted?: boolean;
};

type MediaProps = (Fill | WithJustWidth | WithJustHeight | WithHeightAndWidth) &
  (AsImage | AsVideo) &
  BoxProps & {
    cssWidth?: number | string;
    cssHeight?: number | string;
    alt?: string;
    className?: string;
    thumbnail?: boolean;
    spinner?: React.ReactNode;

    priority?: boolean;
    /** Use useCallback */
    onLoad?: () => void;
  };

const emptyUrl = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";

export default forwardRef<MediaProps, "img" | "picture" | "video">(function Media(
  {
    src: aSrc,
    onLoad,
    priority,
    width,
    height,
    cssWidth,
    cssHeight,
    thumbnail,
    spinner,
    alt,
    className,
    isVideo,
    forceImage,
    autoPlay,
    controls,
    muted,
    loading,
    ...props
  },
  outref,
) {
  const [conf] = server.files.useImageConfig();
  // State
  const [loaded, setLoaded] = useState(false);
  const [wasViewed, setViewed] = useState(false);
  const { observe, inView } = useInView({
    onEnter: () => setViewed(true),
    threshold: 0.25,
  });
  const ref = useRef<HTMLImageElement & HTMLVideoElement>(null);
  const mergeRef = useMergeRefs<(HTMLImageElement | HTMLVideoElement) | null>(ref, outref)!;
  const file = (() => {
    if (typeof aSrc === "string") return;
    if ("file" in aSrc) mediaToFile(aSrc);
    else return aSrc;
  })();

  // Scaling assistance
  let sizeDefaulted = false;
  if (file) {
    [width, height, sizeDefaulted] = getScaling(file, width, height);
  }
  const size = Math.min(width!, height!);

  // Prepare props
  const _isVideo = !forceImage && (isVideo || file?.media?.type === "VIDEO");

  // onLoad functions
  const checkLoaded = useCallback(
    (instant = false) => {
      if (loaded || !ref.current) return;

      if (ref.current.complete || ref.current.readyState >= 2) {
        if (instant) setLoaded(true);
        else {
          window.requestAnimationFrame(() => window.requestAnimationFrame(() => setLoaded(true)));
        }

        if (onLoad) onLoad();
      }
    },
    [loaded, onLoad],
  );

  useEffect(() => {
    observe(ref.current);
  }, [observe]);

  // Trigger for too quickly loaded images
  useEffect(() => {
    checkLoaded(true);
  }, [checkLoaded]);

  const imgSize = useMemo(() => {
    if (!conf || typeof aSrc === "string") return undefined;

    let _size = size;
    let lastDistance: number | null = null;

    if (!conf.sizes.includes(size)) {
      // Find closes possible size
      for (const possibleSize of conf.sizes) {
        _size = possibleSize;
        const distance = Math.abs(possibleSize - size);
        if (lastDistance === null || distance < lastDistance) {
          lastDistance = distance;
        } else if (distance > lastDistance) {
          break;
        }
      }
    }

    return _size;
  }, [aSrc, size, conf]);

  const fileSrc = file ? server.getFileURL(file) : (aSrc as string);
  const posterSrc = file ? server.getFileURL(file, true) : undefined;
  const media = typeof aSrc == "string" ? undefined : aSrc;
  const _a = fileSrc!.split(".");
  const ext = _a[_a.length - 1];

  const show = (!media || conf) && (priority || inView || wasViewed);

  const viewVidSrc = show ? fileSrc : undefined;
  const viewImgSrc = (() => {
    if (!show) return emptyUrl;
    if (!media || !file) return aSrc as string;
    return server.getFileURL(file!, thumbnail);
  })();

  const _onLoad = () => checkLoaded(false);
  const keep = conf?.keepExts.includes(ext) || thumbnail === false;
  const _cssWidth = cssWidth ?? (sizeDefaulted ? undefined : width);
  const _cssHeight = cssHeight ?? (sizeDefaulted ? undefined : height);
  const commonProps = {
    className,
    width: !_cssWidth || isNaN(+_cssWidth) ? _cssWidth : _cssWidth + "px",
    htmlWidth: width,
    height: !_cssHeight || isNaN(+_cssHeight) ? _cssHeight : _cssHeight + "px",
    htmlHeight: height,
  };

  return (
    <>
      {loaded || spinner}

      {(_isVideo && (
        <Box
          as="video"
          ref={mergeRef as any}
          poster={viewImgSrc}
          src={viewVidSrc}
          onLoad={_onLoad}
          autoPlay={autoPlay}
          controls={controls}
          muted={muted}
          {...commonProps}
          {...props}
        >
          {alt}
        </Box>
      )) ||
        ((keep || !media) && (
          <Image
            ref={mergeRef}
            src={viewImgSrc ?? fileSrc}
            alt={alt ?? ""}
            onLoad={_onLoad}
            loading={loading ?? "lazy"}
            objectFit="contain"
            {...commonProps}
            {...props}
          />
        )) || (
          <picture>
            {imgSize && (
              <>
                <source srcSet={server.getImage(media!.id, imgSize)} />
                {conf?.exts.map((aext) => (
                  <source key={aext} type={"image/" + aext} srcSet={server.getImage(media!.id, imgSize, aext)} />
                ))}
              </>
            )}
            <Image
              ref={mergeRef}
              src={viewImgSrc ?? (imgSize ? server.getImage(media!.id, imgSize, "png") : "")}
              alt={alt ?? ""}
              onLoad={_onLoad}
              loading={loading ?? "lazy"}
              objectFit="contain"
              {...commonProps}
              {...props}
            />
          </picture>
        )}

      {!wasViewed && (
        <noscript>
          {_isVideo ? (
            <video poster={posterSrc} src={fileSrc}>
              {alt}
            </video>
          ) : (
            <img src={fileSrc} alt={alt ?? ""} />
          )}
        </noscript>
      )}
    </>
  );
});

function getScaling(file: FileRecord, width?: number, height?: number) {
  if (file?.media === null) throw new Error("Media element was given a non-media file");

  const ratio = file.media.width / file.media.height;
  let sizeDefaulted = false;
  if (height === undefined && width !== undefined) {
    height = width / ratio;
  } else if (width === undefined && height !== undefined) {
    width = height * ratio;
  } else if (width === undefined && height == undefined) {
    width = file.media.width;
    height = file.media.height;
    sizeDefaulted = true;
  }

  return [width, height, sizeDefaulted] as [number, number, boolean];
}
