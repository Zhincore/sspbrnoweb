import { Wrap, ButtonGroup, Icon, Button, Box } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFont,
  faBold,
  faItalic,
  faStrikethrough,
  faHeading,
  faParagraph,
  faQuoteLeft,
  faListUl,
  faListOl,
  faEye,
  faEdit,
} from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import EasyMDE from "easymde";
import TooltipIconButton from "$elements/TooltipIconButton";
import Editor from "./Editor";

const HeadingIcon = (i: number) => (
  <Box as="span" position="relative">
    <Icon as={FontAwesomeIcon} mr={2} icon={faHeading} />
    <Box as="i" position="absolute" top={0} right={0} fontSize="sm">
      {i}
    </Box>
  </Box>
);

const TOOLBAR: EditorToolbarGroup[] = [
  {
    label: "font",
    icon: <FontAwesomeIcon icon={faFont} />,
    items: [
      {
        label: "bold",
        action: EasyMDE.toggleBold,
        icon: <FontAwesomeIcon icon={faBold} />,
      },
      {
        label: "italic",
        action: EasyMDE.toggleItalic,
        icon: <FontAwesomeIcon icon={faItalic} />,
      },
      {
        label: "strikethrough",
        action: EasyMDE.toggleStrikethrough,
        icon: <FontAwesomeIcon icon={faStrikethrough} />,
      },
    ],
  },
  {
    label: "heading",
    icon: <FontAwesomeIcon icon={faHeading} />,
    items: [
      {
        label: "heading 1",
        icon: HeadingIcon(1),
        action: EasyMDE.toggleHeading1,
      },
      {
        label: "heading 2",
        icon: HeadingIcon(2),
        action: EasyMDE.toggleHeading2,
      },
      {
        label: "heading 3",
        icon: HeadingIcon(3),
        action: EasyMDE.toggleHeading2,
      },
    ],
  },
  {
    label: "block",
    icon: <FontAwesomeIcon icon={faParagraph} />,
    items: [
      {
        label: "quote",
        action: EasyMDE.toggleBlockquote,
        icon: <FontAwesomeIcon icon={faQuoteLeft} />,
      },
      {
        label: "unordered list",
        action: EasyMDE.toggleUnorderedList,
        icon: <FontAwesomeIcon icon={faListUl} />,
      },
      {
        label: "ordered list",
        action: EasyMDE.toggleOrderedList,
        icon: <FontAwesomeIcon icon={faListOl} />,
      },
    ],
  },
];

type EditorToolbarProps = {
  editor?: Editor;
  preview?: boolean;
  readOnly?: boolean;
};

export default function EditorToolbar({ editor, preview, readOnly }: EditorToolbarProps) {
  const { t } = useTranslation();

  const shouldAct = editor && !readOnly;

  return (
    <Wrap bg="gray.700" roundedTop="md" p={2} spacing={4}>
      {TOOLBAR.map((group, i) => (
        <ButtonGroup key={i}>
          {group.items.map((item, j) => (
            <TooltipIconButton
              key={j}
              label={t(item.label)}
              icon={item.icon}
              onClick={() => shouldAct && item.action(editor)}
            />
          ))}
        </ButtonGroup>
      ))}

      <Button
        style={{ marginLeft: "auto" }}
        variant="outline"
        isDisabled={!editor}
        isActive={preview}
        onClick={() => editor && editor.togglePreview()}
        leftIcon={<FontAwesomeIcon fixedWidth icon={preview ? faEdit : faEye} />}
      >
        {t(preview ? "end preview" : "preview")}
      </Button>
    </Wrap>
  );
  // return (
  //   <Flex className="pf-u-p-sm pf-u-background-color-100">
  //     {TOOLBAR.map((group, i) => {
  //       const _DropdownItem = "breakpoint" in group ? OverflowMenuDropdownItem : DropdownItem;
  //       const dropdown = (() => {
  //         const tooltipRef = (tooltipRefs[group.id] = tooltipRefs[group.id] || { current: null });
  //         return (
  //           <Dropdown
  //             className="pf-u-mr-0"
  //             isPlain
  //             isOpen={expanded === group.id}
  //             toggle={
  //               <Button
  //                 ref={tooltipRef}
  //                 variant="plain"
  //                 className={commonClassName}
  //                 aria-label={t(group.id)}
  //                 onClick={() => setExpanded((v) => (v === group.id ? undefined : group.id))}
  //               >
  //                 <FontAwesomeIcon fixedWidth icon={group.icon} />
  //                 <Tooltip reference={tooltipRef} content={t(group.id)} />
  //               </Button>
  //             }
  //             dropdownItems={group.items.map(({ label, action, icon, className }, j) => (
  //               <_DropdownItem
  //                 key={j}
  //                 className={className}
  //                 isDisabled={!shouldAct}
  //                 onClick={() => shouldAct && action(editor!)}
  //                 icon={icon && <FontAwesomeIcon fixedWidth icon={icon} />}
  //               >
  //                 {t(label)}
  //               </_DropdownItem>
  //             ))}
  //           />
  //         );
  //       })();
  //
  //       return (
  //         <Fragment key={i}>
  //           {!!i && <hr className={cn("pf-c-divider pf-m-vertical", commonClassName)} />}
  //
  //           {"breakpoint" in group ? (
  //             <OverflowMenu breakpoint={group.breakpoint} className="pf-u-mr-0">
  //               <OverflowMenuContent>
  //                 {group.items.map(({ label, action, icon }, j) => {
  //                   const tooltipRef = (tooltipRefs[label] = tooltipRefs[label] || { current: null });
  //                   return (
  //                     <OverflowMenuItem key={j} className={commonClassName}>
  //                       <Button
  //                         ref={tooltipRef}
  //                         variant="plain"
  //                         isDisabled={!shouldAct}
  //                         aria-label={t(label)}
  //                         onClick={() => shouldAct && action(editor!)}
  //                       >
  //                         <FontAwesomeIcon fixedWidth icon={icon} />
  //                         <Tooltip reference={tooltipRef} content={t(label)} />
  //                       </Button>
  //                     </OverflowMenuItem>
  //                   );
  //                 })}
  //               </OverflowMenuContent>
  //               <OverflowMenuControl>{dropdown}</OverflowMenuControl>
  //             </OverflowMenu>
  //           ) : (
  //             dropdown
  //           )}
  //         </Fragment>
  //       );
  //     })}
  //
  //     <FlexItem align={{ default: "alignRight" }}>
  //       <Button
  //         variant="tertiary"
  //         disabled={!editor}
  //         isActive={preview}
  //         onClick={() => editor && editor.togglePreview()}
  //         icon={<FontAwesomeIcon fixedWidth icon={preview ? faEdit : faEye} />}
  //       >
  //         {t(preview ? "end preview" : "preview")}
  //       </Button>
  //     </FlexItem>
  //   </Flex>
  // );
}

type EditorToolbarItem = {
  icon: JSX.Element;
  label: string;
  action: (editor: Editor) => void;
};

type EditorToolbarGroup = {
  label: string;
  icon: JSX.Element;
  items: EditorToolbarItem[];
};
