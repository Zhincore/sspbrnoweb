import { useState, useEffect, useRef } from "react";
import Toolbar from "./EditorToolbar";
import Editor, { EditorProps } from "./Editor";

export default function MDEditor(props: EditorProps) {
  const [preview, setPreview] = useState(false);
  const [isLoaded, setLoaded] = useState(false);
  const editorRef = useRef<Editor>(null);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div>
      <Toolbar editor={isLoaded ? editorRef.current! : undefined} preview={preview} readOnly={props.readOnly} />

      <Editor
        ref={editorRef}
        isAttached
        preview={preview}
        togglePreview={(_state) => setPreview((state) => (_state === undefined ? !state : !!_state))}
        {...props}
      />
    </div>
  );
}
