import { Component, createRef } from "react";
import EasyMDE, { Options as EasyMDEOptions } from "easymde";
import Codemirror from "codemirror";
import "codemirror/addon/display/placeholder";
import "codemirror/addon/selection/mark-selection";
import "codemirror/addon/edit/continuelist";
import "codemirror/mode/gfm/gfm";
import "codemirror/mode/yaml/yaml";
import styles from "./editor.module.scss";

import "codemirror/lib/codemirror.css";

const CHANGE_DELAY = 100;
const easyMDEOptions: EasyMDEOptions = {
  blockStyles: {
    bold: "**",
    italic: "*",
    code: "```",
  },
  insertTexts: {
    link: ["[", "](#url#)"],
    image: ["![](", "#url#)"],
    //table: ["", "\n\n| Column 1 | Column 2 | Column 3 |\n| -------- | -------- | -------- |\n| Text     | Text     | Text     |\n\n"],
    horizontalRule: ["", "\n\n-----\n\n"],
  },
};

type EditorPropsControlled = {
  value: string;
  onChange: (value: string) => void;
};

type EditorPropsUncontrolled = {
  value?: never;
  onChange?: never;
};

export type EditorProps = (EditorPropsControlled | EditorPropsUncontrolled) & {
  id?: string;
  className?: string;
  preview?: boolean;
  togglePreview?: (state?: boolean) => void;
  placeholder?: string;
  readOnly?: boolean;
  height?: number | string;
  mode?: keyof typeof MODES;
  isAttached?: boolean;
};

const MODES = {
  md: { name: "gfm", highlightFormatting: true, gitHubSpice: false },
  yaml: { name: "yaml" },
  none: undefined,
};

export default class Editor extends Component<EditorProps> implements EasyMDE {
  ref = createRef<HTMLDivElement>();
  editor?: HTMLElement;
  updateTimeout?: NodeJS.Timeout;
  changeTimeout?: NodeJS.Timeout;
  options = easyMDEOptions;

  readonly codemirror = Codemirror((host) => (this.editor = host), {
    mode: MODES[this.props.mode ?? "md"],
    theme: "pcms",
    inputStyle: "contenteditable",
    value: this.props.value,
    addModeClass: true,
    readOnly: this.props.readOnly && "nocursor",
    spellcheck: this.props.mode === "md",
    autocorrect: this.props.mode === "md",
    autocapitalize: this.props.mode === "md",
    styleSelectedText: styles.selection,
    extraKeys: {
      Enter: "newlineAndIndentContinueMarkdownList",
      Tab: "tabAndIndentMarkdownList",
      "Shift-Tab": "shiftTabAndUnindentMarkdownList",
    },
    placeholder: this.props.placeholder,
    lineWrapping: true,
    // lineNumbers: true,
  });

  // EasyMDE compatibility
  value = this.codemirror.getValue;
  isPreviewActive = () => !!this.props.preview;
  toTextArea = () => null;
  isSideBySideActive = () => false;
  isFullscreenActive = () => false;
  clearAutosavedValue = () => null;
  togglePreview = this.props.togglePreview || (() => null);

  cleanup() {
    //
  }

  componentDidMount() {
    this.update(this.props, true);
    this.ref.current!.append(this.editor!);
    this.editor!.classList.add(styles.codemirror);
    this.codemirror.refresh();

    this.editor!.classList.toggle(styles.md, this.props.mode === "md");

    this.codemirror.on("changes", () => {
      if (!this.props.onChange) return;

      if (this.changeTimeout) clearTimeout(this.changeTimeout);
      this.changeTimeout = setTimeout(() => {
        const value = this.codemirror.getValue();

        if (value !== this.props.value) this.props.onChange!(value);
        this.changeTimeout = undefined;
      }, CHANGE_DELAY);
    });
  }

  shouldComponentUpdate(newProps: EditorProps) {
    this.update(newProps);

    return false;
  }

  update(newProps: EditorProps, force = false) {
    const oldProps = force ? {} : this.props;

    if (newProps.value && oldProps.value !== newProps.value) {
      this.setValue(newProps.value, force);
    }

    if (oldProps.preview !== newProps.preview) {
      this.setPreview(!!newProps.preview);
    }

    if (oldProps.placeholder !== newProps.placeholder) {
      this.codemirror.setOption("placeholder", newProps.placeholder);
    }

    if (oldProps.readOnly !== newProps.readOnly) {
      this.codemirror.setOption("readOnly", newProps.readOnly);
      this.editor!.toggleAttribute("readonly", newProps.readOnly);
    }

    if (oldProps.isAttached !== newProps.isAttached) {
      this.editor!.classList.toggle("attached", !!newProps.isAttached);
    }

    if (force || oldProps.height !== newProps.height) {
      const _height = typeof newProps.height === "number" ? newProps.height + "px" : newProps.height;
      this.ref.current!.style.setProperty("--codemirror-height", _height || "16rem");
    }
  }

  setValue(value: string, force = false) {
    // Save cursor pos to restore if needed
    const cursorPos = this.codemirror.getCursor();
    // Apply change
    this.codemirror.setValue(value);
    if (force) this.codemirror.refresh();
    // Restore cursor pos
    if (this.codemirror.hasFocus()) this.codemirror.setCursor(cursorPos);
  }

  setPreview(state: boolean) {
    this.codemirror.setOption("readOnly", state);
    this.codemirror.setOption("spellcheck", this.props.mode === "md" && !state);
    this.editor!.classList.toggle(styles.preview, state);
    this.codemirror.refresh();
  }

  getCurrentStyle(): string | null {
    const pos = this.codemirror.getCursor();
    return this.codemirror.getTokenTypeAt(pos);
  }

  render() {
    return <div ref={this.ref} className={this.props.className} />;
  }
}
