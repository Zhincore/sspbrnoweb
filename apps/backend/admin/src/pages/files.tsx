import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import { normalizePath } from "~/lib/server";
import Page from "$parts/Page";

const FileBrowser = dynamic(() => import("$elements/FileBrowser"), { ssr: false });

export default function Files() {
  const [path, setPath] = useState(normalizePath("/" + decodeURIComponent(location.hash).slice(1)));

  useEffect(() => {
    location.hash = "#" + normalizePath("/" + path);
  }, [path]);

  return (
    <Page name="files">
      <FileBrowser path={path} onChange={setPath} />
    </Page>
  );
}
