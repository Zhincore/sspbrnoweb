import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAt, faPhone, faBox, faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import Page from "$parts/Page";
import ContentEditor, { ContentField, TextField, FileField, MarkdownField } from "$elements/ContentEditor";

export default function General() {
  return (
    <Page name="general" breadcrumb={["settings"]}>
      <ContentEditor contentType="general">
        {(editor) => (
          <>
            <ContentField w="70%">
              <TextField editor={editor} name="name" label="title" isRequired />
            </ContentField>
            <ContentField w="30%">
              <TextField editor={editor} name="shortname" isRequired />
            </ContentField>

            <ContentField w="40%">
              <TextField editor={editor} name="address" icon={<FontAwesomeIcon fixedWidth icon={faMapMarkerAlt} />} />
            </ContentField>
            <ContentField w="20%">
              <TextField editor={editor} name="phone" icon={<FontAwesomeIcon fixedWidth icon={faPhone} />} />
            </ContentField>
            <ContentField w="25%">
              <TextField editor={editor} name="email" type="email" icon={<FontAwesomeIcon fixedWidth icon={faAt} />} />
            </ContentField>
            <ContentField w="15%">
              <TextField editor={editor} name="dataBox" icon={<FontAwesomeIcon fixedWidth icon={faBox} />} />
            </ContentField>

            <ContentField w="33.33%">
              <FileField editor={editor} name="logo" single media mimePrefix={["image"]} helperText="svgRecommended" />
            </ContentField>
            <ContentField w="33.33%">
              <FileField
                editor={editor}
                name="favicon"
                single
                media
                allowedTypes={["image/x-icon", "image/vnd.microsoft.icon"]}
              />
            </ContentField>
            <ContentField w="33.33%">
              <FileField editor={editor} name="cover" single media mimePrefix={["image"]} />
            </ContentField>

            <ContentField w="100%">
              <MarkdownField editor={editor} name="description" height="8rem" />
            </ContentField>

            <ContentField w="100%">
              <FileField editor={editor} name="projects" media mimePrefix={["image"]} />
            </ContentField>
          </>
        )}
      </ContentEditor>
    </Page>
  );
}
