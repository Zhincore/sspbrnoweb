import NextDocument, { Html, Head, Main, NextScript } from "next/document";
import { colors } from "$shared/theme";

export default class Document extends NextDocument {
  render() {
    return (
      <Html style={{ ["--chakra-ui-color-mode" as any]: "dark" }} data-theme="dark">
        <Head>
          <meta key="theme-color" name="theme-color" content={colors.pgreen[400]} />
          <meta name="robots" content="noindex, nofollow" />
        </Head>
        <body className="chakra-ui-dark">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
