import App, { AppProps } from "next/app";
import { AnimatePresence } from "framer-motion";
import Head from "next/head";
import dynamic from "next/dynamic";
import { ChakraProvider } from "@chakra-ui/react";
import { I18nextProvider } from "react-i18next";
import { theme, noStorageManager } from "$shared/theme";
import server from "~/lib/server";
import { normalizePathname } from "~/lib/utils";
import { i18next } from "~/lib/i18n";
import ForceDarkMode from "$parts/ForceDarkMode";
import favicon from "$public/favicon.ico";

import "@fortawesome/fontawesome-svg-core/styles.css";
import "@fontsource/open-sans/400.css";
import "@fontsource/open-sans/600.css";

const LoadingScreen = dynamic(() => import("$parts/LoadingScreen"));
const ServerStatus = dynamic(() => import("$parts/ServerStatus"), { ssr: false });
const LoginScreen = dynamic(() => import("$parts/LoginScreen"), { ssr: false });
const Layout = dynamic(() => import("$parts/Layout"), { ssr: false });

// Fontawesome config
import("@fortawesome/fontawesome-svg-core").then((m) => (m.config.autoAddCss = false));

// Try to pre-connect
let connectPromise: Promise<any> | undefined;
if (typeof document !== "undefined") connectPromise = server.connect().catch(console.warn);

type CustomAppState = {
  connected: boolean;
  connecting: boolean;
};

export default class CustomApp extends App<AppProps, Record<string, never>, CustomAppState> {
  state: CustomAppState = {
    connected: false,
    connecting: true,
  };

  componentDidMount() {
    if (connectPromise) connectPromise.then(() => this.updateConnectionState());

    // Single Page Application support
    const { router } = this.props;
    const asPath = normalizePathname(router.asPath);
    const route = normalizePathname(router.route);
    if (route === "" && asPath !== route) {
      // File for root path was loaded but the url is different (probably because we act as if this is a SPA)

      // "New" URL is always 404 because there is no server to create a new url
      const target = router.urlIsNew(asPath) ? "/404" : asPath;

      // Fetch the actual needed files instead
      console.info(`Replacing current route with '${target}'.`);
      router.replace(target, asPath);
    }

    server.on("connecting", this.updateConnectionState, this);
    server.on("connect", this.updateConnectionState, this);
    server.on("connect_error", this.updateConnectionState, this);
    server.on("disconnect", this.updateConnectionState, this);
  }

  componentWillUnmount() {
    server.off("connecting", this.updateConnectionState, this);
    server.off("connect", this.updateConnectionState, this);
    server.off("connect_error", this.updateConnectionState, this);
    server.off("disconnect", this.updateConnectionState, this);

    server.reset();
  }

  updateConnectionState() {
    this.setState(server.state);
  }

  render() {
    const { Component, pageProps, router } = this.props;

    const Screen = () =>
      this.state.connected ? (
        <Layout>
          <AnimatePresence>
            <Component key={router.asPath} {...pageProps} />
          </AnimatePresence>
        </Layout>
      ) : (
        <LoginScreen />
      );

    return (
      <I18nextProvider i18n={i18next}>
        <ChakraProvider theme={{ ...theme, initialColorMode: "dark" }} colorModeManager={noStorageManager}>
          <ForceDarkMode />

          <Head>
            <link rel="icon" href={favicon.src} />
            <link rel="preconnect" href={process.env.NEXT_PUBLIC_BACKEND_URL} />
            <title>PCMS</title>
          </Head>

          <ServerStatus />

          <AnimatePresence>{this.state.connecting ? <LoadingScreen /> : Screen()}</AnimatePresence>
        </ChakraProvider>
      </I18nextProvider>
    );
  }
}
