import NextLink from "next/link";
import { Box, Link } from "@chakra-ui/react";
import Page from "$parts/Page";

export default function Error404() {
  return (
    <Page name="pageNotFound">
      <Box>
        Jděte{" "}
        <NextLink href="/" passHref>
          <Link>domů</Link>
        </NextLink>
        .
      </Box>
    </Page>
  );
}
