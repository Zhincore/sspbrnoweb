import { Text } from "@chakra-ui/react";
import useServiceData from "~/hooks/useServiceData";
import type { sys } from "~/lib/server";
import DevModWarn from "$parts/DevModWarn";
import Page from "$parts/Page";

// TODO
export default function Index() {
  const [info] = useServiceData<sys.SysInfo>("sys:info");

  return (
    <Page name="dashboard">
      <DevModWarn info={info} />

      <Text mt={8} fontSize="sm">
        Node.js <strong>{info?.nodeVersion}</strong> @ <strong>{info?.hostname}</strong>
      </Text>
    </Page>
  );
}
