import Page from "$parts/Page";
import ContentListing from "$elements/ContentListing";

export default function PostListing() {
  return (
    <Page name="posts" breadcrumb={["content"]}>
      <ContentListing
        contentType="post"
        defaultShownFields={["title", "updatedAt", "createdAt"]}
        defaultSortField="updatedAt"
        fieldTypes={{ updatedAt: "date", createdAt: "date" }}
      />
    </Page>
  );
}
