import { slugify } from "~/lib/utils";
import ContentEditorPage from "$parts/ContentEditorPage";
import { ContentField, TextField, FileField, MarkdownField, CategoryField } from "$elements/ContentEditor";

export default function Post() {
  return (
    <ContentEditorPage contentType="post" plural="posts">
      {(editor) => (
        <>
          <ContentField w="25%">
            <CategoryField editor={editor} name="category" />
          </ContentField>
          <ContentField w="40%">
            <TextField editor={editor} name="title" isRequired />
          </ContentField>
          <ContentField w="35%">
            <TextField editor={editor} name="slug" isRequired icon="/" createFrom="title" filter={slugify} />
          </ContentField>

          <ContentField w="50%">
            <FileField editor={editor} name="cover" single media />
          </ContentField>
          <ContentField w="50%">
            <FileField editor={editor} name="attachments" />
          </ContentField>

          <ContentField w="100%">
            <MarkdownField editor={editor} name="content" />
          </ContentField>
        </>
      )}
    </ContentEditorPage>
  );
}
