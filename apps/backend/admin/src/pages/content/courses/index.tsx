import Page from "$parts/Page";
import ContentListing from "$elements/ContentListing";

export default function PostListing() {
  return (
    <Page name="courses" breadcrumb={["content"]}>
      <ContentListing
        contentType="course"
        defaultShownFields={["name", "slogan", "maturita"]}
        defaultSortField="name"
      />
    </Page>
  );
}
