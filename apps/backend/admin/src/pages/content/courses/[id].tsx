import { slugify } from "~/lib/utils";
import ContentEditorPage from "$parts/ContentEditorPage";
import { ContentField, TextField, SwitchField, IconField, MarkdownField } from "$elements/ContentEditor";

export default function Course() {
  return (
    <ContentEditorPage contentType="course" plural="courses">
      {(editor) => (
        <>
          <ContentField w="10%">
            <TextField editor={editor} name="color" type="color" />
          </ContentField>
          <ContentField w="20%">
            <IconField editor={editor} name="icon" isRequired />
          </ContentField>
          <ContentField w="35%">
            <TextField editor={editor} name="name" isRequired />
          </ContentField>
          <ContentField w="35%">
            <TextField editor={editor} name="slug" isRequired icon="/" createFrom="name" filter={slugify} />
          </ContentField>

          <ContentField w="90%">
            <TextField editor={editor} name="slogan" />
          </ContentField>
          <ContentField w="10%">
            <SwitchField editor={editor} name="maturita" />
          </ContentField>

          <ContentField w="100%">
            <MarkdownField editor={editor} name="description" />
          </ContentField>
        </>
      )}
    </ContentEditorPage>
  );
}
