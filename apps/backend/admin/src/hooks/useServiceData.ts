import { useState, useEffect, useCallback, useRef } from "react";
import server from "~/lib/server";

export default function useServiceData<Type>(eventName: string, ...args: any[]) {
  const hash = server.hashCall(eventName, ...args);
  const { current: initialHash } = useRef(hash);
  const [data, setData] = useState<Type | null>(server.getCachedData(eventName, ...args));

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const update = useCallback<() => Promise<Type>>(() => server.call<Type>(eventName, ...args), [eventName, ...args]);

  useEffect(() => {
    const service = eventName.split(":")[0];

    // Listen for updates
    server.connection.on(service + ":updated", update);

    // Download data
    if (hash !== initialHash || !data) update();

    // Cleanup
    const cleanCallListener = server.getCallListener<Type>(setData, eventName, ...args);
    return () => {
      server.connection.off(service + ":updated", update);
      cleanCallListener();
    };
  }, [update, eventName, ...args]); // eslint-disable-line react-hooks/exhaustive-deps

  return [data, update] as [typeof data, () => void];
}
