import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import resourcesToBackend from "i18next-resources-to-backend";

i18next
  .use(
    resourcesToBackend((language, _namespace, callback) =>
      import(`../locales/${language}.yml`).then(
        (resource) => callback(null, flattenPluralObjects(resource)),
        (error) => callback(error, null),
      ),
    ),
  )
  .use(initReactI18next)
  .init({
    lng: "cs",
    ns: [],
    partialBundledLanguages: true,
    interpolation: {
      escapeValue: false,
    },
    react: {
      useSuspense: false,
    },
  });

export { i18next };

function flattenPluralObjects({ ...resource }: Record<string, any>) {
  for (const [key, value] of Object.entries(resource)) {
    if (typeof value === "string") continue;
    delete resource[key];
    for (const [suffix, subvalue] of Object.entries(value)) {
      resource[`${key}_${suffix}`] = subvalue;
    }
  }
  return resource;
}
