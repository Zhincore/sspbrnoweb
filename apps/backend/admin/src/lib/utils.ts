const getSlugify = () => import("slugify").then((m) => m.default);

declare global {
  interface String {
    capitalize(): string;
    decapitalize(): string;
  }
}

String.prototype.capitalize = function () {
  return this[0].toUpperCase() + this.slice(1);
};

String.prototype.decapitalize = function () {
  return this[0].toLowerCase() + this.slice(1);
};

export const slugifyOptions = {
  lower: true,
  strict: true,
  trim: false,
};

export async function slugify(value: string) {
  return (await getSlugify())(value, slugifyOptions);
}

export function ensureArray(obj: any) {
  return Array.isArray(obj) ? obj : [obj];
}

export function getVideoLength(length: number, float = false) {
  const hours = Math.trunc(length / 3600);
  const minutes = Math.trunc((length - hours * 3600) / 60);
  const seconds = length - minutes * 60 - hours * 3600;

  return [minutes && hours, minutes || "00", float ? seconds : Math.round(seconds)]
    .filter((v) => v)
    .map((v) => String(v).padStart(2, "0"))
    .join(":");
}

export function normalizePathname(path: string) {
  return new URL(path, location.href).pathname.replace(/\/$/, "");
}
