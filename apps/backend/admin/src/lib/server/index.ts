import { EventEmitter } from "eventemitter3";
import type { Socket } from "socket.io-client";
import { TFunction } from "i18next";
import { FileRecord } from "$shared/APITypes";
import ControllablePromise from "$shared/ControllablePromise";
import * as modules from "./modules";
import ProxySocket from "./ProxySocket";
const io = import("socket.io-client").then((m) => m.default);

const REINIT_DELAY = 2000;
const MAX_CACHE_SIZE = 64;

export * from "./modules";

type Callback<Type> = (...args: Type[]) => void;

class ServerConnector extends EventEmitter {
  private readonly cache = new Map<string, any>();
  private readonly debounce = new Map<string, Promise<any>>();
  private socket?: Socket;
  private destroyed = false;
  private _connectPromise = new ControllablePromise<Socket>();
  readonly connection = new ProxySocket();
  connectPromise: Promise<Socket> = new Promise(this._connectPromise.getController());
  connected = false;
  connecting = false;

  constructor() {
    super();

    if (typeof window !== "undefined") {
      window.addEventListener(
        "unload",
        (ev) => {
          if (ev.defaultPrevented) return;
          this.destroy();
        },
        { passive: true },
      );
    }
  }

  get state() {
    const { connected, connecting } = this;
    return { connected, connecting };
  }

  reinitPromises() {
    this._connectPromise = new ControllablePromise();
    this.connectPromise = new Promise(this._connectPromise.getController());
  }

  reset() {
    if (this.socket && this.socket.connected) this.socket.disconnect();

    this.cache.clear();
    this.debounce.clear();
    this.reinitPromises();
  }

  destroy() {
    this.destroyed = true;
    this.reset();
  }

  async connect(): Promise<Socket> {
    if (this.socket || this.destroyed) return this.connectPromise;

    const access_token = modules.auth.getToken();
    if (!access_token) {
      this._connectPromise.reject(new Error("Missing access token"));
      const connectPromise = this.connectPromise;
      this.reinitPromises();
      return connectPromise;
    }

    this.emit("connecting", this.connectPromise);
    this.connecting = true;

    const url = new URL(process.env.NEXT_PUBLIC_BACKEND_URL!);
    this.socket = (await io)(url.origin, {
      path: url.pathname.replace(/\/$/, "") + "/socket/",
      rememberUpgrade: true,
      reconnection: true,
      reconnectionDelay: REINIT_DELAY,
      transports: ["websocket", "polling"],
      closeOnBeforeunload: true,
      auth: { access_token },
    });

    this.socket.on("connect", () => {
      if (this.connected) return; // duplicate
      this.connection.bindSocket(this.socket);

      this.connected = true;
      this.connecting = false;
      this.emit("connect");
      this._connectPromise.resolve(this.socket!);
    });

    this.socket.on("connect_error", (err) => {
      this.connected = false;
      this.connecting = false;

      if (err.message === "Unauthorized") {
        // Reset token
        this._connectPromise.reject(err);
        return modules.auth.logout();
      }

      this.emit("connect_error", err);
      this._connectPromise.reject(err);
    });

    this.socket.on("disconnect", (reason) => {
      this.connection.bindSocket();

      this.connected = false;
      this.connecting = false;
      this.emit("disconnect", reason === "io client disconnect");
      this.reinitPromises();

      if (!this.destroyed && reason === "ping timeout") {
        // Probabably a connection issue
        this.socket?.connect();
      } else {
        // Something happened, reset is probbably needed
        this.socket?.off();
        this.socket = undefined;
        if (reason !== "io client disconnect") setTimeout(() => this.connect(), REINIT_DELAY);
      }
    });

    return this.connectPromise;
  }

  hashCall(eventName: string, ...args: any[]) {
    return JSON.stringify([eventName, ...args]); // NOTE: There might be a better hashing method out there?
  }

  private handleError(error: any) {
    const name = error.name ?? error.error ?? error.message;
    if (name === "Unauthorized") modules.auth.logout();
    this.emit("error", error);
  }

  async _waitFor<Type>(eventName: string, ...params: any[]): Promise<Type> {
    const hash = this.hashCall(eventName, ...params);

    // If this exact command is already running at this moment, don't rerun in
    if (this.debounce.has(hash)) return this.debounce.get(hash);

    const promise = new Promise<Type>((resolve, reject) =>
      this.connection.send(eventName, ...params, ({ data, error }: { data: Type; error?: any }) => {
        this.debounce.delete(hash);

        if (error) {
          reject(error);
          this.handleError(error);
        } else resolve(data);

        // Trigger update events
        this.emit(hash, data);
        this.cache.set(hash, data);
        this.capCache();
      }),
    );
    this.debounce.set(hash, promise);

    return promise;
  }

  capCache() {
    if (this.cache.size > MAX_CACHE_SIZE) cutMap(this.cache, MAX_CACHE_SIZE);
    if (this.debounce.size > MAX_CACHE_SIZE) cutMap(this.debounce, MAX_CACHE_SIZE);
  }

  async fetch(...params: Parameters<typeof fetch>) {
    return fetch(params[0], {
      mode: "cors",
      ...params[1],
      headers: {
        ...(params[1]?.headers ?? {}),
        Authorization: `bearer ${modules.auth.getToken()}`,
      },
    })
      .then(async (result) => ({ data: await result.json(), result }))
      .then(({ data, result }) => {
        if (!result.ok) {
          const error = new Error(data.message);
          if (data.error) error.name = data.error;
          this.handleError(error);
          throw error;
        }
        return data;
      });
  }

  async call<Type>(eventName: string, ...args: any[]) {
    return this._waitFor<Type>(eventName, ...args);
  }

  getCachedData<Type>(eventName: string, ...args: any[]): Type | null {
    return this.cache.get(this.hashCall(eventName, ...args));
  }

  // Register a callback for when other part of code refreshes data that your part uses
  getCallListener<Type>(callback: Callback<Type>, eventName: string, ...args: any[]): () => void {
    const hash = this.hashCall(eventName, ...args);

    this.on(hash, callback);

    return () => {
      this.off(hash, callback);
    };
  }

  getFileURL(file: FileRecord, poster?: boolean) {
    poster = poster && file.media?.type === "VIDEO";
    return process.env.NEXT_PUBLIC_BACKEND_URL! + "/files/" + (file.id + "." + (poster ? "poster.png" : file.ext));
  }

  getImage(fileId: string, size: number, ext?: string) {
    return process.env.NEXT_PUBLIC_BACKEND_URL! + "/images/" + size + "/" + fileId + (ext ? "." + ext : "");
  }

  getErrorMessage(error: any, t: TFunction) {
    if (!error) return;
    if (typeof error === "string") return error;

    const name: string | undefined = error.name ?? error.error ?? error.message;
    if (name) {
      const nameLw = name.toLowerCase();
      const trans = t(nameLw);
      if (trans === nameLw) return name + (error.message && error.message !== name ? `: ${error.message}` : "");
      return trans;
    } else if ("toString" in error && typeof error.toString === "function" && error.toString() !== "[object Object]") {
      return error.toString();
    } else {
      return JSON.stringify(error);
    }
  }

  normalizePath = normalizePath;
}

export default Object.assign(new ServerConnector(), modules);

function cutMap(map: Map<any, any>, size: number) {
  const iterator = map.keys();
  while (map.size > size) map.delete(iterator.next());
}

export function normalizePath(path: string) {
  return ("/" + path.trim() + "/").replace(/\/+/g, "/");
}
