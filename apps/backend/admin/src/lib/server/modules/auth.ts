import server from "~/lib/server";

export async function login(username: string, password: string) {
  return fetch(process.env.NEXT_PUBLIC_BACKEND_URL! + "/auth/login", {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
  }).then(async (response) => {
    const result: { access_token: string } | { message: string } = await response.json();
    if ("message" in result) {
      window.localStorage.removeItem("access_token");
      return result.message;
    }

    window.localStorage.setItem("access_token", result.access_token);
    server.connect();
  });
}

export function getToken() {
  return window.localStorage.getItem("access_token");
}

export function logout() {
  window.localStorage.removeItem("access_token");
  server.reset();
}
