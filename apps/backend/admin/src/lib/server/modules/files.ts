import type { FolderRecord, FileRecord, ImageConfig } from "$shared/APITypes";
import useServiceData from "~/hooks/useServiceData";
import server from "~/lib/server";

export { fileToMedia, mediaToFile } from "$shared/utils";

export function get(id: string) {
  return server.call<FileRecord | null>("files:get", id);
}
export function useFile(id: string) {
  return useServiceData<FileRecord | null>("files:get", id);
}

export function useImageConfig() {
  return useServiceData<ImageConfig>("images:getConfig");
}

export function getFolder(pathOrId: string | number) {
  return server.call<FolderRecord>("files:list", pathOrId);
}
export function useFolder(pathOrId: string | number) {
  return useServiceData<FolderRecord>("files:list", pathOrId);
}

export function rename(id: string | number, name: string) {
  return server.call<void>("files:rename", id, name);
}

export function mkdir(path: string) {
  return server.call<{ id: number }>("files:mkdir", path);
}

export function mv(target: number, items: (string | number)[]) {
  return server.call<{ id: number }>("files:mv", target, items);
}

export function rmdir(pathOrId: string | number) {
  return server.call<boolean>("files:rmdir", pathOrId);
}

export function rm(id: string | number) {
  return server.call<boolean>("files:rm", id);
}

export async function upload(files: FileList | null, path = "/") {
  if (!files) return;

  const body = new FormData();
  for (const file of files) {
    body.append("files", file, window.btoa(encodeURIComponent(path + "/" + (file.webkitRelativePath || file.name))));
  }

  return server.fetch(process.env.NEXT_PUBLIC_BACKEND_URL! + "/files", { method: "POST", body });
}
