import { Prisma } from "$shared/prisma";
import { ContentTypes, ContentTypesEnum } from "$shared/APITypes";
import { KeysOfType } from "~/lib/utils";
import server from "~/lib/server";
import useServiceData from "~/hooks/useServiceData";

type _ListingOptions<T extends Record<string, any>> = Pick<T, "orderBy" | "where">;
export type ListableContentTypes = {
  post: _ListingOptions<Prisma.PostFindManyArgs>;
  course: _ListingOptions<Prisma.CourseFindManyArgs>;
};
export type ListableContentTypesEnum = keyof ListableContentTypes;

export const idFields: { [Type in ContentTypesEnum]: KeysOfType<ContentTypes[Type], string | number> } = {
  general: "id",
  post: "slug",
  course: "slug",
};

export const scalarFields: {
  [Type in ContentTypesEnum]?: KeysOfType<ContentTypes[Type], string | number | Date | null | undefined>[];
} = {
  post: ["content", "slug", "createdAt", "updatedAt", "title"],
  course: ["name", "slogan", "slug", "description"],
};

export type ContentListingOptions<ContentType extends ListableContentTypesEnum> = {
  fields?: KeysOfType<ContentTypes[ContentType], any>[];
  where?: ListableContentTypes[ContentType]["where"];
  orderBy?: ListableContentTypes[ContentType]["orderBy"];
  pageSize?: number | null;
  page?: number;
};

export function useList<ContentType extends ListableContentTypesEnum>(
  contentType: ContentType,
  options?: ContentListingOptions<ContentType>,
) {
  return useServiceData<ContentTypes[ContentType][]>("content:list", contentType, options);
}

export function remove(model: ContentTypesEnum, id: any) {
  return server.call("content:delete", model, id);
}
