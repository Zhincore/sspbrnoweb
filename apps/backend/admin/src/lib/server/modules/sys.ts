import server from "~/lib/server";

export type SysInfo = {
  hostname: string;
  arch: string;
  platform: NodeJS.Platform;
  osRelease: string;
  nodeVersion: string;
  isProduction: boolean;
};

export async function info() {
  return server.call<SysInfo>("sys:info");
}
