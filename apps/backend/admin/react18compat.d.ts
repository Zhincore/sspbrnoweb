import * as React from "react";

declare module "@chakra-ui/react" {
  export const PopoverTrigger: React.FunctionComponent<{
    children?: React.ReactNode;
  }>;
}
