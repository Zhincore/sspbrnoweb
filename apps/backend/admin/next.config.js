/* eslint-disable @typescript-eslint/no-var-requires */
const analyze = process.env.ANALYZE === "true";
const production = process.env.NODE_ENV === "production";

const NEXT = require("next/constants");
const withPWA = require("next-pwa");
const withTM = require("next-transpile-modules")(["$shared"]);
const withBundleAnalyzer = analyze
  ? require("@next/bundle-analyzer")({
      enabled: analyze,
    })
  : (a) => a;

module.exports = (phase) => {
  /**
   * @type {import('next').NextConfig}
   */
  const config = {
    poweredByHeader: false,
    reactStrictMode: true,
    trailingSlash: true,
    compress: false, // NGINX should do this
    basePath: process.env.NEXT_PUBLIC_ADMIN_BASE_PATH ?? "",
    eslint: {
      ignoreDuringBuilds: production,
    },
    typescript: {
      ignoreBuildErrors: production, // speed
    },
    exportPathMap:
      phase === NEXT.PHASE_EXPORT
        ? () => ({
            "/": { page: "/" },
          })
        : undefined,
    webpack(webpackConfig) {
      webpackConfig.module.rules.push(
        {
          test: /\.svg$/,
          issuer: { and: [/\.(js|ts)x?$/] },
          use: ["@svgr/webpack"],
        },
        {
          test: /\.ya?ml$/,
          type: "json",
          use: { loader: "yaml-loader", options: { asJSON: true } },
        },
      );

      return webpackConfig;
    },
    pwa: {
      disable: phase === NEXT.PHASE_DEVELOPMENT_SERVER,
      dest: "public",
    },
  };
  return withBundleAnalyzer(withPWA(withTM(config)));
};
